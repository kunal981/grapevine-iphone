//
//  NSRegularExpression+Utility.h
//  GrapeVine
//
//  Created by Zachary Gavin on 9/27/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSRegularExpression (Utility)

- (NSArray*) matchesInString:(NSString *)string;

@end
