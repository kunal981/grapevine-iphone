//
//  UIImage+Color.h
//  GrapeVine
//
//  Created by Zachary Gavin on 7/23/13.
//  Copyright (c) 2013 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Color)

+ (UIImage*) imageWithColor:(UIColor*)color;

@end
