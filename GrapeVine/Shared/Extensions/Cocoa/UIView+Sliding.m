//
//  UIView+Sliding.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/24/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "UIView+Sliding.h"
#import <objc/runtime.h>

enum {
  UIViewSlidingOut,
  UIViewSlidingIn
};
typedef NSUInteger UIViewSliding;

@implementation UIView (Sliding)

static char* slidingStatusKey = "slidingStatusKey";

- (void) slideIn {
  [self.superview bringSubviewToFront:self];
  objc_setAssociatedObject(self,slidingStatusKey,@(UIViewSlidingIn),OBJC_ASSOCIATION_RETAIN);
  
  [UIView animateWithDuration:0.5 animations:^{
    CGRect frame = self.frame;
    frame.origin.y = self.superview.frame.size.height-frame.size.height;
    self.frame = frame;
  }];
}

- (void) slideOut {
  objc_setAssociatedObject(self,slidingStatusKey,@(UIViewSlidingOut),OBJC_ASSOCIATION_RETAIN);
  [self.superview bringSubviewToFront:self];
  [UIView animateWithDuration:0.5 animations:^{
    CGRect frame = self.frame;
    frame.origin.y = self.superview.frame.size.height;
    self.frame = frame;
  }];
}

- (void) slideToggle {
  ((NSNumber*) objc_getAssociatedObject(self, slidingStatusKey)).unsignedIntegerValue ? [self slideOut] : [self slideIn];
}

@end
