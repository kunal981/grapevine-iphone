//
//  NSDictionary+Subscript.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "NSDictionary+Subscript.h"

@implementation NSDictionary (Subscript)

- (id) objectForKeyedSubscript:(id)key {
  return [self objectForKey:key];
}


@end
