//
//  NSDictionary+Subscript.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Subscript)

- (id) objectForKeyedSubscript:(id)key;

@end
