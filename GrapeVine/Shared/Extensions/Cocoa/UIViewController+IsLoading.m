//
//  UIViewController+IsLoading.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/7/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "UIViewController+IsLoading.h"
#import "IsLoadingView.h"
#import <objc/runtime.h>
#import <TapkuLibrary/TapkuLibrary.h>

static char const * const IS_LOADING_KEY = "IS_LOADING_KEY";
static char const * const IS_LOADING_TARGET_VIEW_KEY = "IS_LOADING_TARGET_VIEW_KEY";
static char const * const IS_LOADING_VIEW_KEY = "IS_LOADING_VIEW_KEY";

@implementation UIViewController (IsLoading)

- (void) setIsLoading:(BOOL)isLoading {
  BOOL _isLoading = self.isLoading;
  if (isLoading != _isLoading) {
    objc_setAssociatedObject(self, IS_LOADING_KEY, @(isLoading), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    IsLoadingView* isLoadingView = objc_getAssociatedObject(self, IS_LOADING_VIEW_KEY);
    if(isLoading) {
      isLoadingView = [[IsLoadingView alloc] initWithTarget:self.isLoadingTargetView];
      [isLoadingView show];
    } else {
      [isLoadingView hide];
      isLoadingView = nil;
    }
    
    objc_setAssociatedObject(self, IS_LOADING_VIEW_KEY, isLoadingView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
  }
}

- (BOOL) isLoading {
  return ((NSNumber*) objc_getAssociatedObject(self, IS_LOADING_KEY)).boolValue;
}

- (void) setIsLoadingTargetView:(UIView *)isLoadingTargetView {
   objc_setAssociatedObject(self, IS_LOADING_TARGET_VIEW_KEY, isLoadingTargetView, OBJC_ASSOCIATION_RETAIN_NONATOMIC) ;
}

- (UIView*) isLoadingTargetView {
  return (UIView*) objc_getAssociatedObject(self, IS_LOADING_TARGET_VIEW_KEY) ?: self.view;
}

@end
