//
//  NSString+Utility.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/16/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utility)

- (NSString*) classify;
- (NSString*) classify:(BOOL)capitalizeFirst;
- (NSString*) underscore;
- (BOOL)startsWith:(NSString *)start;
- (BOOL)endsWith:(NSString *)end;

@end
