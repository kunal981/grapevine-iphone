//
//  NSObject+Introspection.m
//  GrapeVine
//
//  Created by Zachary Gavin on 9/24/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "NSObject+Introspection.h"
#import <objc/runtime.h>
#import "NSArray+Subscript.h"
#import "NSArray+Enumerable.h"

@implementation NSObject (Introspection)


+ (BOOL) isPropertyDefined:(NSString*)name {
  return class_getProperty(self, [name UTF8String]) != NULL;
}

+ (BOOL) isPropertyWriteable:(NSString*)name {
  NSArray* attributes = [self propertyAttributes:name];
  return attributes ? ![attributes any:^(NSString* component){ return [component isEqualToString:@"R"]; }] : NO;
}

+ (NSString*) propertyType:(NSString*)name {
  return [[self propertyAttributes:name][0] substringWithRange:NSMakeRange(1, 1)];
}

+ (Class) propertyClass:(NSString*)name {
  NSString* className = [self propertyAttributes:name][0];
  return NSClassFromString([className substringWithRange:NSMakeRange(3, className.length-4)]);
}

+ (NSArray*) propertyAttributes:(NSString*)name {
  objc_property_t property = class_getProperty(self, [name UTF8String]);
  if(property == NULL) return nil;
  
  return [[NSString stringWithUTF8String:property_getAttributes(property)] componentsSeparatedByString:@","];
}

+ (NSArray*) propertyNames {
  unsigned int count;
  objc_property_t* list =  class_copyPropertyList(self, &count);
  NSMutableArray* ret = [@[] mutableCopy];
  for(int i=0;i<count;i++) {
    [ret addObject:[NSString stringWithUTF8String:property_getName(list[i])]];
  }
  return [NSArray arrayWithArray:ret];
}

- (BOOL) isPropertyDefined:(NSString*)name {
  return [[self class] isPropertyDefined:name];
}

- (BOOL) isPropertyWriteable:(NSString*)name {
  return [[self class] isPropertyWriteable:name];
}

- (NSString*) propertyType:(NSString*)name {
  return [[self class] propertyType:name];
}

- (Class) propertyClass:(NSString*)name {
  return [[self class] propertyClass:name];
}

- (NSArray*) propertyNames {
  return [[self class] propertyNames];
}

@end
