//
//  NSArray+Subscript.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "NSArray+Subscript.h"

@implementation NSArray (Subscript)

- (id) objectAtIndexedSubscript:(NSUInteger)index {
  return [self objectAtIndex:index];
}

@end
