//
//  UIViewController+IsLoading.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/7/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (IsLoading)

@property (assign) BOOL isLoading;
@property (nonatomic,retain) UIView* isLoadingTargetView;

@end
