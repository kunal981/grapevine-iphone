
//
//  NSMutableDictionary+Subscript.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "NSMutableDictionary+Subscript.h"

@implementation NSMutableDictionary (Subscript)

- (void) setObject:(id)obj forKeyedSubscript:(id)key {
  [self setObject:obj forKey:key];
}

@end
