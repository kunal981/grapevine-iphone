//
//  NSObject+Introspection.h
//  GrapeVine
//
//  Created by Zachary Gavin on 9/24/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Introspection)

+ (NSArray*) propertyNames;
+ (BOOL) isPropertyDefined:(NSString*)name;
+ (BOOL) isPropertyWriteable:(NSString*)name;
+ (NSString*) propertyType:(NSString*)name;
+ (Class) propertyClass:(NSString*)name;

- (NSArray*) propertyNames;
- (BOOL) isPropertyDefined:(NSString*)name;
- (BOOL) isPropertyWriteable:(NSString*)name;
- (NSString*) propertyType:(NSString*)name;
- (Class) propertyClass:(NSString*)name;

@end
