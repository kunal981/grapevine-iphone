//
//  NSMutableArray+Subscript.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (Subscript)

- (void) setObject: (id) obj atIndexedSubscript:(NSUInteger)index;

@end
