//
//  NSMutableArray+Subscript.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "NSMutableArray+Subscript.h"

@implementation NSMutableArray (Subscript)

- (void) setObject: (id) obj atIndexedSubscript:(NSUInteger)index {
  if (index < self.count){
    if (obj)
      [self replaceObjectAtIndex:index withObject:obj];
    else
      [self removeObjectAtIndex:index];
  } else {
    [self addObject:obj];
  }
}

@end
