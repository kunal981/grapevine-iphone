//
//  NSURLConnection+AsynchronousRequest.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/17/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface NSURLConnection (AsynchronousRequest)

+ (void) asyncSimple:(NSString*) url callback:(void(^)(NSData *, NSURLResponse*))block;
+ (void) asyncSimple:(NSString*) url success:(void(^)(NSData *, NSURLResponse*))successBlock failure:(void(^)(NSData *, NSError *))failureBlock;
+ (void) asyncRequest:(NSURLRequest *)request success:(void(^)(NSData *, NSURLResponse *))successBlock failure:(void(^)(NSData *, NSError *))failureBlock;

@end