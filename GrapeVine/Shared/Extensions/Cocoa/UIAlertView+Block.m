//
//  UIAlertView+Block.m
//  GrapeVine
//
//  Created by Zachary Gavin on 7/22/13.
//  Copyright (c) 2013 com.grape-vine. All rights reserved.
//

#import "UIAlertView+Block.h"
#import <objc/runtime.h>

static char DISMISS_IDENTIFER;

@implementation UIAlertView (Block)

@dynamic dismissBlock;

- (void) setDismissBlock:(DismissBlock)dismissBlock {
	objc_setAssociatedObject(self, &DISMISS_IDENTIFER, dismissBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (DismissBlock) dismissBlock {
	return objc_getAssociatedObject(self, &DISMISS_IDENTIFER);
}

+ (UIAlertView*) alertViewWithTitle:(NSString*)title message:(NSString*) message cancelButtonTitle:(NSString*)cancelButtonTitle otherButtonTitles:(NSArray*)otherButtonTitles onDismiss:(DismissBlock)dismissed  {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:[self class] cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil];
	
	for(NSString* title in otherButtonTitles) [alert addButtonWithTitle:title];
	
	[alert setDismissBlock:dismissed];
	
	[alert show];
	
	return alert;
}

+ (void) alertView:(UIAlertView*) alertView didDismissWithButtonIndex:(NSInteger) buttonIndex {
	if( alertView.dismissBlock ) alertView.dismissBlock(alertView, buttonIndex);
}

@end