//
//  NSURLConnection+AsynchronousRequest.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/17/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "NSURLConnection+AsynchronousRequest.h"

@implementation NSURLConnection (AsynchronousRequest)


+ (void) asyncSimple:(NSString*) url callback:(void(^)(NSData *, NSURLResponse*))block {
	[NSURLConnection asyncSimple:url success:block failure:nil];
}

+ (void) asyncSimple:(NSString*) url success:(void(^)(NSData *, NSURLResponse*))successBlock failure:(void(^)(NSData *, NSError *))failureBlock {
	NSURLRequest* request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10.0];
	[NSURLConnection asyncRequest:request success:successBlock failure:failureBlock];
}

+ (void) asyncRequest:(NSURLRequest *)request success:(void(^)(NSData *, NSURLResponse *))successBlock failure:(void(^)(NSData *, NSError *))failureBlock {
	
	dispatch_queue_t queue = dispatch_get_current_queue();
	NSLog(@"Async URL Request: %@", request.URL.absoluteString);
	
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
    NSURLResponse *response = nil;
    NSError *error = nil;
		
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
		
    dispatch_async(queue, ^{
			if (!data) {
				if(failureBlock) failureBlock(data,error);
			} else {
				successBlock(data,response);
			}
		});
	});
}

@end
