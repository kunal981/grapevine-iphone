//
//  NSNull+Conversions.h
//  GrapeVine
//
//  Created by Zachary Gavin on 9/21/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNull (Conversions)

- (double) doubleValue;
- (NSInteger) intValue;

@end
