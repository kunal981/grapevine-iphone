//
//  UIAlertView+Block.h
//  GrapeVine
//
//  Created by Zachary Gavin on 7/22/13.
//  Copyright (c) 2013 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^DismissBlock)(UIAlertView* alertView, int buttonIndex);

@interface UIAlertView (Block) <UIAlertViewDelegate>

+ (UIAlertView*) alertViewWithTitle:(NSString*)title message:(NSString*) message cancelButtonTitle:(NSString*)cancelButtonTitle otherButtonTitles:(NSArray*)otherButtonTitles onDismiss:(DismissBlock)dismissed;

@property (nonatomic, copy) DismissBlock dismissBlock;

@end
