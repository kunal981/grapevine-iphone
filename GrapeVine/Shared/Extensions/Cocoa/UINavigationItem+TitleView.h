//
//  UINavigationItem+TitleView.h
//  GrapeVine
//
//  Created by Zachary Gavin on 7/17/13.
//  Copyright (c) 2013 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationItem (TitleView)

@end
