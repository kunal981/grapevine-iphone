//
//  NSArray+Enumerable.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <Foundation/Foundation.h>

extern const NSInteger NSArrayFlattenDepthInfinite;

@interface NSArray (Enumerable)

- (id) find:(BOOL(^)(id)) blk;
- (NSArray*) select:(BOOL(^)(id)) blk;
- (NSArray*) selectClass:(Class) klass;
- (NSArray*) map:(id(^)(id)) blk;
- (NSArray*) mapKey:(NSString*)key;
- (id) inject:(id) value withBlock:(id(^)(id,id)) blk;
- (BOOL) any:(BOOL(^)(id)) blk;
- (BOOL) all:(BOOL(^)(id)) blk;
- (NSArray*) flatten;
- (NSArray*) flattenToDepth:(int) depth;
- (NSArray*) compact;

@end
