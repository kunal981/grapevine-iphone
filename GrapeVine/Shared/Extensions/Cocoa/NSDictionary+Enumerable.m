//
//  NSDictionary+Enumerable.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/11/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "NSDictionary+Enumerable.h"
#import "NSDictionary+Subscript.h"

@implementation NSDictionary (Enumerable)

- (BOOL) any:(BOOL(^)(id,id)) blk {
  for(id key in self.allKeys) {
    if (blk(key,self[key])) {
      return YES;
    }
  }
  return NO;
}

- (BOOL) all:(BOOL(^)(id,id)) blk {
  for(id key in self.allKeys) {
    if (!blk(key,self[key])) {
      return NO;
    }
  }
  return YES;
}

- (NSDictionary*) merge:(NSDictionary *)other {
  NSMutableDictionary* result = [self mutableCopy];
  [other enumerateKeysAndObjectsUsingBlock:^(id key,id value,BOOL* stop) {
    result[key] = value;
  }];
  
  return result;
}

@end
