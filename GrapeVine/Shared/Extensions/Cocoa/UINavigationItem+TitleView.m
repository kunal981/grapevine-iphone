//
//  UINavigationItem+TitleView.m
//  GrapeVine
//
//  Created by Zachary Gavin on 7/17/13.
//  Copyright (c) 2013 com.grape-vine. All rights reserved.
//

#import "UINavigationItem+TitleView.h"
#import <objc/runtime.h>
#import <objc/message.h>

const char* DEFAULT_TITLE_VIEW_KEY = "DEFAULT_TITLE_VIEW_KEY";

@implementation UINavigationItem (TitleView)

+ (void) initialize {
  Method origMethod = class_getInstanceMethod([UINavigationItem class], @selector(title));
  Method newMethod = class_getInstanceMethod([UINavigationItem class], @selector(titleWithDefaultIfNoTitle));
  method_exchangeImplementations(origMethod, newMethod);
  
	origMethod = class_getInstanceMethod([UINavigationItem class], @selector(setTitle:));
  newMethod = class_getInstanceMethod([UINavigationItem class], @selector(setTitleWithDefaultView:));
  method_exchangeImplementations(origMethod, newMethod);
}

static UIImage* defaultTitleImage;

- (UIImageView*) defaultTitleView {
	UIImageView* view = objc_getAssociatedObject(self, DEFAULT_TITLE_VIEW_KEY);
	
	if ( !view ) {
		if( !defaultTitleImage ) defaultTitleImage = [UIImage imageNamed:@"n.png"];
		
		view = [[UIImageView alloc] initWithImage:defaultTitleImage];
		objc_setAssociatedObject(self, DEFAULT_TITLE_VIEW_KEY, view, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
	}
	
	return view;
}

- (void) setTitleWithDefaultView:(NSString*)title {
	[self setTitleWithDefaultView:title];

	if( !title && !self.titleView ) {
		self.titleView = self.defaultTitleView;
	} else if (title && self.titleView == self.defaultTitleView) {
		self.titleView = nil;
	}
}

- (UIView*) titleWithDefaultIfNoTitle {
	if( !self.titleWithDefaultIfNoTitle && !self.titleView ) self.titleView = self.defaultTitleView;
	
	return self.titleWithDefaultIfNoTitle;
}

@end
