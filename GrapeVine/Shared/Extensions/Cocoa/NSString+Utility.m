	//
//  NSString+Utility.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/16/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "NSString+Utility.h"

@implementation NSString (Utility)

- (NSString*) classify {
  return [self classify:NO];
}

- (NSString*) classify:(BOOL)capitalizeFirst {
  NSMutableString* ret = [NSMutableString stringWithString:@""];
  
  NSScanner* scanner = [NSScanner scannerWithString:self];
  while (!scanner.isAtEnd) {
    NSString* tmp;
    
    
    [scanner scanUpToString:@"_" intoString:&tmp];
    
    if(ret.length > 0 || capitalizeFirst ) {
      if(tmp.length > 0) [ret appendString:[tmp substringToIndex:1].uppercaseString];
      if(tmp.length > 1) [ret appendString:[tmp substringFromIndex:1]];
    } else {
      [ret appendString:tmp];
    }
    if(!scanner.isAtEnd) scanner.scanLocation = scanner.scanLocation+1;
  }
  return ret;
}

- (NSString*) underscore {
  NSRegularExpression* expression = [NSRegularExpression regularExpressionWithPattern:@"([^\\A])([A-Z])" options:0 error:nil];
  
  return [expression stringByReplacingMatchesInString:self options:0 range:NSMakeRange(0, self.length) withTemplate:@"$1_$2"].lowercaseString;
}

- (BOOL)startsWith:(NSString *)start {
  return start.length <= self.length && [[self substringToIndex:start.length] isEqualToString:start];
}

- (BOOL)endsWith:(NSString *)end {
  return end.length <= self.length && [[self substringFromIndex:self.length-end.length] isEqualToString:end];
}

@end
