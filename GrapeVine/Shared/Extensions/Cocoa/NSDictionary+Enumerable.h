//
//  NSDictionary+Enumerable.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/11/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Enumerable)

- (BOOL) any:(BOOL(^)(id,id)) blk;
- (BOOL) all:(BOOL(^)(id,id)) blk;
- (NSDictionary*) merge:(NSDictionary*)other;


@end
