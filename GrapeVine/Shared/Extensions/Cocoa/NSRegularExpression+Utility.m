//
//  NSRegularExpression+Utility.m
//  GrapeVine
//
//  Created by Zachary Gavin on 9/27/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "NSRegularExpression+Utility.h"
#import "NSArray+Enumerable.h"

@implementation NSRegularExpression (Utility)

- (NSArray*) matchesInString:(NSString *)string {
  NSTextCheckingResult* result = [self firstMatchInString:string options:0 range:NSMakeRange(0, string.length)];
  
  NSMutableArray* ret = [@[] mutableCopy];
  
  for(int i=1;i<result.numberOfRanges;i++) {
    [ret addObject:[string substringWithRange:[result rangeAtIndex:i]]];
  }
  
  return ret ;
}

@end
