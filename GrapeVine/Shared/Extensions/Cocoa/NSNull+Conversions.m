//
//  NSNull+Conversions.m
//  GrapeVine
//
//  Created by Zachary Gavin on 9/21/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "NSNull+Conversions.h"

@implementation NSNull (Conversions)

- (double) doubleValue { return 0.0; }
- (NSInteger) intValue { return 0; }

@end
