//
//  NSArray+Enumerable.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "NSArray+Enumerable.h"

const NSInteger NSArrayFlattenDepthInfinite = -2;

@implementation NSArray (NSArray_Enumerable)


- (id) find:(BOOL(^)(id)) blk {
  for(id obj in self) {
    if(blk(obj)) return obj;
  }
  return nil;
}

- (NSArray*) select:(BOOL(^)(id)) blk {
  NSMutableArray * retArray = [NSMutableArray array];
  for(id obj in self) {
    if(blk(obj)) [retArray addObject:obj];
  }
  return retArray;
}

- (NSArray*) selectClass:(Class) klass {
  return [self select:^(id obj){
    return [obj isKindOfClass:klass];
  }];
}

- (NSArray*) map:(id(^)(id)) blk {
    
    NSLog(@"%@",blk);
  NSMutableArray * retArray = [NSMutableArray arrayWithCapacity:self.count];
  for(id obj in self)
  {
		id ret =  blk(obj) ;
    [retArray addObject:(ret == nil) ? [NSNull null]:ret];
  }
  return retArray;
}

- (NSArray*) mapKey:(NSString *)key {
  NSMutableArray * retArray = [NSMutableArray arrayWithCapacity:self.count];
  for(id obj in self) {
		id ret = [obj valueForKey:key];
    [retArray addObject:(ret == nil) ? [NSNull null]:ret];
  }
  return retArray;
}

- (id) inject:(id) value withBlock:(id(^)(id,id)) blk {
  for(id obj in self) {
    value = blk(value,obj);
  }
  return value;
}

- (BOOL) any:(BOOL(^)(id)) blk {
  for(id obj in self) {
    if(blk(obj)) return YES;
  }
  return NO;
}

- (BOOL) all:(BOOL(^)(id)) blk {
  for(id obj in self) {
    if(!blk(obj)) return NO;
  }
  return YES;
}

- (NSArray*) flatten {
  return [self flattenToDepth:1];
}

- (NSArray*) flattenToDepth:(int)depth {
  NSMutableArray* array = [@[] mutableCopy];
  for(id obj in self) {
    if([obj isKindOfClass:[NSArray array]]) {
      NSArray* tmp = (NSArray*) obj;
      if (depth - 1 > 0) {
        tmp = [tmp flattenToDepth:depth-1];
      }
			[array addObjectsFromArray:tmp];
    } else {
      [array addObject:obj];
    }
  }

  return array;
}

- (NSArray*) compact {
  return [self select:^(id obj) { return (BOOL) ![obj isKindOfClass:[NSNull class]]; }];
}

@end
