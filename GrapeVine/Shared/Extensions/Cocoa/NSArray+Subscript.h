//
//  NSArray+Subscript.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Subscript)

- (id) objectAtIndexedSubscript:(NSUInteger)index;

@end
