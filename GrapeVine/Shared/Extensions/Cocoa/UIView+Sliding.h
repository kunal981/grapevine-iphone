//
//  UIView+Sliding.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/24/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Sliding)

- (void) slideIn;
- (void) slideOut;
- (void) slideToggle;

@end
