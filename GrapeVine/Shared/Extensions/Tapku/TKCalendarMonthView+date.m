//
//  TKCalendarMonthView+date.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/24/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "TKCalendarMonthView+date.h"
#import "NSString+Utility.h"


@implementation TKCalendarMonthView (date)
/*
  For some reason tapku calculates everything as GMT, so we have to translate going in and out
*/
- (NSDate*) date {
  return [self.dateSelected dateByAddingTimeInterval:-1*[[NSTimeZone localTimeZone] secondsFromGMT]];
}

- (void) setDate:(NSDate *)date {
  [self selectDate:[date dateByAddingTimeInterval:[[NSTimeZone localTimeZone] secondsFromGMT]]];
}

@end
