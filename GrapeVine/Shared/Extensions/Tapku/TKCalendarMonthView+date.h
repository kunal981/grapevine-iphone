//
//  TKCalendarMonthView+date.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/24/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <TapkuLibrary/TapkuLibrary.h>

@interface TKCalendarMonthView (date)

@property (nonatomic,retain) NSDate* date;

@end
