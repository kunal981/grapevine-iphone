//
//  Sharer.h
//  GrapeVine
//
//  Created by Zachary Gavin on 11/6/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GVObject.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#include <FacebookSDK/FacebookSDK.h>

@interface Sharer : NSObject <UIActionSheetDelegate,MFMailComposeViewControllerDelegate>

+ (Sharer*) sharerWithObject:(GVObject*)object;

- (void) shareFromController:(UIViewController*)controller;

@property (nonatomic,strong) GVObject* object;
@property (readonly) UIViewController* controller;

@end
