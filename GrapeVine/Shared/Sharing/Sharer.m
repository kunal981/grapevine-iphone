//
//  Sharer.m
//  GrapeVine
//
//  Created by Zachary Gavin on 11/6/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "Sharer.h"
#import "AppDelegate.h"
#import <Twitter/Twitter.h>
#import <Social/Social.h>
#import "User.h"
#import "Event.h"
#import "LocationManager.h"


@implementation Sharer

static NSMutableArray* sharers;
+ (NSMutableArray*) sharers {
  if(!sharers) sharers = [@[] mutableCopy];
  return sharers;
}

+ (Sharer*) sharerWithObject:(GVObject *)object {
  Sharer* sharer = [[self alloc] init];
  sharer.object = object;
  
  return sharer;
}

- (id) init {
  if(self = [super init]) {
    [[[self class] sharers] addObject:self];
  }
  
  return self;
}

- (void) remove {
  [[[self class] sharers] removeObject:self];
}

- (NSString*) url {
  return [self.object valueForKey:([self.object isKindOfClass:[Event class]] ? @"publicUrl" : @"websiteUrl")];
}

- (void) trackWithAction:(NSString*)action {
  [[LocationManager sharedLocationManager] fetchLocation];
  [User.currentUser trackAction:action withData:self.object.key];
}

- (void) shareFromController:(UIViewController*)controller {
  _controller = controller;
  UIActionSheet* actionSheet = [[UIActionSheet alloc] initWithTitle:@"Share" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Facebook",@"Twitter",@"Email", nil];
  actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent ;

  [actionSheet showInView:controller.view.window];
}

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
  if(buttonIndex == 0) {
    if (NSClassFromString(@"SLComposeViewController") && [SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook] && [FBSession activeSession].loginType == FBSessionLoginTypeSystemAccount) {
      SLComposeViewController* facebookSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
      [facebookSheet addURL:[NSURL URLWithString:self.url]];
      [facebookSheet setInitialText:@"Check this out..."];
      facebookSheet.completionHandler = ^(SLComposeViewControllerResult result) {
        if(result == SLComposeViewControllerResultDone) [self trackWithAction:@"FacebookShare"];
        [self remove];
      };
      [self.controller presentViewController:facebookSheet animated:YES completion:nil];
    } else {
      NSDictionary *params = @{@"message":@"Check this out...",@"link":self.url};
      
    //  [AppDelegate.currentDelegate.facebook dialog:@"feed" andParams:[params mutableCopy] andDelegate:self];
    }
  } else if (buttonIndex == 1) {

    TWTweetComposeViewController *tweetSheet = [[TWTweetComposeViewController alloc] init];
    [tweetSheet setInitialText:@"Check this out..."];
    [tweetSheet addURL:[NSURL URLWithString:self.url]];
    tweetSheet.completionHandler = ^(TWTweetComposeViewControllerResult result) {
      if(result == TWTweetComposeViewControllerResultDone) [self trackWithAction:@"TwitterShare"];
      [self.controller dismissModalViewControllerAnimated:NO];
      [self remove];
      
    };
    
    [self.controller presentModalViewController:tweetSheet animated:YES];
    
  } else if (buttonIndex == 2) {
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    
    controller.mailComposeDelegate = self;
    
    NSString* body = [[NSString alloc] initWithData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"friend_share" ofType:@"htm"]] encoding:NSUTF8StringEncoding];
    
    body = [NSString stringWithFormat:body,self.url,User.currentUser.firstName,User.currentUser.lastName];
    
    NSString* type;
    
    if([self.object isKindOfClass:[Event class]]) {
      type = ((Event*) self.object).isContent ? @"article" : @"event";
    } else {
      type = @"organization";
    }
    
    [controller setSubject:[NSString stringWithFormat:@"Check out this %@ from GrapeVine",type]];
    
    [controller setMessageBody:body isHTML:YES];
    
    [self.controller presentModalViewController:controller animated:YES];
  } else {
    [self remove];
  }
}

- (void) actionSheetCancel:(UIActionSheet *)actionSheet {
  [self remove];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
  if(result == MFMailComposeResultSaved || result == MFMailComposeResultSent) {
    [self trackWithAction:@"EmailShare"];
  }
  
  [self.controller dismissModalViewControllerAnimated:YES];
  [self remove];
}

//- (void)dialogDidComplete:(FBDialog *)dialog {
//  [self trackWithAction:@"FacebookShare"];
//  dialog.delegate = nil;
//  [self remove];
//}
//
//- (void)dialogDidNotComplete:(FBDialog *)dialog {
//  dialog.delegate = nil;
//  [self remove];
//}
//
//- (void)dialog:(FBDialog*)dialog didFailWithError:(NSError *)error {
//  dialog.delegate = nil;
//  [self remove];
//}



@end
