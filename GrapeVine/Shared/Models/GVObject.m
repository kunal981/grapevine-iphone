//
//  GVObject.m
//  GrapeVine
//
//  Created by Jack Kustanowitz on 9/13/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "GVObject.h"
#import "NSDictionary+Subscript.h"
#import "NSMutableDictionary+Subscript.h"
#import "NSString+Utility.h"
#import "NSObject+Introspection.h"

@implementation GVObject

- (id) initWithKey:(id)key {
	if(self = [super init]) {
		_key = key;
		[[self class] allDict][_key] = self;
	}
	
	return self;
}

static NSMutableDictionary* allDict;

+ (NSArray*) all {
  return [self allDict].allValues;
}

+ (NSMutableDictionary*) allDict {
  if(!allDict) allDict = [@{} mutableCopy];
  NSString* name = NSStringFromClass(self);
  if(!allDict[name]) allDict[name] = [@{} mutableCopy];
	return allDict[name];
}

+ (void) resetCache {
  if([NSStringFromClass(self) isEqualToString:@"GVObject"]) {
    allDict = [@{} mutableCopy];
  } else {
    NSLog(@"reseting cache for: %@",NSStringFromClass(self));
    allDict[NSStringFromClass(self)] = [@{} mutableCopy];
  }
}

+ (GVObject*) objectWithKey:(id)key {
  if ([self allDict][key]) return [self allDict][key];

  GVObject* obj = [[self alloc] initWithKey:key];
  [obj afterInitialize];
  return obj;
}

+ (GVObject*) objectWithDictionary:(NSDictionary *)dict {
	GVObject* obj = [self objectWithKey:dict[self.keyName]];
	[obj loadDataFromDictionary:dict];
	return obj;
}

+ (NSString*) keyName {
  return @"id";
}

+ (NSDictionary*) keysForUpdateWithDictionary {
  return @{};
}

+ (NSTimeInterval) expirationInterval {
  return 60 * 60 * 24 * 7;	// 1 week
}

- (void) afterInitialize {
  
}

- (void) loadDataFromDictionary:(NSDictionary*) dict {
  
  NSLog(@"dict is = %@",dict);
  [dict enumerateKeysAndObjectsUsingBlock:^(NSString* key,id obj,BOOL* stop){
    NSString* attribute = [key classify];
    if(obj != [NSNull null] && [self isPropertyWriteable:attribute]) {
      if([[self propertyType:attribute] isEqualToString:@"@"]) {
        Class klass = [self propertyClass:attribute];
        if([obj isKindOfClass:klass]) {
          [self setValue:obj forKey:attribute];
        } else if (klass == [NSDate class] && [obj isKindOfClass:[NSString class]]) {
          [self setValue:[NSDate dateWithTimeIntervalSince1970:((NSString*) obj).intValue] forKey:attribute];
        }
      } else {
        [self setValue:obj forKey:attribute];
      }
    }
  }];
  
  [[[self class] keysForUpdateWithDictionary] enumerateKeysAndObjectsUsingBlock:^(NSString* attribute,NSString* key,BOOL* stop) {
    [self setValue:dict[key] forKey:attribute];
  }];
  
  _retrievedAt = [NSDate date];
}

- (BOOL) isExpired {
  return !self.retrievedAt || [[self.retrievedAt dateByAddingTimeInterval:self.class.expirationInterval] compare:[NSDate date]] == NSOrderedAscending;
}

- (NSString*) description {
  NSMutableArray* properties = [@[] mutableCopy];
  for(NSString* name in [self propertyNames]) {
    if (![[self propertyType:name] isEqualToString:@"@"] || [@[[NSString class],[NSNumber class]] indexOfObject:[self propertyClass:name]] != NSNotFound) {
      [properties addObject:[NSString stringWithFormat:@"%@=%@",name,[self valueForKey:name]]];
    }
  }
  
  return [NSString stringWithFormat:@"<%@: %p key=%@ %@>",NSStringFromClass([self class]),self,self.key,[properties componentsJoinedByString:@" "]];
}

@end
