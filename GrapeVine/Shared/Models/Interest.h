//
//  Interest.h
//  GrapeVine
//
//  Created by Jack Kustanowitz on 9/13/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GVObject.h"

@interface Interest : GVObject <NamedGVObject>

@end
