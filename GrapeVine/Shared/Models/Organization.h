//
//  Organization.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GVObject.h"

@interface Organization : GVObject <NamedGVObject>

@property (nonatomic,retain) NSString* summary;
@property (nonatomic,retain) NSString* primaryContact;
@property (nonatomic,retain) NSString* email;
@property (nonatomic,retain) NSString* phone;
@property (nonatomic,retain) NSString* facebookUrl;
@property (nonatomic,retain) NSString* websiteUrl;
@property (nonatomic,retain) NSString* twitterHashtag;
@property (nonatomic,retain) NSString* addressLine1;
@property (nonatomic,retain) NSString* city;
@property (nonatomic,retain) NSString* state;
@property (nonatomic,retain) NSString* postalcode;
@property (nonatomic,retain) NSString* logoUrl;
@property (readonly) NSArray* events;
@property (readonly) NSArray* interests;
@property (readonly) NSArray* communities;

- (void) updateWithCompletionBlock:(void(^)(NSError*))callback;

@end
