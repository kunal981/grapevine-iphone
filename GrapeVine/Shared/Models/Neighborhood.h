//
//  Neighborhood.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/4/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "GVObject.h"

@class Community;

extern NSString* const kNeighborhoodParamsCommunityId;

@interface Neighborhood : GVObject <NamedGVObject> {
  Community* _community;
}

@property (readonly) Community* community;


@end
