//
//  GVObject.h
//  GrapeVine
//
//  Created by Jack Kustanowitz on 9/13/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol NamedGVObject <NSObject>

@property (nonatomic,strong) NSString* name;

@end


@interface GVObject : NSObject

@property (readonly) id key;
@property (readonly) NSDate* retrievedAt;
@property (readonly) BOOL isExpired;

+ (GVObject*) objectWithKey:(id)key;
+ (GVObject*) objectWithDictionary:(NSDictionary*)dict;
+ (NSArray*) all;
+ (void) resetCache;

+ (NSString*) keyName;
+ (NSDictionary*) keysForUpdateWithDictionary;
+ (NSTimeInterval) expirationInterval;

- (void) afterInitialize;
- (void) loadDataFromDictionary:(NSDictionary*)dict;

@end
