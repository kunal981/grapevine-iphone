//
//  Organization.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "Organization.h"
#import "APIWrapper.h"
#import "Event.h"
#import "Interest.h"

static NSString* OrganizationParamsEvents = @"upcoming_events";
static NSString* OrganizationParamsInterests = @"interests";
static NSString* OrganizationParamsCommunities = @"communities";

@implementation Organization

@synthesize name;

+ (NSDictionary*) keysForUpdateWithDictionary {
  return @{@"summary":@"description"};
}

- (void) afterInitialize {
  _communities = @[];
}

- (void) loadDataFromDictionary:(NSDictionary *)dict {
  [super loadDataFromDictionary:dict];
  
  NSMutableArray* events = [@[] mutableCopy];
  for(NSDictionary* eventDict in dict[OrganizationParamsEvents]) {
    [events addObject:[Event objectWithDictionary:eventDict]];
  }
  
  _events = [NSArray arrayWithArray:events];
  
  if(dict[OrganizationParamsInterests]) {
    NSMutableArray* interests = [@[] mutableCopy];
    for(NSNumber* key in dict[OrganizationParamsInterests]) {
      Interest* interest = (Interest*) [Interest objectWithKey:key];
      [interests addObject:interest];
    }
    
    _interests = [NSArray arrayWithArray:interests];
  }
  
  if(dict[OrganizationParamsCommunities]) {
    NSMutableArray* communities = [@[] mutableCopy];
    for(NSNumber* key in dict[OrganizationParamsCommunities]) {
      Community* community = (Community*) [Community objectWithKey:key];
      [communities addObject:community];
    }
    
    _communities = [NSArray arrayWithArray:communities];
  }
}

- (BOOL) update:(NSError**) error {
  NSDictionary* dict = [APIWrapper.api organizationGetDetails:self error:error];
  [self loadDataFromDictionary:dict];
  return *error == nil;
}

- (void) updateWithCompletionBlock:(void(^)(NSError*))callback {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    NSError* error ;
    
    [self update:&error];
    
    if(callback) {
      dispatch_async(dispatch_get_main_queue(), ^{
        callback(error);
      });
    }
  });
}

@end
