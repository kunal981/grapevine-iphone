//
//  Event.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "Event.h"
#import "NSString+Utility.h"
#import "APIWrapper.h"
#import "NSArray+Subscript.h"
#import "NSDictionary+Subscript.h"
#import "NSMutableDictionary+Subscript.h"
#import "NSArray+Enumerable.h"
#import "NSRegularExpression+Utility.h"
#import "User.h"
#import <EventKit/EventKit.h>

NSString* const EventTypeRecommended = @"recommended";
NSString* const EventTypeFriends = @"friends";
NSString* const EventTypeRegistered = @"registered";
NSString* const EventTypeNearby = @"nearby";
NSString* const EventTypeSponsored = @"sponsored";
NSString* const EventTypeAll = @"all";

NSString* const EventParamsType = @"type";
NSString* const EventParamsFilterStartDate = @"filter_start_date";
NSString* const EventParamsFilterEndDate = @"filter_end_date";
NSString* const EventParamsFilterOrganizationId = @"filter_organization_id";
NSString* const EventParamsFilterNeighborhoodId = @"filter_neighborhood_id";
NSString* const EventParamsFilterInterest = @"filter_interests";
NSString* const EventParamsFilterCommunity = @"filter_community_id";
NSString* const EventParamsFilterId = @"filter_id";
NSString* const EventParamsFilterTerm = @"filter_term";
NSString* const EventParamsGroupByStartDay = @"group_by_start_day";
NSString* const EventParamsSort = @"sort";
NSString* const EventParamsSortOrder = @"sort_order";
NSString* const EventParamsLon = @"lon";
NSString* const EventParamsLat = @"lat";
NSString* const EventParamsRadiusMeter = @"radius_meters";
NSString* const EventParamsZipcode = @"postalcode";

NSString* const EventFeedbackOverall = @"rating_overall";
NSString* const EventFeedbackWelcomed= @"rating_welcomed_by_leadership";
NSString* const EventFeedbackConnections = @"rating_made_connections";
NSString* const EventFeedbackInvolvement = @"rating_encouraged_involvement";
NSString* const EventFeedbackResonated = @"rating_content_resonated";
NSString* const EventFeedbackRecommend = @"rating_would_recommend";
NSString* const EventFeedbackComments = @"feedback_comments";

const NSArray* EventFeedbackRatings;


NSString* const EventActionLike = @"like";
NSString* const EventActionUnlike = @"unlike";
NSString* const EventActionNotLike = @"notlike";
NSString* const EventActionUnnotLike = @"unnotlike";
NSString* const EventActionRegister = @"register";
NSString* const EventActionUnregister = @"unregister";
NSString* const EventActionBookmark = @"bookmark";
NSString* const EventActionUnbookmark = @"unbookmark";
NSString* const EventActionFeedback = @"feedback";

NSString* const EventTypeContent = @"content";
NSString* const EventTypeEvent = @"event";


@implementation Event

+ (void) initialize {
  EventFeedbackRatings = @[EventFeedbackOverall,EventFeedbackWelcomed,EventFeedbackConnections,EventFeedbackInvolvement,EventFeedbackResonated,EventFeedbackRecommend];
}

- (void) afterInitialize {
  [super afterInitialize];
  _feedback = [@{} mutableCopy];
  for (NSString* key in EventFeedbackRatings) _feedback[key] = @(0);
  _feedback[EventFeedbackComments] = @"";
}

- (void) loadDataFromDictionary:(NSDictionary*)item {
        NSLog(@"item is = %@",item);
  loadedFromDict = NO;
  [super loadDataFromDictionary:item];

  self.organization = (Organization*) [Organization objectWithDictionary:item[@"organization"]];
  self.sponsoring_organization = (Organization*) [Organization objectWithKey:item[@"sponsoring_organization"][@"id"]];
  
  self.neighborhood = (Neighborhood*) [Neighborhood objectWithKey:[item valueForKeyPath:@"neighborhood.id"]];
	
	// add "Posted at" to the text of an article. Easier than doing it in a separate control and rejiggering everything
	NSString* description = self.description;
	if ([self isContent] && self.createdTime) {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd, yyyy"];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
		self.description = [NSString stringWithFormat:@"%@\n\nPosted %@", description, [formatter stringFromDate:self.createdTime]];
	}
  
  loadedFromDict = YES;
}

+ (NSDictionary*) keysForUpdateWithDictionary {
  return @{@"zipcode":EventParamsZipcode};
}

+ (void) eventsWithCompletionBlock:(EventCallback)callback fromBlock:(NSDictionary*(^)(NSError**))block {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
		NSError* error;
		NSDictionary* eventDictFromApi = block(&error);
		NSMutableDictionary* eventsByTypeDict = [[NSMutableDictionary alloc] init];
    
		for (NSString* eventType in eventDictFromApi.allKeys) {
      if(eventDictFromApi[eventType] == [NSNull null]) continue;
      eventsByTypeDict[eventType] = [eventDictFromApi[eventType] map:^(NSDictionary* eventDict){ return [Event objectWithDictionary:eventDict];}];
		}
		
		dispatch_async(dispatch_get_main_queue(), ^{
			callback(eventsByTypeDict, error);
		});
	});
}

+ (void) savedEventsForUser:(User*)user withCompletionBlock:(EventCallback)callback {
  
}

+ (void) recommendedEventsForUser:(User*)user withCompletionBlock:(EventCallback)callback {
  NSLog(@"%@",callback);
	[self eventsWithCompletionBlock:callback fromBlock:^(NSError** error){
    NSLog(@"%@",user);
    return [APIWrapper.api userGetRecommendedEvents:user error:error];
  }];
}

+ (void) nearbyEventsForUser:(User*)user withCompletionBlock:(EventCallback)callback {
  [self eventsWithCompletionBlock:callback fromBlock:^(NSError** error){
    return [APIWrapper.api userGetNearbyEvents:user error:error];
  }];
}

+ (void) eventsForUser:(User*)user fromDate:(NSDate*)startDate toDate:(NSDate*)endDate withCompletionBlock:(EventCallback)callback {
  
  NSLog(@"%@,%@",startDate,endDate);
  
  [self eventsWithCompletionBlock:callback fromBlock:^(NSError** error)
  {
    return [APIWrapper.api userGetEvents:user fromDate:startDate toDate:endDate error:error];
  }];
}

+ (void) eventsForUser:(User*)user byNeighborhood:(Neighborhood*)neighborhood withCompletionBlock:(EventCallback)callback {
  [self eventsWithCompletionBlock:callback fromBlock:^(NSError** error){
    return [APIWrapper.api userGetEvents:user byNeighborhood:neighborhood error:error];
  }];
}

+ (void) eventsForUser:(User*)user byCommunity:(Community*)community withCompletionBlock:(EventCallback)callback {
  [self eventsWithCompletionBlock:callback fromBlock:^(NSError** error){
    return [APIWrapper.api userGetEvents:user byCommunity:community error:error];
  }];
}

+ (void) eventsForUser:(User*)user byOrganization:(Organization*)organization withCompletionBlock:(EventCallback)callback {
  [self eventsWithCompletionBlock:callback fromBlock:^(NSError** error){
    return [APIWrapper.api userGetEvents:user byOrganization:organization error:error];
  }];
}

+ (void) eventsForUser:(User*)user bySearchTerm:(NSString*)searchTerm withCompletionBlock:(EventCallback)callback {
  [self eventsWithCompletionBlock:callback fromBlock:^(NSError** error){
    return [APIWrapper.api userGetEvents:user bySearchTerm:searchTerm error:error];
  }];	
}

+ (void) eventsForUser:(User*)user byInterest:(Interest*)interest withCompletionBlock:(EventCallback)callback {
  [self eventsWithCompletionBlock:callback fromBlock:^(NSError** error){
    return [APIWrapper.api userGetEvents:user byInterest:interest error:error];
  }];
}

+ (void) eventsForUser:(User*)user byIds:(NSArray*)ids withCompletionBlock:(EventCallback)callback {
  
  NSLog(@"ids is = %@",ids);
  
  [self eventsWithCompletionBlock:callback fromBlock:^(NSError** error){
    return [APIWrapper.api userGetEvents:user byIds:ids error:error];
  }];
}

+ (void) groupedEventsForUser:(User*)user withCompletionBlock:(EventCallback)callback {

  
  NSLog(@"user is = %@",user);
  NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
  formatter.locale = [NSLocale currentLocale];
  formatter.dateFormat = @"M/d/yy";
  
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
		NSError* error;
		NSArray* dates = [APIWrapper.api userGetGroupedEvents:user error:&error];
    NSMutableDictionary* datesDict = [@{} mutableCopy];
    if (!error) {
      for(NSDictionary* dict in dates) {
        if (((NSNumber*)dict[@"count"]).intValue) {
          NSRegularExpression* regexp = [NSRegularExpression regularExpressionWithPattern:@"\\/Date\\((\\d+)(-\\d+)\\)\\/" options:0 error:&error];
          NSArray* matches = [regexp matchesInString:dict[@"date"]];
          
          NSTimeInterval timestamp = ((NSString*) matches[0]).doubleValue/1000;
          
          NSDate* date = [NSDate dateWithTimeIntervalSince1970:timestamp];
          
          NSString* dateString = [formatter stringFromDate:date];
          datesDict[dateString] = dict[@"count"];
        }
      }
    }

		dispatch_async(dispatch_get_main_queue(), ^{
			callback(datesDict, error);
		});
	});
}

- (void) action:(NSString*)action {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    NSError* error;
    [APIWrapper.api userAction:action forUser:User.currentUser andEvent:self error:&error];
  });
}

- (void) setUserLike:(BOOL)userLike {
  [self setUserLike:userLike sync:NO];
}

- (void) setUserLike:(BOOL)userLike sync:(BOOL)sync {
  _userLike = userLike;
  if(userLike) _userNotLike = NO;
  if(sync) [self action:(userLike ? EventActionLike : EventActionUnlike )];
  if (loadedFromDict) {
    
    if(userLike) [User.currentUser addEventLike:self]; else [User.currentUser removeEventLike:self];
  }
}

- (void) setUserNotLike:(BOOL)userNotLike {
  [self setUserNotLike:userNotLike sync:NO];
}

- (void) setUserNotLike:(BOOL)userNotLike sync:(BOOL)sync {
  _userNotLike = userNotLike;
  if (loadedFromDict) {
    if(sync) [self action:(userNotLike ? EventActionNotLike : EventActionUnnotLike )];
    if(_userLike) [User.currentUser removeEventLike:self];
  }
  if(userNotLike) _userLike = NO;
}

- (void) setUserBookmarked:(BOOL)userBookmarked {
  [self setUserBookmarked:userBookmarked sync:NO];
}

- (void) setUserBookmarked:(BOOL)userBookmarked sync:(BOOL)sync {
  _userBookmarked = userBookmarked;
  
  if(loadedFromDict) {
    if(sync) [self action:(userBookmarked ? EventActionBookmark : EventActionUnbookmark)];
    if(userBookmarked) [User.currentUser addEventBookmark:self]; else [User.currentUser removeEventBookmark:self];
  }
}

- (void) setUserRegistered:(BOOL)userRegistered {
  [self setUserRegistered:userRegistered sync:NO];
}

- (void) setUserRegistered:(BOOL)userRegistered sync:(BOOL)sync  {
  if(_userRegistered && userRegistered) return;
  _userRegistered = userRegistered;
  if(loadedFromDict) {
    if(sync) [self action: userRegistered ? EventActionRegister : EventActionUnregister];
    if(userRegistered) [User.currentUser addEventRegister:self]; else [User.currentUser removeEventRegister:self];
  }
}

- (BOOL) hasFacebookFriends {
  return self.facebookFriends.count > 0;
}

- (void) setFeedback:(id)value forKey:(NSString*)key {
  _feedback[key] = value;
}

- (id) feedbackForKey:(NSString*)key {
  return _feedback[key];
}

- (void) setUserFeedback:(BOOL)userFeedback {
  _userFeedback = userFeedback;
}

- (void) submitFeedback {
  self.userFeedback = YES;
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    NSError* error;
    [APIWrapper.api userFeedback:_feedback forUser:User.currentUser andEvent:self error:&error];
  });
}

- (double) distanceInMiles {
  return self.distance/1609.34;
}

- (BOOL) isContent {
  return [self.type isEqualToString:EventTypeContent];
}

- (void) addToCalendarWithCompletionBlock:(void(^)(BOOL))completion {
	
  EKEventStore* eventStore = eventStore=[[EKEventStore alloc] init];
  
  void(^block)(BOOL,NSError*) = ^(BOOL granted, NSError *error) {
    if(!granted) return;
    EKEvent *addEvent=[EKEvent eventWithEventStore:eventStore];
    addEvent.title=self.title;
    addEvent.startDate=self.startTime;
    addEvent.endDate= [self.startTime laterDate:self.endTime];
    addEvent.location = self.location;
    addEvent.calendar = [eventStore defaultCalendarForNewEvents];
   
		if ( [eventStore saveEvent:addEvent span:EKSpanThisEvent error:&error] ) {
      dispatch_async(dispatch_get_main_queue(), ^{
				completion(YES);
      });
    } else {
      NSLog(@"Event add failure: %@",error);
			completion(NO);
    }
  };
  
  if( [eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)] ) {
    [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:block];
  } else {
    block(YES,nil);
  }
}


@end
