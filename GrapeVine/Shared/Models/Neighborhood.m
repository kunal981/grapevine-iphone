//
//  Neighborhood
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "Neighborhood.h"
#import "Community.h"
#import "NSDictionary+Subscript.h"

NSString* const kNeighborhoodParamsCommunityId = @"parent_community";

@implementation Neighborhood

@synthesize name;

- (void) loadDataFromDictionary:(NSDictionary *)dict {
  [super loadDataFromDictionary:dict];
  
  _community = (Community*) [Community objectWithKey:dict[kNeighborhoodParamsCommunityId]];
  [_community addNeighborhood:self];
}

@end
