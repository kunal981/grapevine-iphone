//
//  User.h
//  GrapeVine
//
//  Created by Jack Kustanowitz on 8/7/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GVObject.h"
#import "Interest.h"
#import "Community.h"
#import "Event.h"
#import "SocialCommunity.h"

enum {
  UserKeyStatusNotActivated,
  UserKeyStatusActivated,
};
typedef NSUInteger UserKeyStatus;

extern NSString* const kUserParamsEmail;
extern NSString* const kUserParamsPassword;
extern NSString* const kUserParamsAge;
extern NSString* const kUserParamsGender;
extern NSString* const kUserParamsZipcode;
extern NSString* const kUserParamsFirstName;
extern NSString* const kUserParamsLastName;
extern NSString* const kUserParamsLikeEventIds;
extern NSString* const kUserParamsNotLikeEventIds;
extern NSString* const kUserParamsRegisterEventIds;
extern NSString* const kUserParamsBookmarkEventIds;
extern NSString* const kUserParamsPastEventIds;

extern NSString* const kUserParamsKeyStatus;
extern NSString* const kUserParamsType;
extern NSString* const kUserParamsAction;
extern NSString* const kUserParamsLat;
extern NSString* const kUserParamsLon;
extern NSString* const kUserParamsAccessToken;
extern NSString* const kUserParamsActivationCode;
extern NSString* const kUserParamsConfirmationCode;
extern NSString* const kUserParamsInterests;
extern NSString* const kUserParamsCommunity;
extern NSString* const kUserParamsNeighborhood;
extern NSString* const kUserParamsSocialCommunities;
extern NSString* const kUserParamsTimestamp;
extern NSString* const kUserParamsIosPushId;


extern NSString* const kUserKey;
extern NSString* const kUserActivationCode;

extern NSString* const kObjectKey;
extern NSString* const kObjectParamsName;

extern NSString* const kProfileCommunitiesKey;
extern NSString* const kProfileNeighborhoodsKey;
extern NSString* const kProfileInterestsKey;
extern NSString* const kProfileOrganizationsKey;
extern NSString* const kProfileSocialCommunitiesKey;

extern NSString* const UserNotificationNeighborhoodUpdate;
extern NSString* const UserNotificationProfileUpdate;

extern NSString* const kUserTrackingParamsAction;
extern NSString* const kUserTrackingParamsData;


@interface User : GVObject {
  NSMutableArray* _interests;
  NSMutableArray* _socialCommunities;
  NSMutableArray* _bookmarkedEvents;
  NSMutableArray* _registeredEvents;
  NSMutableArray* _likedEvents;
  NSMutableArray* _pastEvents;
  
  Community* _community;
  Neighborhood* _neighborhood;
  
  BOOL _eventsLoaded;
}

@property (nonatomic,retain) NSDate* lastRetrieved;

@property (nonatomic, retain) NSString* firstName;
@property (nonatomic, retain) NSString* lastName;
@property (nonatomic, retain) NSString* zipcode;
@property (nonatomic, retain) NSString* email;
@property (nonatomic, retain) NSString* gender;
@property (nonatomic, retain) NSNumber* age;
@property (nonatomic, assign) UserKeyStatus keyStatus;
@property (nonatomic, strong) NSString* confirmationCode;
@property (nonatomic, strong) NSString* facebookImage;

@property (nonatomic,assign) NSInteger likeCount;
@property (nonatomic,strong) Neighborhood* neighborhood;
@property (nonatomic,assign) BOOL isNew;

@property (readonly) Community* community;
@property (readonly) NSArray* interests;
@property (readonly) NSArray* socialCommunities;
@property (readonly) NSArray* bookmarkedEvents;
@property (readonly) NSArray* registeredEvents;
@property (readonly) NSArray* registeredEventsWithoutContent;
@property (readonly) NSArray* pastEvents;
@property (readonly) NSArray* likedEvents;

@property (readonly) BOOL eventsLoaded;

+ (User*) currentUser;
+ (void) clearSavedLogin;
+ (BOOL) isLoginSaved;

+ (void) loginWithSavedDetails:(void (^)(User* user, NSError* error))callback;
+ (void) loginWithEmail:(NSString*)email andPassword:(NSString*) password andCompletionBlock:(void (^)(User*, NSError*))callback;
+ (void) loginRegisterWithFacebookAccessToken:(NSString*)token andCompletionBlock:(void (^)(User*, NSError*))callback;

+ (void) registerWithParams:(NSDictionary*)params andCompletionBlock:(void (^)(User*,NSError*))callback;
+ (void) requestPasswordReset:(NSString*) email andCompletionBlock:(void(^)(NSError*))callback;

+ (void) logout;

- (void) activateWithCode:(NSString*)code andCompletionBlock:(void(^)(NSError*))callback;
- (void) recoverPassword:(NSString*)password withCompletionBlock:(void(^)(NSError*))callback;

- (void) refreshProfileWithCompletionBlock:(void(^)(NSError*))callback;

- (void) updateDetailsWithCompletionBlock:(void(^)(NSError*))callback;

- (void) updateInterests;
- (BOOL) hasInterest:(Interest*)interest;
- (void) addInterest:(Interest*)interest;
- (void) removeInterest:(Interest*)interest;

- (void) updateSocialCommunities;
- (BOOL) hasSocialCommunity:(SocialCommunity*)socialCommunity;
- (void) addSocialCommunity:(SocialCommunity*)socialCommunity;
- (void) removeSocialCommunity:(SocialCommunity*)socialCommunity;

- (BOOL) hasEventBookmark:(Event*)event;
- (void) addEventBookmark:(Event*)event;
- (void) removeEventBookmark:(Event*)event;

- (BOOL) hasEventRegister:(Event*)event;
- (void) addEventRegister:(Event*)event;
- (void) removeEventRegister:(Event*)event;

- (BOOL) hasEventLike:(Event*)event;
- (void) addEventLike:(Event*)event;
- (void) removeEventLike:(Event*)event;

- (void) updateNeighborhood:(Neighborhood *)neighborhood withCompletionBlock:(void(^)(NSError*))callback;

- (void) loadRelatedEventsWithCompletionBlock:(void(^)(NSError*))callback;
- (void) updateAccessToken:(NSString*)accessToken;

- (void) trackAction:(NSString*)action withData:(id)data;

@end
