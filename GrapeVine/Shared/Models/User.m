//
//  User.m
//  GrapeVine
//
//  Created by Jack Kustanowitz on 8/7/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "User.h"
#import "APIWrapper.h"
#import "NSDictionary+Subscript.h"
#import "NSMutableDictionary+Subscript.h"

#import "Neighborhood.h"
#import "Organization.h"
#import "Interest.h"
#import "SocialCommunity.h"
#import "NSArray+Subscript.h"
#import "NSArray+Enumerable.h"
//#import <FacebookSDK/FacebookSDK.h>






NSString* const kUserParamsEmail = @"email";
NSString* const kUserParamsPassword = @"password";
NSString* const kUserParamsAge = @"age";
NSString* const kUserParamsGender = @"gender";
NSString* const kUserParamsZipcode = @"offer_postalcode";
NSString* const kUserParamsFirstName = @"first_name";
NSString* const kUserParamsLastName = @"last_name";
NSString* const kUserParamsLikeEventIds = @"event_likes";
NSString* const kUserParamsNotLikeEventIds = @"event_notlikes";
NSString* const kUserParamsRegisterEventIds = @"event_registers";
NSString* const kUserParamsBookmarkEventIds = @"event_bookmarks";
NSString* const kUserParamsPastEventIds = @"event_registers_past";

NSString* const kUserParamsKeyStatus = @"key_status";
NSString* const kUserParamsType = @"type";
NSString* const kUserParamsAction = @"action";
NSString* const kUserParamsLat = @"lat";
NSString* const kUserParamsLon = @"lon";
NSString* const kUserParamsAccessToken = @"access_token";
NSString* const kUserParamsActivationCode = @"activation_code";
NSString* const kUserParamsConfirmationCode = @"confirmation_code";
NSString* const kUserParamsInterests = @"interests";
NSString* const kUserParamsCommunity = @"community_id";
NSString* const kUserParamsNeighborhood = @"neighborhood_id";
NSString* const kUserParamsSocialCommunities = @"target_communities";
NSString* const kUserParamsIosPushId = @"ios_push_id";

NSString* const kUserParamsTimestamp = @"timestamp";

NSString* const kUserKey = @"user_key";
NSString* const kUserActivationCode = @"activation_code";

NSString* const kObjectKey = @"id";
NSString* const kObjectParamsName = @"value";

NSString* const kProfileCommunitiesKey = @"communities_all";
NSString* const kProfileNeighborhoodsKey = @"neighborhoods_all";
NSString* const kProfileInterestsKey = @"interests_all";
NSString* const kProfileOrganizationsKey = @"organizations_all";
NSString* const kProfileSocialCommunitiesKey = @"target_communities_all";

NSString* const UserNotificationNeighborhoodUpdate = @"UserNotificationNeighborhoodUpdate";
NSString* const UserNotificationProfileUpdate = @"UserNotificationProfileUpdate";


NSString* const kUserTrackingParamsAction = @"action";
NSString* const kUserTrackingParamsData = @"data";

@implementation User

- (void) afterInitialize {
  _interests = [@[] mutableCopy];
  _socialCommunities = [@[] mutableCopy];
  _registeredEvents = [@[] mutableCopy];
  _bookmarkedEvents = [@[] mutableCopy];
  _likedEvents = [@[] mutableCopy];
  _pastEvents = [@[] mutableCopy];
  _eventsLoaded = NO;
}

+ (NSString*) keyName {
  return kUserKey;
}

static User* currentUser;
+ (User*) currentUser {
  return currentUser;
}

static NSString* SAVED_USER_IDENTIFIER = @"GrapeVineUserIdentifier";
- (void) saveLogin {
  [[NSUserDefaults standardUserDefaults] setObject:self.key forKey:SAVED_USER_IDENTIFIER];
  [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void) clearSavedLogin {
  [[NSUserDefaults standardUserDefaults] removeObjectForKey:SAVED_USER_IDENTIFIER];
  [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (BOOL) isLoginSaved {
  return [[NSUserDefaults standardUserDefaults] objectForKey:SAVED_USER_IDENTIFIER] != nil;
}

+ (void) loginWithSavedDetails:(void (^)(User* user, NSError* error))callback {
  if([self isLoginSaved]) {   
    currentUser = (User*) [User objectWithKey:[[NSUserDefaults standardUserDefaults] objectForKey:SAVED_USER_IDENTIFIER]];

    [currentUser refreshProfileWithCompletionBlock:^(NSError* error){
			if (error) {
				currentUser = nil;
			}
			
      callback(currentUser,error);
    }];
  } else {
    callback(nil,[NSError errorWithDomain:@"No Saved Login" code:1 userInfo:nil]);
  }
}

+ (void) loginWithEmail:(NSString*)email andPassword:(NSString*)password andCompletionBlock:(void(^)(User*, NSError*))callback {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
		NSError* error;
		
		// try to login
		NSDictionary* userDict = [APIWrapper.api userLoginWithEmail:email andPassword:password error:&error];
    
    
    if(!userDict) {
      error = [NSError errorWithDomain:@"Login Error" code:0 userInfo:nil];
    } else if (!error) {
			currentUser = (User*) [User objectWithDictionary:userDict];
      [currentUser saveLogin];
		}
    
		// Then on the main thread...
    if(callback) dispatch_async(dispatch_get_main_queue(), ^{
      callback(currentUser,error);
    });
	});
}

+ (void) loginRegisterWithFacebookAccessToken:(NSString*)token andCompletionBlock:(void (^)(User*, NSError*))callback {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
		NSError* error;
		
		// try to login
		NSDictionary* userDict = [APIWrapper.api userLoginRegisterWithFacebookAccessToken:token error:&error];

		if (!error) {
			currentUser = (User*) [User objectWithDictionary:userDict];
      [currentUser saveLogin];
		}
    
		// Then on the main thread...
    if(callback) dispatch_async(dispatch_get_main_queue(), ^{
      callback(currentUser,error);
    });
	});
}

+ (void) registerWithParams:(NSDictionary*)params andCompletionBlock:(void (^)(User*, NSError*))callback {
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
		NSError* error;
    
    
		// try to register, this will return a user key that isn't usable for anything but activation
		NSDictionary* userDict = [APIWrapper.api userRegisterWithParams:params error:&error];
    if (!error) {
      currentUser = (User*) [User objectWithDictionary:userDict];
      if (currentUser.keyStatus == UserKeyStatusActivated) {
        [currentUser saveLogin];
      } 
    }

		// Then on the main thread...
    if(callback) dispatch_async(dispatch_get_main_queue(), ^{
      callback(currentUser,error);
    });
	});
	
}

+ (void) requestPasswordReset:(NSString*) email andCompletionBlock:(void(^)(NSError*))callback {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
		NSError* error;
    [APIWrapper.api userRequestPasswordReset:email error:&error];
  
    if(callback) dispatch_async(dispatch_get_main_queue(), ^{
      callback(error);
    });
  });
}

+ (void) logout {
  [GVObject resetCache];
  currentUser = nil;
  [self.class clearSavedLogin];
//  if([FBSession activeSession]) {
//    [[FBSession activeSession] closeAndClearTokenInformation];
//  }
}

+ (NSDictionary*) keysForUpdateWithDictionary {
  return @{
    @"zipcode":kUserParamsZipcode,
	};
}

- (void) loadDataFromDictionary:(NSDictionary*)dict {
  [super loadDataFromDictionary:dict];
  
  self.keyStatus = ((NSString*) dict[kUserParamsKeyStatus]).boolValue;
  
  _interests = [@[] mutableCopy];
  _socialCommunities = [@[] mutableCopy];
  _registeredEvents = [@[] mutableCopy];
  _bookmarkedEvents = [@[] mutableCopy];
  _likedEvents = [@[] mutableCopy];
  _pastEvents = [@[] mutableCopy];  
  
  [Organization resetCache];
  NSDictionary* map = @{ kProfileCommunitiesKey:@"Community",kProfileNeighborhoodsKey:@"Neighborhood", kProfileInterestsKey:@"Interest",kProfileOrganizationsKey:@"Organization",kProfileSocialCommunitiesKey:@"SocialCommunity"};
  [map enumerateKeysAndObjectsUsingBlock:^(NSString* key,NSString* className,BOOL* stop) {
    if(dict[key] == [NSNull null]) return;
    Class klass = NSClassFromString(className);
		for (NSDictionary* itemDict in dict[key]) {
      GVObject* obj = [klass objectWithDictionary:itemDict];
      if([obj respondsToSelector:@selector(setName:)]) [obj setValue:itemDict[@"value"] forKey:@"name"];
    }
  }];
  
	if (dict[kUserParamsInterests] != [NSNull null]) {
		for(id key in dict[kUserParamsInterests]) {
			[_interests addObject:[Interest objectWithKey:key]];
		}
	}
  
  if (dict[kUserParamsSocialCommunities] != [NSNull null]) {
		for(id key in dict[kUserParamsSocialCommunities]) {
			[_socialCommunities addObject:[SocialCommunity objectWithKey:key]];
		}
	}
  
  map = @{kUserParamsLikeEventIds:_likedEvents,kUserParamsRegisterEventIds:_registeredEvents,kUserParamsBookmarkEventIds:_bookmarkedEvents,kUserParamsPastEventIds:_pastEvents,};
  [map enumerateKeysAndObjectsUsingBlock:^(NSString* param,NSMutableArray* array, BOOL* stop) {
    if(dict[param] == [NSNull null]) return;
    for (id key in dict[param]) {
      [array addObject:[Event objectWithKey:key]];
    }
  }];
  
  for(Event* e in _bookmarkedEvents) {
    e.userBookmarked = YES;
  }

	NSNumber* neighborhoodId = dict[kUserParamsNeighborhood];
	if (neighborhoodId.boolValue) {
		self.neighborhood = (Neighborhood*) [Neighborhood objectWithKey:neighborhoodId];
	}

}

- (BOOL) refreshProfile:(NSError**) error {
  NSDictionary* dict = [APIWrapper.api userGetProfile:self error:error];
  
	if( !dict ) *error =  [NSError errorWithDomain:@"Profile Data Missing" code:2 userInfo:nil];
	
  if(!*error) {
    _eventsLoaded = NO;
    [self loadDataFromDictionary:dict];
  }
	
  return *error == nil;
}

- (void) refreshProfileWithCompletionBlock:(void (^)(NSError *))callback {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    NSError* error;
    [self refreshProfile:&error];
    if(callback) {
      dispatch_async(dispatch_get_main_queue(), ^{
        callback(error);
      });
    }
  });
}

- (void) activateWithCode:(NSString*)code andCompletionBlock:(void(^)(NSError*))callback {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    NSError* error;
    [APIWrapper.api userActivateRegistration:self withCode:code error:&error];
    
    if(!error) {
      [self refreshProfile:&error];
      currentUser = self;
      [currentUser saveLogin];
    }
    
    if(callback) dispatch_async(dispatch_get_main_queue(), ^{
      callback(error);
    });
  });
}

- (void) recoverPassword:(NSString*)password withCompletionBlock:(void(^)(NSError*))callback {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    NSError* error;
    
    [APIWrapper.api userRecover:self withPassword:password error:&error];
    
    if(!error) {
      [self refreshProfile:&error];
      if(!error) {
        currentUser = self;
        [currentUser saveLogin];
      }
    }
    
    if(callback) dispatch_async(dispatch_get_main_queue(), ^{
      callback(error);
    });
  });
}

- (void) updateDetailsWithCompletionBlock:(void(^)(NSError*))callback {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    NSError* error;
    
    [APIWrapper.api userUpdateDetails:self error:&error];
    
    if(callback) dispatch_async(dispatch_get_main_queue(), ^{
      callback(error);
			
			if(!error) [NSNotificationCenter.defaultCenter postNotificationName:UserNotificationProfileUpdate object:self];
    });
  });
}


- (void) updateInterests {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    NSError* error;
    
    [APIWrapper.api userUpdateInterests:self error:&error];
		
		if(!error) dispatch_async(dispatch_get_main_queue(), ^{
      [NSNotificationCenter.defaultCenter postNotificationName:UserNotificationProfileUpdate object:self];
    });
  });
}


- (NSArray*) interests {
  return [NSArray arrayWithArray:_interests];
}

- (BOOL) hasInterest:(Interest*)interest {
  return [self.interests indexOfObject:interest] != NSNotFound;
}

- (void) addInterest:(Interest*)interest {
  if(![self hasInterest:interest]) [_interests addObject:interest];
}

- (void) removeInterest:(Interest*)interest {
  [_interests removeObject:interest];
}

- (void) updateSocialCommunities {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    NSError* error;
    
    [APIWrapper.api userUpdateSocialCommunities:self error:&error];
		
		if(!error) dispatch_async(dispatch_get_main_queue(), ^{
      [NSNotificationCenter.defaultCenter postNotificationName:UserNotificationProfileUpdate object:self];
    });
  });
}

- (NSArray*) socialCommunities {
  return [NSArray arrayWithArray:_socialCommunities];
}

- (BOOL) hasSocialCommunity:(SocialCommunity*)socialCommunity {
  return [self.socialCommunities indexOfObject:socialCommunity] != NSNotFound;
}

- (void) addSocialCommunity:(SocialCommunity*)socialCommunity {
  if(![self hasSocialCommunity:socialCommunity]) [_socialCommunities addObject:socialCommunity];
}

- (void) removeSocialCommunity:(SocialCommunity*)socialCommunity {
  [_socialCommunities removeObject:socialCommunity];
}

- (NSArray*) bookmarkedEvents {
  return [NSArray arrayWithArray:_bookmarkedEvents];
}

- (BOOL) hasEventBookmark:(Event*)event {
  return [_bookmarkedEvents indexOfObject:event] != NSNotFound;
}

- (void) addEventBookmark:(Event*)event {
  if(![self hasEventBookmark:event]) [_bookmarkedEvents addObject:event];
}

- (void) removeEventBookmark:(Event*)event {
  [_bookmarkedEvents removeObject:event];
}

- (NSArray*) registeredEvents {
  return [NSArray arrayWithArray:_registeredEvents];
}

- (NSArray*) registeredEventsWithoutContent {
  return [[self registeredEvents] select:^(Event* event){ return (BOOL) (!event.isContent); }];
}

- (BOOL) hasEventRegister:(Event*)event {
  return [_registeredEvents indexOfObject:event] != NSNotFound;
}

- (void) addEventRegister:(Event*)event {
  if(![self hasEventRegister:event]) [_registeredEvents addObject:event];
}

- (void) removeEventRegister:(Event*)event {
  [_registeredEvents removeObject:event];
}

- (NSArray*) registeredLike {
  return [NSArray arrayWithArray:_likedEvents];
}

- (BOOL) hasEventLike:(Event*)event {
  return [_likedEvents indexOfObject:event] != NSNotFound;
}

- (void) addEventLike:(Event*)event {
  if(![self hasEventLike:event]) [_likedEvents addObject:event];
}

- (void) removeEventLike:(Event*)event {
  [_likedEvents removeObject:event];
}

- (NSArray*) pastEvents {
  return [NSArray arrayWithArray:_pastEvents];
}

- (void) loadRelatedEventsWithCompletionBlock:(void(^)(NSError*))callback {
  if(self.eventsLoaded) return callback(nil);
  NSMutableSet* set = [NSMutableSet setWithArray:_registeredEvents];
  [set addObjectsFromArray:_bookmarkedEvents];
  [set addObjectsFromArray:_pastEvents];

  [Event eventsForUser:self byIds:[set.allObjects mapKey:@"key"] withCompletionBlock:^(NSDictionary* events,NSError* error){
    _eventsLoaded = YES;
    callback(error);
  }];
}

- (Community*) community {
  return self.neighborhood.community ?: (Community*) [Community objectWithKey:@(27)];
}

- (void) setNeighborhood:(Neighborhood *)neighborhood {
  _neighborhood = neighborhood;
  [[NSNotificationCenter defaultCenter] postNotificationName:UserNotificationNeighborhoodUpdate object:self];
}

- (void) updateNeighborhood:(Neighborhood *)neighborhood withCompletionBlock:(void(^)(NSError*))callback {
  self.neighborhood = neighborhood;
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    NSError* error;
    
    [APIWrapper.api userUpdateNeighborhood:self error:&error];
    dispatch_async(dispatch_get_main_queue(), ^(){
      if(callback) callback(error);
    });
  });
}

- (void) updateAccessToken:(NSString*)accessToken {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    NSError* error;
    
    [APIWrapper.api userUpdate:self withAccessToken:accessToken error:&error];
  });
}


- (void) trackAction:(NSString*)action withData:(id)data {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    NSError* error;
    
    [APIWrapper.api userTrack:action forUser:self andData:data error:&error];
  });
}


@end
