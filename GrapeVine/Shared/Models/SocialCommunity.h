//
//  SocialCommunity.h
//  Grapevine
//
//  Created by Zachary Gavin on 10/13/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "GVObject.h"

@interface SocialCommunity : GVObject <NamedGVObject>

@end
