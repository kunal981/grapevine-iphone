//
//  Event.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Neighborhood.h"
#import "Organization.h"
#import "Interest.h"

@class User;

extern NSString* const EventTypeRecommended;
extern NSString* const EventTypeFriends;
extern NSString* const EventTypeRegistered;
extern NSString* const EventTypeNearby;
extern NSString* const EventTypeSponsored;
extern NSString* const EventTypeAll;

extern NSString* const EventParamsType;
extern NSString* const EventParamsFilterStartDate;
extern NSString* const EventParamsFilterEndDate;
extern NSString* const EventParamsFilterOrganizationId;
extern NSString* const EventParamsFilterNeighborhoodId;
extern NSString* const EventParamsFilterInterest;
extern NSString* const EventParamsFilterCommunity;
extern NSString* const EventParamsFilterId;
extern NSString* const EventParamsFilterTerm;
extern NSString* const EventParamsGroupByStartDay;
extern NSString* const EventParamsSort;
extern NSString* const EventParamsSortOrder;
extern NSString* const EventParamsLon;
extern NSString* const EventParamsLat;
extern NSString* const EventParamsRadiusMeter;
extern NSString* const EventParamsZipcode;

extern NSString* const EventFeedbackOverall;
extern NSString* const EventFeedbackWelcomed;
extern NSString* const EventFeedbackConnections;
extern NSString* const EventFeedbackInvolvement;
extern NSString* const EventFeedbackResonated;
extern NSString* const EventFeedbackRecommend;
extern NSString* const EventFeedbackComments;

extern const NSArray* EventFeedbackRatings;

extern NSString* const EventActionLike;
extern NSString* const EventActionUnlike;
extern NSString* const EventActionNotLike;
extern NSString* const EventActionUnnotLike;
extern NSString* const EventActionRegister;
extern NSString* const EventActionUnregister;
extern NSString* const EventActionBookmark;
extern NSString* const EventActionUnbookmark;
extern NSString* const EventActionFeedback;

extern NSString* const EventTypeContent;
extern NSString* const EventTypeEvent;

typedef void(^EventCallback)(NSDictionary*,NSError*);

@interface Event : GVObject {
  BOOL loadedFromDict;
  NSMutableDictionary* _feedback;
}

@property (nonatomic,retain) NSDate* lastRetrieved;

@property (nonatomic,retain) NSString* blurb;
@property (nonatomic,retain) NSString* addressLine1;
@property (nonatomic,retain) NSString* city;
@property (nonatomic,retain) NSString* description;
@property (nonatomic,retain) NSDate* endTime;
@property (nonatomic,retain) NSString* eventUrl;
@property (nonatomic,retain) NSString* facebookUrl;
@property (nonatomic,retain) NSArray* friends;
@property (nonatomic,retain) NSString* imageUrl;
@property (nonatomic,retain) NSArray* interests;
@property (nonatomic,retain) NSString* location;
@property (nonatomic,retain) NSDate* startTime;
@property (nonatomic,retain) NSDate* createdTime; // Post date for content
@property (nonatomic,retain) NSString* state;
@property (nonatomic,retain) NSString* teaser;
@property (nonatomic,retain) NSString* title;
@property (nonatomic,retain) NSString* twitterHashtag;
@property (nonatomic,retain) NSString* zipcode;
@property (nonatomic,assign) double distance;
@property (readonly) double distanceInMiles;
@property (nonatomic,assign) double latitude;
@property (nonatomic,assign) double longitude;
@property (nonatomic,assign) BOOL userLike;
@property (nonatomic,assign) BOOL userNotLike;
@property (nonatomic,assign) BOOL userRegistered;
@property (nonatomic,assign) BOOL userBookmarked;
@property (nonatomic,assign) BOOL userFeedback;
@property (nonatomic,assign) int userScore;
@property (nonatomic,assign) BOOL sponsored;
@property (nonatomic,assign) int popularityScore;
@property (nonatomic,retain) Organization* organization;
@property (nonatomic,retain) Organization* sponsoring_organization;
@property (nonatomic,strong) NSArray* facebookFriends;
@property (nonatomic,strong) NSString* registrationUrl;
@property (nonatomic,strong) NSString* publicUrl;
@property (readonly) BOOL hasFacebookFriends;
@property (nonatomic,strong) NSString* type;
@property (readonly) BOOL isContent;
@property (nonatomic,strong) Neighborhood* neighborhood;
@property (nonatomic,readwrite) BOOL isOnSite;

  
+ (void) savedEventsForUser:(User*)user withCompletionBlock:(EventCallback)callback;
+ (void) recommendedEventsForUser:(User*)user withCompletionBlock:(EventCallback)callback;
+ (void) nearbyEventsForUser:(User*)user withCompletionBlock:(EventCallback)callback;
+ (void) eventsForUser:(User*)user fromDate:(NSDate*)startDate toDate:(NSDate*)endDate withCompletionBlock:(EventCallback)callback;
+ (void) eventsForUser:(User*)user byNeighborhood:(Neighborhood*)neighborhood withCompletionBlock:(EventCallback)callback;
+ (void) eventsForUser:(User*)user byCommunity:(Community*)community withCompletionBlock:(EventCallback)callback;
+ (void) eventsForUser:(User*)user byOrganization:(Organization*)organization withCompletionBlock:(EventCallback)callback;
+ (void) eventsForUser:(User*)user bySearchTerm:(NSString*)searchTerm withCompletionBlock:(EventCallback)callback;
+ (void) eventsForUser:(User*)user byInterest:(Interest*)interest withCompletionBlock:(EventCallback)callback;
+ (void) eventsForUser:(User*)user byIds:(NSArray*)ids withCompletionBlock:(EventCallback)callback;
+ (void) groupedEventsForUser:(User*)user withCompletionBlock:(EventCallback)callback;

- (void) setUserLike:(BOOL)userLike sync:(BOOL)sync;
- (void) setUserNotLike:(BOOL)userNotLike sync:(BOOL)sync;
- (void) setUserBookmarked:(BOOL)userBookmarked sync:(BOOL)sync;
- (void) setUserRegistered:(BOOL)userRegistered sync:(BOOL)sync;

- (void) setFeedback:(id)feedback forKey:(NSString*)key;
- (id) feedbackForKey:(NSString*)key;
- (void) submitFeedback;

- (void) addToCalendarWithCompletionBlock:(void(^)(BOOL))completion;

@end
