//
//  Location.h
//  GrapeVine
//
//  Created by Jack Kustanowitz on 9/13/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GVObject.h"

@class Neighborhood;

@interface Community : GVObject <NamedGVObject> {
  NSMutableArray* _neighborhoods;
}

@property (readonly) NSArray* neighborhoods;

- (BOOL) hasNeighborhood:(Neighborhood*)neighborhood;
- (void) addNeighborhood:(Neighborhood*)neighborhood;
- (void) removeNeighborhood:(Neighborhood*)neighborhood;

@end
