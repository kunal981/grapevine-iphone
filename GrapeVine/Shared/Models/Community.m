//
//  Location.m
//  GrapeVine
//
//  Created by Jack Kustanowitz on 9/13/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "Community.h"
#import "Neighborhood.h"

@implementation Community

@synthesize name;

- (void) afterInitialize {
  _neighborhoods = [@[] mutableCopy];
}

- (NSArray*) neighborhoods {
  return [NSArray arrayWithArray:_neighborhoods];
}

- (BOOL) hasNeighborhood:(Neighborhood*)neighborhood {
  return [self.neighborhoods indexOfObject:neighborhood] != NSNotFound;
}

- (void) addNeighborhood:(Neighborhood*)neighborhood {
  if(![self hasNeighborhood:neighborhood]) [_neighborhoods addObject:neighborhood];
}

- (void) removeNeighborhood:(Neighborhood*)neighborhood {
  [_neighborhoods removeObject:neighborhood];
}

@end
