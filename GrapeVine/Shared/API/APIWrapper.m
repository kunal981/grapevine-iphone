//
//  APIWrapper.m
//  GrapeVine
//
//  Created by Jack Kustanowitz on 8/5/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "APIWrapper.h"
#import "NSDictionary+Subscript.h"
#import "NSMutableDictionary+Subscript.h"
#import "LocationManager.h"
#import "NSArray+Enumerable.h"
#import "NSNull+Conversions.h"
#import "NSDictionary+Enumerable.h"

//static NSString* API_BASE_URL = @"http://jjcdev.repeatsys.com/apiv1";
static NSString* API_BASE_URL = @"https://www.grape-vine.com/apiv1";
static NSString* API_KEY = @"ac7004f3e19641cd991a649087212164";

@implementation APIWrapper

+ (APIWrapper*) api  {
    static APIWrapper *instance;
	
    @synchronized(self) {
        if(!instance) {
            instance = [[APIWrapper alloc] init];
        }
    }
	
    return instance;
}

#pragma mark User Login/Register Functions
- (NSDictionary*) userLoginWithEmail:(NSString*)email andPassword:(NSString*)password error:(NSError**)error {
	CLLocationCoordinate2D location = [LocationManager sharedLocationManager].currentLocation;
	NSString* apnid = [[NSUserDefaults standardUserDefaults] valueForKey:@"apnid"] ? [[NSUserDefaults standardUserDefaults] valueForKey:@"apnid"] : @"";
	
	return [self dataWithMethod:@"POST" andEndpoint:@"users" andParams:@{kUserParamsEmail:email, kUserParamsPassword:password, kUserParamsType:@"existing", kUserParamsLat:@(location.latitude), kUserParamsLon:@(location.longitude), kUserParamsIosPushId:apnid} debug:NO error:error];
}

- (NSDictionary*) userLoginRegisterWithFacebookAccessToken:(NSString*)token error:(NSError**)error {
	CLLocationCoordinate2D location = [LocationManager sharedLocationManager].currentLocation;
	NSString* apnid = [[NSUserDefaults standardUserDefaults] valueForKey:@"apnid"] ? [[NSUserDefaults standardUserDefaults] valueForKey:@"apnid"] : @"";

	return [self dataWithMethod:@"POST" andEndpoint:@"users" andParams:@{kUserParamsAccessToken:token, kUserParamsType:@"facebook", kUserParamsLat:@(location.latitude), kUserParamsLon: @(location.longitude), kUserParamsIosPushId:apnid} debug:YES error:error];
}

- (NSDictionary*) userRegisterWithParams:(NSDictionary*)params	  error:(NSError**)error {
	NSString* apnid = [[NSUserDefaults standardUserDefaults] valueForKey:@"apnid"] ? [[NSUserDefaults standardUserDefaults] valueForKey:@"apnid"] : @"";
	NSMutableDictionary* newParams = [NSMutableDictionary dictionaryWithDictionary:params];
	newParams[kUserParamsIosPushId] = apnid;
	return [self userRegisterWithParams:newParams debug:NO error:error];
}

- (NSDictionary*) userRegisterWithParams:(NSDictionary*)params debug:(BOOL) debug error:(NSError**)error {
  CLLocationCoordinate2D location = [LocationManager sharedLocationManager].currentLocation;
	NSString* apnid = [[NSUserDefaults standardUserDefaults] valueForKey:@"apnid"] ? [[NSUserDefaults standardUserDefaults] valueForKey:@"apnid"] : @"";
  params = [params merge:@{@"type":@"new",kUserParamsLat:@(location.latitude), kUserParamsLon: @(location.longitude), kUserParamsIosPushId:apnid}];

	return [self dataWithMethod:@"POST" andEndpoint:@"users" andParams:params debug:debug error:error];
}

- (BOOL) userActivateRegistration:(User*)user withCode:(NSString*)code error:(NSError**)error; {
  NSDictionary* params = @{kUserParamsActivationCode:code};
	[self dataWithMethod:@"PUT" andEndpoint:[NSString stringWithFormat:@"users/%@/user_key", user.key] andParams:params debug:NO error:error];
	
	return (*error == nil);
}

- (BOOL) userDelete:(User*)user error:(NSError**)  error {
	[self dataWithMethod:@"DELETE" andEndpoint:[NSString stringWithFormat:@"users/%@", user.email] andParams:nil debug:YES error:error];
	
	return (*error == nil);
}

- (BOOL) userRequestPasswordReset:(NSString*)email error:(NSError**)error {
	[self dataWithMethod:@"POST" andEndpoint:[NSString stringWithFormat:@"users/%@/recover", email] andParams:nil debug:NO error:error];
	
	return (*error == nil);
}

- (NSDictionary*) userRecover:(User*)user withPassword:(NSString*)password error:(NSError**)error {
  NSDictionary* params = @{kUserParamsConfirmationCode:user.confirmationCode,kUserParamsPassword:password};
  return [self dataWithMethod:@"PUT" andEndpoint:[NSString stringWithFormat:@"users/%@/recover",user.key] andParams:params debug:NO error:error];
}

#pragma mark User Profile Functions

- (NSDictionary*) userGetProfile:(User*) user error:(NSError**) error {
  CLLocationCoordinate2D location = [LocationManager sharedLocationManager].currentLocation;
	NSString* apnid = [[NSUserDefaults standardUserDefaults] valueForKey:@"apnid"] ? [[NSUserDefaults standardUserDefaults] valueForKey:@"apnid"] : @"";

	return [self dataWithMethod:@"GET" andEndpoint:[NSString stringWithFormat:@"users/%@",user.key] andParams:@{kUserParamsLat:@(location.latitude), kUserParamsLon: @(location.longitude), kUserParamsIosPushId:apnid} debug:NO error:error];
}

- (BOOL) userUpdateInterests:(User*)user error:(NSError**)error {
  NSDictionary* params = @{kUserParamsInterests:[user.interests mapKey:@"key"]};
  return [self userUpdateProfile:user withParams:params error:error];
}

- (BOOL) userUpdateSocialCommunities:(User*)user error:(NSError**)error {
  NSDictionary* params = @{kUserParamsSocialCommunities:[user.socialCommunities mapKey:@"key"]};
  return [self userUpdateProfile:user withParams:params error:error];
}

- (BOOL) userUpdateDetails:(User*)user error:(NSError**)error {
  NSDictionary* params = @{
    kUserParamsFirstName:user.firstName,
    kUserParamsLastName:user.lastName,
    kUserParamsEmail:user.email,
    kUserParamsAge:user.age,
  };
  
  if(user.gender) params = [params merge:@{kUserParamsGender:user.gender}];
  return [self userUpdateProfile:user withParams:params error:error];
}

- (BOOL) userUpdateNeighborhood:(User*)user error:(NSError**)error {
  NSDictionary* params = @{kUserParamsNeighborhood:user.neighborhood.key};
  return [self userUpdateProfile:user withParams:params error:error];
}

- (BOOL) userUpdate:(User *)user withAccessToken:(NSString*)accessToken error:(NSError **)error {
  NSDictionary* params = @{kUserParamsAccessToken:accessToken};
  return [self userUpdateProfile:user withParams:params error:error];
}

- (BOOL) userUpdateProfile:(User*)user withParams:(NSDictionary*)params error:(NSError**)error {
  params = [@{kUserParamsTimestamp:@((int) [[NSDate date] timeIntervalSince1970]+7200 )} merge:params];
  [self dataWithMethod:@"PUT" andEndpoint:[NSString stringWithFormat:@"users/%@",user.key] andParams:params debug:NO error:error];
  
  return error != nil;
}

#pragma mark User Event Functions
- (NSDictionary*) userGetRecommendedEvents:(User*)user error:(NSError**)error {
	NSDictionary* params = @{
                            EventParamsType:[@[EventTypeSponsored,EventTypeFriends,EventTypeRecommended] componentsJoinedByString:@","],
                            EventParamsFilterCommunity:(user.community.key ?: @"25")
                         };
	return [self userGetEvents:user params:params error:error];
}

- (NSDictionary*) userGetNearbyEvents:(User*)user error:(NSError**)error {
  NSDictionary* params = @{EventParamsType:EventTypeNearby,@"radius_meters":@(10000)};
  
  return [self userGetEvents:user params:params error:error];
}

- (NSDictionary*) userGetEvents:(User*)user fromDate:(NSDate*)startDate toDate:(NSDate*)endDate error:(NSError**)error {
  NSDictionary* params = @{
                            EventParamsFilterStartDate:@(round(startDate.timeIntervalSince1970)),
                            EventParamsFilterEndDate:@(round(endDate.timeIntervalSince1970)),
                            EventParamsFilterCommunity:(user.community.key ?: @"25"),
                            EventParamsType:EventTypeAll
                         };
  
  return [self userGetEvents:user params:params error:error];
}

- (NSDictionary*) userGetEvents:(User*)user byNeighborhood:(Neighborhood*)neighborhood error:(NSError**)error {
  NSDictionary* params = @{EventParamsFilterNeighborhoodId:neighborhood.key,EventParamsType:EventTypeAll};
  
  return [self userGetEvents:user params:params error:error];
}

- (NSDictionary*) userGetEvents:(User*)user byCommunity:(Community*)community error:(NSError**)error {
  NSDictionary* params = @{EventParamsFilterCommunity:community.key,EventParamsType:EventTypeAll};
  
  return [self userGetEvents:user params:params error:error];
}

- (NSDictionary*) userGetEvents:(User*)user byInterest:(Interest*)interest error:(NSError**)error {
  NSDictionary* params = @{
														EventParamsFilterInterest:interest.key,EventParamsType:EventTypeAll,
														EventParamsFilterCommunity:(user.community.key ?: @"25")
												 };
  
  return [self userGetEvents:user params:params error:error];
}

- (NSDictionary*) userGetEvents:(User*)user byOrganization:(Organization*)organization error:(NSError**)error {
  NSDictionary* params = @{EventParamsFilterOrganizationId:organization.key,EventParamsType:EventTypeAll};
  return [self userGetEvents:user params:params error:error];
}

- (NSDictionary*) userGetEvents:(User*)user byIds:(NSArray*)ids error:(NSError**)error {
  if(ids.count == 0) return @{EventTypeAll:@[]};
  NSDictionary* params = @{EventParamsFilterId:[ids componentsJoinedByString:@","],EventParamsType:EventTypeAll};
  return [self userGetEvents:user params:params error:error];
}

- (NSDictionary*) userGetEvents:(User*)user bySearchTerm:(NSString*) searchTerm error:(NSError**)error {
	NSDictionary* params = @{EventParamsFilterTerm:searchTerm,EventParamsType:EventTypeAll,EventParamsFilterCommunity:(user.community.key ?: @"25")};
	return [self userGetEvents:user params:params error:error];
}

- (NSDictionary*) userGetEvents:(User*)user params:(NSDictionary*)params error:(NSError**)error {
  
  NSLog(@"user is = %@,params is = %@",user,params);
  CLLocationCoordinate2D location = [LocationManager sharedLocationManager].currentLocation;
  params = [@{ kUserParamsLat:@(location.latitude), kUserParamsLon:@(location.longitude)} merge:params];
  
  return [self dataWithMethod:@"GET" andEndpoint:[NSString stringWithFormat:@"users/%@/events", user.key] andParams:params debug:NO error:error];
}

- (NSArray*) userGetGroupedEvents:(User *)user error:(NSError**)error {
  NSDictionary* params = @{EventParamsType:EventTypeAll};
  NSLog(@" dict is = %@",params);
  return [self dataWithMethod:@"GET" andEndpoint:[NSString stringWithFormat:@"users/%@/events/groupbyday", user.key] andParams:params debug:NO error:error]; 
}

#pragma mark User Action on Event Functions
- (BOOL) userAction:(NSString*)action forUser:(User*)user andEvent:(Event*)event error:(NSError**)error {
  NSDictionary* params = @{kUserParamsAction:action,kUserParamsTimestamp:@((int) [[NSDate date] timeIntervalSince1970])};
	
  [self dataWithMethod:@"PUT" andEndpoint:[NSString stringWithFormat:@"users/%@/events/%@",user.key, event.key] andParams:params debug:NO error:error];
	
	return (*error == nil);
}

- (BOOL) userFeedback:(NSDictionary*)feedback forUser:(User*)user andEvent:(Event*)event error:(NSError**)error {
  NSDictionary* params = [@{kUserParamsAction:EventActionFeedback,kUserParamsTimestamp:@((int) [[NSDate date] timeIntervalSince1970])} merge:feedback];
  
	[self dataWithMethod:@"PUT" andEndpoint:[NSString stringWithFormat:@"users/%@/events/%@",user.key, event.key] andParams:params debug:NO error:error];
	
	return (*error == nil);
}

- (BOOL) userTrack:(NSString*)action forUser:(User*)user andData:(id)data error:(NSError**)error {
  CLLocationCoordinate2D location = [LocationManager sharedLocationManager].currentLocation;
  NSDictionary* params = @{kUserTrackingParamsAction:action, kUserTrackingParamsData:data, kUserParamsLat:@(location.latitude), kUserParamsLon:@(location.longitude)};
  
	[self dataWithMethod:@"POST" andEndpoint:[NSString stringWithFormat:@"users/%@/track",user.key] andParams:params debug:NO error:error];
	
	return (*error == nil);
}

#pragma mark Organization Functions
- (NSDictionary*) organizationGetDetails:(Organization*)org error:(NSError**)error {
	return [self dataWithMethod:@"GET" andEndpoint:[NSString stringWithFormat:@"organizations/%@", org.key] andParams:nil debug:NO error:error];
}

#pragma mark Support Functions
- (id) dataWithMethod:(NSString*) method andEndpoint:(NSString*)endpoint andParams:(NSDictionary*)_params debug:(BOOL)debug error:(NSError**)error {
	NSString* debugParam = debug ? [NSString stringWithFormat:@"&debug=1"] : @"";
	
  NSMutableDictionary* params = [_params mutableCopy];
	
	NSError* jsonError;
	NSString* urlString = [NSString stringWithFormat:@"%@/%@?api_key=%@%@", API_BASE_URL, endpoint, API_KEY, debugParam];
	
	// Setup request params
	if (([method isEqualToString:@"GET"] || [method isEqualToString:@"DELETE"]) && params) {
		// append to url
		urlString = [APIWrapper addQueryStringToUrlString:urlString withDictionary:params];
	}
	
	NSURL* url = [NSURL URLWithString:urlString];
  NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:30];
  [request setHTTPMethod:method];
	[request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	
	if (!([method isEqualToString:@"GET"] || [method isEqualToString:@"DELETE"]) && params) {
		// for everything EXCEPT GET requests
		NSData* requestData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&jsonError];
		if (!requestData) {
			NSLog(@"Error generating JSON for request: %@", jsonError);
			return nil;
		}
    NSLog(@"Sending request data: %@",[[NSString alloc] initWithData:requestData encoding:NSASCIIStringEncoding]);
		NSString* requestDataLengthString = [[NSString alloc] initWithFormat:@"%lu", (unsigned long)[requestData length]];
		[request setValue:requestDataLengthString forHTTPHeaderField:@"Content-Length"];
		[request setHTTPBody:requestData];
	}
	
	// Send request
  NSHTTPURLResponse *response;
	NSError* httpError;
	NSLog(@"Calling API url: %@ %@", method, urlString);

  NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&httpError];
	if(response.statusCode != 200) {
    
    NSString* responseDataString = responseData.length > 0 ? [NSString stringWithUTF8String:[responseData bytes]]: @"";
    
    NSLog(@"Error %ld calling the API. Could not retrieve from %@, error: %@, responseData: %@", (long)response.statusCode, urlString, httpError, responseDataString);
		int statusCode = 0;
		if (response) {
			statusCode = response.statusCode;
    } else if (!response && ((int)[httpError code] == -1012)) {
      // ANNOYING: http://stackoverflow.com/questions/3912532/ios-how-can-i-receive-http-401-instead-of-1012-nsurlerrorusercancelledauthenti
      statusCode = 401;
    }
    *error = [NSError errorWithDomain:@"HTTP" code:statusCode userInfo:@{@"response":responseDataString?:@""}];
    return nil;
  }
	
  if(!responseData) return nil;
  
	// Got a response! 
	jsonError = nil;
	id jsonObject = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&jsonError];
  NSLog(@"%@",jsonObject);
	return jsonObject;
}

// from http://stackoverflow.com/questions/718429/creating-url-query-parameters-from-nsdictionary-objects-in-objectivec
+(NSString*)urlEscapeString:(NSString *)unencodedString {
    CFStringRef originalStringRef = (__bridge_retained CFStringRef)unencodedString;
    NSString *s = (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(NULL,originalStringRef, NULL, NULL,kCFStringEncodingUTF8);
    CFRelease(originalStringRef);
    return s;
}


+(NSString*)addQueryStringToUrlString:(NSString *)urlString withDictionary:(NSDictionary *)dictionary {
    NSMutableString *urlWithQuerystring = [[NSMutableString alloc] initWithString:urlString];
	
    for (id key in dictionary) {
        NSString *keyString = [key description];
        NSString *valueString = [[dictionary objectForKey:key] description];
		
        if ([urlWithQuerystring rangeOfString:@"?"].location == NSNotFound) {
            [urlWithQuerystring appendFormat:@"?%@=%@", [self urlEscapeString:keyString], [self urlEscapeString:valueString]];
        } else {
            [urlWithQuerystring appendFormat:@"&%@=%@", [self urlEscapeString:keyString], [self urlEscapeString:valueString]];
        }
    }
    return urlWithQuerystring;
}


@end
