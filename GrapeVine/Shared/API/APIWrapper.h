//
//  APIWrapper.h
//  GrapeVine
//
//  Created by Jack Kustanowitz on 8/5/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "Organization.h"
#import "LocationManager.h"
#import "Community.h"
#import "Organization.h"
#import "Event.h"

@interface APIWrapper : NSObject
+ (APIWrapper *)api;

// Login/Register
- (NSDictionary*) userLoginWithEmail:(NSString*)email andPassword:(NSString*)password error:(NSError**)error;
- (NSDictionary*) userLoginRegisterWithFacebookAccessToken:(NSString*)token error:(NSError**)error;
- (NSDictionary*) userRegisterWithParams:(NSDictionary*)params error:(NSError**)error;
- (NSDictionary*) userRegisterWithParams:(NSDictionary*)params debug:(BOOL) debug error:(NSError**)error;
- (BOOL) userActivateRegistration:(User*)user withCode:(NSString*)code error:(NSError**)error;
- (BOOL) userDelete:(User*)user error:(NSError**)error;
- (BOOL) userRequestPasswordReset:(NSString*)email error:(NSError**)error;
- (NSDictionary*) userRecover:(User*)user withPassword:(NSString*)password error:(NSError**)error;

// User Profile
- (NSDictionary*) userGetProfile:(User*)user error:(NSError**)error;
- (BOOL) userUpdateInterests:(User*)user error:(NSError**)error;
- (BOOL) userUpdateSocialCommunities:(User*)user error:(NSError**)error;
- (BOOL) userUpdateDetails:(User*)user error:(NSError**)error;
- (BOOL) userUpdateNeighborhood:(User*)user error:(NSError**)error;
- (BOOL) userUpdate:(User *)user withAccessToken:(NSString*)accessToken error:(NSError **)error;

// Events
- (NSDictionary*) userGetRecommendedEvents:(User*)user error:(NSError**)error;
- (NSDictionary*) userGetNearbyEvents:(User*)user error:(NSError**)error;
- (NSDictionary*) userGetEvents:(User*)user fromDate:(NSDate*)startDate toDate:(NSDate*)endDate error:(NSError**)error;
- (NSDictionary*) userGetEvents:(User*)user byNeighborhood:(Neighborhood*)neighborhood error:(NSError**)error;
- (NSDictionary*) userGetEvents:(User*)user byCommunity:(Community*)community error:(NSError**)error;
- (NSDictionary*) userGetEvents:(User*)user byInterest:(Interest*)interest error:(NSError**)error;
- (NSDictionary*) userGetEvents:(User*)user byOrganization:(Organization*)organization error:(NSError**)error;
- (NSDictionary*) userGetEvents:(User*)user byIds:(NSArray*)ids error:(NSError**)error;
- (NSDictionary*) userGetEvents:(User*)user bySearchTerm:(NSString*) query error:(NSError**)error;
- (NSArray*) userGetGroupedEvents:(User*)user error:(NSError**)error;

// Search

//Actions
- (BOOL) userAction:(NSString*)action forUser:(User*)user andEvent:(Event*)event error:(NSError**)error;
- (BOOL) userFeedback:(NSDictionary*)feedback forUser:(User*)user andEvent:(Event*)event error:(NSError**)error;
- (BOOL) userTrack:(NSString*)action forUser:(User*)user andData:(id)data error:(NSError**)error;

// Organizations
- (NSDictionary*) organizationGetDetails:(Organization*)orgId error:(NSError**)error;

@end
