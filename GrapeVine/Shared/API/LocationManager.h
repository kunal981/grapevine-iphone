//
//  LocationManager.h
//  GrapeVine
//
//  Created by Jack Kustanowitz on 8/21/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationManager : NSObject <CLLocationManagerDelegate> {
	CLLocationManager *mgr;
	id delegate;
}

+ (LocationManager *)sharedLocationManager;
- (void) fetchLocation;

@property(readonly) CLLocationCoordinate2D currentLocation;


@end
