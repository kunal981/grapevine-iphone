//
//  LocationManager.m
//  GrapeVine
//
//  Created by Jack Kustanowitz on 8/21/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "LocationManager.h"

@implementation LocationManager

+ (LocationManager *)sharedLocationManager  {
    static LocationManager *instance;
	
    @synchronized(self) {
        if(!instance) {
            instance = [[LocationManager alloc] init];
        }
    }
	
    return instance;
}

- (CLLocationManager*) mgr {
  @synchronized(self) {
    if(!mgr) {		// custom init
      mgr = [[CLLocationManager alloc] init];
      mgr.delegate = self;
  
        int value=[[[UIDevice currentDevice] systemVersion]intValue];
        NSLog(@"%d",value);
        if (value>=8)
        {
            [mgr requestWhenInUseAuthorization];
            [mgr requestAlwaysAuthorization];
            
        }

    }
}
  return mgr;
}

- (void) fetchLocation {
	[self.mgr startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
	_currentLocation.latitude = newLocation.coordinate.latitude;
	_currentLocation.longitude = newLocation.coordinate.longitude;
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"LocationNotification" object:self];
	
	// don't need to drain the battery
	[self.mgr stopUpdatingLocation];
  [self.mgr stopMonitoringSignificantLocationChanges];
  
  mgr = nil;
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
	// we'll hit this if the user doesn't allow gps, default to NYC
  _currentLocation = CLLocationCoordinate2DMake(40.7142, -74.0064);
  
  [[NSNotificationCenter defaultCenter] postNotificationName:@"LocationNotification" object:self];
  
  // Dnt' need the manager anymore
  [self.mgr stopUpdatingLocation];
  [self.mgr stopMonitoringSignificantLocationChanges];
  
  mgr = nil;
}

@end
