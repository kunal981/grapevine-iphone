//
//  IsLoadingAlertView.h
//  GrapeVine
//
//  Created by Zachary Gavin on 9/5/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TapkuLibrary/TapkuLibrary.h>

@interface IsLoadingView : UIView 

- (id) initWithTarget:(UIView*) target;

- (void) show;
- (void) hide;

@property (readonly) UIView* target;
@property (readonly) TKProgressCircleView* circleView;

@end
