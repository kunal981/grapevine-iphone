//
//  RemoteImageCache.m
//  Grapevine
//
//  Created by Zachary Gavin on 10/9/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "RemoteImageCache.h"
#import <CommonCrypto/CommonDigest.h>

@implementation RemoteImageCache

static NSMutableDictionary* all;

+ (void) initialize {
  all = [@{} mutableCopy];
}

+ (void) getImageAtURL:(NSString *)url withCompletionBlock:(RemoteImageCallback)callback {
	@synchronized (all) {
		[(all[url] ?: [[RemoteImageCache alloc] initWithURL:url]) registerCallback:callback];
	}
}

- (id) initWithURL:(NSString*)_url {
  if( self = [super init] ) {
    url = _url;
		
		@synchronized (all) {
			all[url] = self;
		}
		
		@synchronized (self) {
			callbacks = [@[] mutableCopy];
		}
		
		[self fetchImage];
  }
  return self;
}

- (void) registerCallback:(RemoteImageCallback)callback {
	if ( !callback ) return;
	
	@synchronized(self) {
		if ( image || callbacks == nil ) {
			callback(image);
		} else {
			[callbacks addObject:callback];
		}
	}
}

- (NSString*) sha256:(NSString*)str {
  NSData* data = [str dataUsingEncoding:NSASCIIStringEncoding];
  NSMutableString* ret = [@"" mutableCopy];
  unsigned char hash[CC_SHA256_DIGEST_LENGTH];
  
  if ( CC_SHA256([data bytes], [data length], hash) ) {
    for (int i=0; i<CC_SHA256_DIGEST_LENGTH; i++) {
      [ret appendString:[NSString stringWithFormat:@"%x",hash[i]]];
    }
  }
  
  return ret;
}

- (NSString*) filePath {
  if( !_filePath ) {
		_filePath =  [self.class.cachePath stringByAppendingPathComponent:[self sha256:url]];
	}
	
  return _filePath;
}

- (void) fetchImage {
  if ( [[NSFileManager defaultManager] fileExistsAtPath:self.filePath] ) {
    [self loadImage];
  } else {
		NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:30];
		
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse* response,NSData* data,NSError* error) {
			if( error || ! [data writeToFile:self.filePath atomically:YES]) {
				[self runCallbacks];
			} else {
				[self loadImage];
			}
    }];
  }
}

- (void) loadImage {
  image = [UIImage imageWithData:[NSData dataWithContentsOfFile:self.filePath]];
	
	[self runCallbacks];
}

- (void) runCallbacks {
	@synchronized(self) {
		for (RemoteImageCallback callback in callbacks) {
			callback(image);
		}
		
		callbacks = nil;
	}
	
	@synchronized(all) {
		[all removeObjectForKey:url];
	}
}

static NSString* cachePath;
+ (NSString*) cachePath {
	if(!cachePath) {
		NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
		
		NSString* tmpPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"RemoteImages"];
		
		
		BOOL isDir = NO;
		NSError *error;
		if ( ![[NSFileManager defaultManager] fileExistsAtPath:tmpPath isDirectory:&isDir] && isDir == NO ) {
			[[NSFileManager defaultManager] createDirectoryAtPath:tmpPath withIntermediateDirectories:YES attributes:nil error:&error];
			
			if( !error ) cachePath = tmpPath;
		} else {
			cachePath = tmpPath;
		}
	}
	
	return cachePath;
}

+ (void) purgeExpiredImages {
	NSFileManager* manager = NSFileManager.defaultManager;
	
	for(NSString* fileName in [manager contentsOfDirectoryAtPath:[self cachePath] error:nil]) {
		NSString* file = [self.cachePath stringByAppendingPathComponent:fileName];
		
		NSDate* updateDate = [manager attributesOfItemAtPath:file error:nil][NSFileModificationDate];
		
		if(updateDate.timeIntervalSinceReferenceDate + 60*60*24*45 < [NSDate timeIntervalSinceReferenceDate]) {
			[manager removeItemAtPath:file error:nil];
		}
	}
}

@end
