//
//  UILoadingImageView.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/17/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "RemoteImageView.h"
#import "RemoteImageCache.h"


@implementation RemoteImageView


- (void) setUrl:(NSString *)url {
  
  __block CGFloat scale = [UIScreen mainScreen].scale;
  _url = url;
  
  NSString* resizedUrl = self.attemptResize ? [NSString stringWithFormat:@"%@%@height=%d&width=%d&mode=max",
                            url,
                            ([url rangeOfString:@"?"].location == NSNotFound ? @"?" : @"&"),
                            ((int) (self.frame.size.height*scale)),
                            ((int) (self.frame.size.width*scale))
                         ] : url ;
  
  [RemoteImageCache getImageAtURL:resizedUrl withCompletionBlock:^(UIImage* image){
    self.image = image.scale == scale ? image : [UIImage imageWithCGImage:image.CGImage scale:scale orientation:image.imageOrientation];
  }];
}



@end
