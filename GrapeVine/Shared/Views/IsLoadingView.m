//
//  IsLoadingAlertView.m
//  GrapeVine
//
//  Created by Zachary Gavin on 9/5/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "IsLoadingView.h"

@implementation IsLoadingView

@synthesize target = _target, circleView = _circleView;

- (id) initWithTarget:(UIView *)target {
  if(self = [self initWithFrame:CGRectZero]) {
    _target = target;
    self.backgroundColor = [UIColor clearColor];
  }
  return self;
}

- (void) drawRect:(CGRect)rect{
	[[UIColor colorWithRed:0 green:0 blue:0 alpha:0.8] set];
  
  self.circleView.center = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
  
  
  CGContextRef context = UIGraphicsGetCurrentContext();
  
  
  // Draw the background gradient
  CGGradientRef glossGradient;
  CGColorSpaceRef rgbColorspace;
  size_t num_locations = 2;
  CGFloat locations[2] = { 0.0, 1.0 };
  CGFloat components[8] = { 0.3, 0.3, 0.3, 0.3,  // Start color
    0.0, 0.0, 0.0, 0.8 }; // End color
  
  rgbColorspace = CGColorSpaceCreateDeviceRGB();
  glossGradient = CGGradientCreateWithColorComponents(rgbColorspace, components, locations, num_locations);
  
  CGRect currentBounds = self.bounds;
  
  CGPoint center = CGPointMake(CGRectGetMidX(currentBounds), CGRectGetMidY(currentBounds));
  
  CGContextDrawRadialGradient(context, glossGradient, center, 0, center, MIN(currentBounds.size.width,currentBounds.size.height), 0);
  CGGradientRelease(glossGradient);
  CGColorSpaceRelease(rgbColorspace);
  
  // Draw the circle background
  rect = CGRectMake(CGRectGetMidX(rect)-25, CGRectGetMidY(rect)-25, 50, 50);
  CGFloat radius = 25;
  
  CGContextSetRGBFillColor(context, 0,0,0,0.6);
  
  CGContextMoveToPoint(context, rect.origin.x, rect.origin.y + radius);
  CGContextAddLineToPoint(context, rect.origin.x, rect.origin.y + rect.size.height - radius);
  CGContextAddArc(context, rect.origin.x + radius, rect.origin.y + rect.size.height - radius,
                  radius, M_PI, M_PI / 2, 1); //STS fixed
  CGContextAddLineToPoint(context, rect.origin.x + rect.size.width - radius,
                          rect.origin.y + rect.size.height);
  CGContextAddArc(context, rect.origin.x + rect.size.width - radius,
                  rect.origin.y + rect.size.height - radius, radius, M_PI / 2, 0.0f, 1);
  CGContextAddLineToPoint(context, rect.origin.x + rect.size.width, rect.origin.y + radius);
  CGContextAddArc(context, rect.origin.x + rect.size.width - radius, rect.origin.y + radius,
                  radius, 0.0f, -M_PI / 2, 1);
  CGContextAddLineToPoint(context, rect.origin.x + radius, rect.origin.y);
  CGContextAddArc(context, rect.origin.x + radius, rect.origin.y + radius, radius,
                  -M_PI / 2, M_PI, 1);
  
  CGContextFillPath(context);
}


- (void) show {
  self.frame = self.target.frame;
  [self.target.superview insertSubview:self aboveSubview:self.target];
}

- (void) hide {
	[self removeFromSuperview];
}

- (TKProgressCircleView *) circleView {
	if(_circleView==nil){
    _circleView = [[TKProgressCircleView alloc] init];
    _circleView.twirlMode = YES;
    [self addSubview:self.circleView];
	}
	return _circleView;
}

@end
