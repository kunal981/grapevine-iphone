//
//  PickerButton.h
//  Grapevine
//
//  Created by Zachary Gavin on 10/10/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PickerButton;

@protocol PickerButtonDelegate <NSObject>

@optional

- (void) pickerButton:(PickerButton*)pickerButton didChangeValue:(NSString*)value;
- (void) pickerButtonWillShowPicker:(PickerButton *)pickerButton;

@end

@interface PickerButton : UIButton <UIPickerViewDelegate,UIPickerViewDataSource> {
  UIPickerView* pickerView;
  UIView* container;
}

@property (nonatomic,weak) IBOutlet id<PickerButtonDelegate> delegate;
@property (nonatomic,strong) NSArray* options;
@property (nonatomic,strong) UIView* pickerTarget;
@property (nonatomic,assign) NSInteger selectedIndex;
@property (readonly) NSString* value;

@end

