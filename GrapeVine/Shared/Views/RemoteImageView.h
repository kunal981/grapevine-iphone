//
//  UILoadingImageView.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/17/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RemoteImageView : UIImageView

@property (nonatomic,retain) NSString* url;

@property (nonatomic,assign) BOOL attemptResize;

@end
