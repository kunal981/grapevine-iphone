//
//  ColoredPlaceholderTextField.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/15/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "ColoredPlaceholderTextField.h"

@implementation ColoredPlaceholderTextField

- (void) drawPlaceholderInRect:(CGRect)rect {
  [[UIColor lightGrayColor] setFill];
  //[[self placeholder] drawInRect:rect withFont:self.font];
}

@end
