//
//  PickerButton.m
//  Grapevine
//
//  Created by Zachary Gavin on 10/10/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "PickerButton.h"

@implementation PickerButton

- (id)initWithFrame:(CGRect)frame {
  if (self = [super initWithFrame:frame]) {
    [self setup];
  }
  return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder {
  if(self = [super initWithCoder:aDecoder]) {
    [self setup];
  }
  return self;
}

- (void) setup {
  UIToolbar* toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
  toolbar.barStyle = UIBarStyleBlackTranslucent;
  
  NSArray* items = @[
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(donePressed:withEvent:)]
  ];
  
  [toolbar setItems:items];
  
  pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, toolbar.frame.size.height, 320, 216)];
  pickerView.delegate = self;
  pickerView.dataSource = self;
  pickerView.showsSelectionIndicator = YES;
  
  container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, pickerView.frame.size.height+toolbar.frame.size.height)];
  [container addSubview:toolbar];
  [container addSubview:pickerView];
  
  [self addTarget:self action:@selector(selfPressed:withEvent:) forControlEvents:UIControlEventTouchUpInside];
  
  [self setBackgroundImage:[[self backgroundImageForState:UIControlStateNormal] resizableImageWithCapInsets:UIEdgeInsetsMake(1, 5, 1, 23)] forState:UIControlStateNormal];
  [self setBackgroundImage:[[self backgroundImageForState:UIControlStateSelected] resizableImageWithCapInsets:UIEdgeInsetsMake(1, 5, 1, 23)] forState:UIControlStateSelected];
}

- (NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView {
  return 1;
}

- (NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
  return self.options.count;
}

- (NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
  return [self stringForRow:row];
}

- (NSString*) stringForRow:(NSInteger)row {
  return row >= self.options.count ? @"" : self.options[row];
}

- (void) setOptions:(NSArray *)options {
  _options = options;
  [pickerView reloadAllComponents];

  [self setTitle:[self stringForRow:0] forState:UIControlStateNormal];
}

- (void) donePressed:(UIControl*)button withEvent:(UIEvent*)event {
  [self selectRow:[pickerView selectedRowInComponent:0]];
  [self resignFirstResponder];
}

- (UIView*) pickerTarget {
  return self.window;
}

- (BOOL) canBecomeFirstResponder {
  return YES;
}

- (BOOL) resignFirstResponder {
  self.selected = NO;
  
  if(container.superview) {
    [UIView animateWithDuration:0.5 animations:^{
      CGRect frame = container.frame;
      frame.origin.y = frame.origin.y+frame.size.height;
      container.frame = frame;
    } completion:^(BOOL finished) {
      if(finished) [container removeFromSuperview];
    }];
  }
  
  return [super resignFirstResponder];
}

- (void) selfPressed:(UIControl*)button withEvent:(UIEvent*)event {
  self.selected = YES;
  
  [self becomeFirstResponder];
  
  CGRect frame = container.frame;
  frame.origin.y = self.pickerTarget.frame.size.height;
  container.frame = frame;
  [self.pickerTarget addSubview:container];
  
  if([self.delegate respondsToSelector:@selector(pickerButtonWillShowPicker:)]) [self.delegate pickerButtonWillShowPicker:self];
  
  [UIView animateWithDuration:0.5 animations:^{
    CGRect frame = container.frame;
    frame.origin.y = frame.origin.y-frame.size.height;
    container.frame = frame;
  }];
}

- (void) setSelectedIndex:(NSInteger)selectedIndex {
  [self selectRow:selectedIndex];
}

- (void) selectRow:(NSInteger)row {
  _value = [self stringForRow:row];
  
  _selectedIndex = row;
  
  [pickerView selectRow:row inComponent:0 animated:NO];
  
  if([self.delegate respondsToSelector:@selector(pickerButton:didChangeValue:)]) {
    [self.delegate pickerButton:self didChangeValue:self.value];
  }
  
  [self setTitle:self.value forState:UIControlStateNormal];
}

@end
