//
//  RemoteImageCache.h
//  Grapevine
//
//  Created by Zachary Gavin on 10/9/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^RemoteImageCallback)(UIImage*);

@interface RemoteImageCache : NSObject  {
  NSString* url;
  UIImage* image;
  NSMutableArray* callbacks;
  
  NSString* _filePath;
}

+ (void) getImageAtURL:(NSString*)url withCompletionBlock:(RemoteImageCallback)callback;

@end
