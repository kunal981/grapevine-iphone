//
//  ColoredPlaceholderTextField.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/15/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColoredPlaceholderTextField : UITextField

@end
