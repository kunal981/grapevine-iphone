//
//  AppDelegate.m
//  GrapeVine
//
//  Created by Zachary Gavin on 7/23/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "AppDelegate.h"
#import "NSString+Utility.h"
#import "LocationManager.h"
#import "NSArray+Subscript.h"
#import "User.h"
#import "UIViewController+IsLoading.h"
#import "APIWrapper.h"
#import "LoginRecoverPasswordController.h"
#import <TapkuLibrary/TapkuLibrary.h>
#import <FacebookSDK/FacebookSDK.h>
#import "NSObject+Introspection.h"
#import "Event.h"
#import "Neighborhood.h"
#import "ProfileInterestsController.h"
#import "GANTracker.h"
#import "LocationManager.h"
#import "EventController.h"
//#import "UINavigationItem+TitleView.h"


@implementation AppDelegate
@synthesize mainTabBarController;
static AppDelegate* currentDelegate;

- (id) init {
  if (self = [super init]) {
    currentDelegate = self;
  }
  return self;
}

+ (AppDelegate*) currentDelegate {
  return currentDelegate;
}

- (void) transitionToController:(UIViewController*)controller withTransitionOptions:(UIViewAnimationOptions)transitionOptions {
  [UIView transitionWithView:self.window duration:0.5 options:transitionOptions animations:^{
    self.window.rootViewController = controller;
  } completion:nil];
}


- (void) transitionToHome:(UIViewAnimationOptions)transitionOptions {
  
  for(UIViewController* controller in self.mainTabBarController.viewControllers) {
    if([controller isKindOfClass:[UINavigationController class]])
      [(UINavigationController*) controller popToRootViewControllerAnimated:YES];
    
    
    
  
  }
  
  self.mainTabBarController.selectedIndex = 0;
  
  [self transitionToController:self.mainTabBarController withTransitionOptions:transitionOptions];
}

- (void) transitionToLogin:(UIViewAnimationOptions)transitionOptions {
  [self.loginNavigationController popToRootViewControllerAnimated:NO];
  [self transitionToController:self.loginNavigationController withTransitionOptions:transitionOptions];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
//  NSError* error;
//  User* u = (User*) [User objectWithKey:@""];
//  u.email = @"zgavin@gmail.com";
//  [APIWrapper.api userDelete:u error:&error];

//  [[NSUserDefaults standardUserDefaults] setObject:@"0f5145715c034400bcc3342eeec77ed2" forKey:@"GrapeVineUserIdentifier"];

  [Crittercism initWithAppID: @"5079f16a01ed85595f000004"];
  
  //_facebook = [[Facebook alloc] initWithAppId:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"FacebookAppID"] andDelegate:nil];
   
  [[UINavigationBar appearance] setTitleTextAttributes:@{UITextAttributeFont:[UIFont fontWithName:@"Futura-Medium" size:19]}];
  
  [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"bg_header.png"] forBarMetrics:UIBarMetricsDefault];
  
  UIImage *backButton = [[UIImage imageNamed:@"backButton.png"]  resizableImageWithCapInsets:UIEdgeInsetsMake(5, 13, 5, 5)];
  
  [[UIBarButtonItem appearance] setBackButtonBackgroundImage:backButton forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
  
  [[UIBarButtonItem appearance] setBackButtonBackgroundVerticalPositionAdjustment:-1 forBarMetrics:UIBarMetricsDefault];
  
  [[UIBarButtonItem appearance] setBackgroundImage:[[UIImage imageNamed:@"barButton.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 5 )]forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
  
  [[UIBarButtonItem appearance] setTitleTextAttributes: @{UITextAttributeTextColor:[UIColor colorWithRed:92.0/255 green:3.0/255 blue:45.0/255 alpha:1],UITextAttributeFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:12], UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0, 0)]} forState: UIControlStateNormal];
  
  [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, 1) forBarMetrics:UIBarMetricsDefault];
  
  
  // UISegmentedControl
  UIFont* font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
  [[UISegmentedControl appearance] setTitleTextAttributes:@{UITextAttributeFont:font,UITextAttributeTextColor:[UIColor whiteColor]} forState:UIControlStateSelected];
  [[UISegmentedControl appearance] setTitleTextAttributes:@{UITextAttributeFont:font,UITextAttributeTextColor:[UIColor colorWithRed:92.0/255 green:3.0/255 blue:45.0/255 alpha:1.0]} forState:UIControlStateNormal];
  

  UIImage *segmentSelected =[[UIImage imageNamed:@"segmentedControl.button.selected.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 6, 0, 6)];
  UIImage *segmentUnselected = [[UIImage imageNamed:@"segmentedControl.button.unselected.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 5, 0, 5 )];
  
  [[UISegmentedControl appearance] setBackgroundImage:segmentUnselected forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
  [[UISegmentedControl appearance] setBackgroundImage:segmentSelected forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
  
  [[UISegmentedControl appearance] setDividerImage:[UIImage imageNamed:@"segmentedControl.divider.unselectedunselected.png"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
  [[UISegmentedControl appearance] setDividerImage:[UIImage imageNamed:@"segmentedControl.divider.selectedunselected.png"] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
  [[UISegmentedControl appearance] setDividerImage:[UIImage imageNamed:@"segmentedControl.divider.unselectedselected.png"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
  
  //removes sharekit cache for action sheet options
  [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"SHK_FAVS_1"];
  
  [[GANTracker sharedTracker] startTrackerWithAccountID:@"UA-27193481-2" dispatchPeriod:60 delegate:nil];
	
  [self.window makeKeyAndVisible];
	
  
	// Let the device know you want to receive push notifications
	[[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
  
  return YES;
}
#pragma mark Push Notifications
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken {
	NSLog(@"Got device token: %@", [devToken description]);
	
	//    [self sendProviderDeviceToken:[devToken bytes]]; // custom method; e.g., send to a web service and store
	
	// from urban airship via http://stackoverflow.com/questions/5021642/need-help-sending-a-url-with-device-token-to-our-database-from-our-iphone-app
	NSString *deviceToken = [[devToken description] stringByReplacingOccurrencesOfString: @"<" withString: @""];
	deviceToken = [deviceToken stringByReplacingOccurrencesOfString: @">" withString: @""] ;
	deviceToken = [deviceToken stringByReplacingOccurrencesOfString: @" " withString: @""];
	
	[[NSUserDefaults standardUserDefaults] setValue:deviceToken forKey:@"apnid"];
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
	NSLog(@"Error in registration. Error: %@", err);
}

- (void) application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
	NSLog(@"Recieved Remote Notification %@",userInfo);
	NSString* offerId = [userInfo objectForKey:@"o"];
	
	[self showEventVCForId:offerId];

}

-(void) showEventVCForId:(NSString*) offerId
{
	// get currently visible VC
	UINavigationController* navController = self.homeController.navigationController;
  self.mainTabBarController.selectedIndex = 0;
	navController.isLoading = YES;
	
	[Event eventsForUser:[User currentUser] byIds:@[@(offerId.integerValue)] withCompletionBlock:^(NSDictionary* eventsDict, NSError * error) {
		NSLog(@"EventsDict for push notification: %@", eventsDict );
		
		// grab the event
		NSArray* eventArray = eventsDict[EventTypeAll];
		NSLog(@"Event for push notification: %@", eventArray[0] );
		Event* event = eventArray[0];
		
		// set up the event VC
		EventController* eventVC = nil;
		if ([[((UINavigationController*)self.mainTabBarController.selectedViewController) topViewController] isKindOfClass:[EventController class]]) {
			eventVC = (EventController*) [((UINavigationController*)self.mainTabBarController.selectedViewController) topViewController];
			eventVC.event = event;
		} else {
			eventVC = [[EventController alloc] init];
			eventVC.event = event;
			[navController pushViewController:eventVC animated:YES]	;
		}
		
		// and show it
		navController.isLoading = NO;
	}];
}

#pragma mark URLs
- (void) openURLGrapevineReset:(NSArray*)pathComponents {
  self.window.rootViewController = self.loginNavigationController;
  LoginRecoverPasswordController* controller = [[LoginRecoverPasswordController alloc] init];
  controller.user = (User*) [User objectWithDictionary:@{User.keyName:pathComponents[0],kUserParamsConfirmationCode:pathComponents[1]}];
  [self.loginNavigationController pushViewController:controller animated:YES];
}

- (void) openURLGrapevineO:(NSArray*)pathComponents {
	[self showEventVCForId:pathComponents[0]];
}

- (void) openURLGrapevineActivate:(NSArray*)pathComponents {
  [User logout];
  
  self.window.rootViewController = self.loginNavigationController;

  self.loginNavigationController.topViewController.isLoading = YES;
  [(User*) [User objectWithKey:pathComponents[0]] activateWithCode:pathComponents[1] andCompletionBlock:^(NSError* error) {
    self.loginNavigationController.topViewController.isLoading = NO;
    if(!error) {
      ProfileInterestsController* controller = [[ProfileInterestsController alloc] init];
      [self transitionToLogin:UIViewAnimationOptionTransitionFlipFromRight];
      [self.loginNavigationController pushViewController:controller animated:NO];
    } else {
      [[TKAlertCenter defaultCenter] postAlertWithMessage:@"That activation link is not valid"];
    }
  }];

}

- (void)applicationWillEnterForeground:(UIApplication *)application {
  [User.currentUser refreshProfileWithCompletionBlock:nil];
  
  [[LocationManager sharedLocationManager] fetchLocation];

	// use this for testing push notifications
//	[self application:application didReceiveRemoteNotification:@{
//		@"aps": @{
//			@"alert": @"Check out your top recommendation this week: Family Farm Day",
//			@"badge": @"7",
//			@"sound": @"sound.caf"
//	 },
//		@"o": @"932"
//	}	 ];
}

- (BOOL) application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
  NSArray* pathComponents = [url.pathComponents subarrayWithRange:NSMakeRange(1, url.pathComponents.count-1)];
  
  SEL selector = NSSelectorFromString([NSString stringWithFormat:@"openURL%@%@%@%@:",[url.scheme substringToIndex:1].uppercaseString,[url.scheme substringFromIndex:1],[url.host substringToIndex:1].uppercaseString,[url.host substringFromIndex:1]]);

  if([self respondsToSelector:selector]) {
    NSMethodSignature *methodSig = [[self class] instanceMethodSignatureForSelector:selector];
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:methodSig];
    [invocation setSelector:selector];
    [invocation setTarget:self];
    [invocation setArgument:&pathComponents atIndex:2];
    
    [invocation invoke];
  }

  
  return  YES;
}


@end
