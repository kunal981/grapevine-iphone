//
//  AppDelegate.h
//  GrapeVine
//
//  Created by Zachary Gavin on 7/23/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Crittercism.h" 
//#import "Facebook.h"
#import "HomeController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong,nonatomic) IBOutlet UIWindow *window;
@property (strong,nonatomic) IBOutlet UINavigationController* loginNavigationController;
@property (strong,nonatomic) IBOutlet UIViewController* splashController;
@property (strong,nonatomic) IBOutlet UITabBarController* mainTabBarController;
@property (strong,nonatomic) IBOutlet HomeController* homeController;

//@property (strong,nonatomic) Facebook* facebook;

+ (AppDelegate*) currentDelegate;

- (void) transitionToLogin:(UIViewAnimationOptions)transitionOptions;
- (void) transitionToHome:(UIViewAnimationOptions)transitionOptions;

@end
