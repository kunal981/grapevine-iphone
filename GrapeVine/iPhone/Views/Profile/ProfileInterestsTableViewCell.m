//
//  ProfileInterestsTableViewCell.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/24/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "ProfileInterestsTableViewCell.h"

@implementation ProfileInterestsTableViewCell

- (void) setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];

  button.selected = selected;
}

@end
