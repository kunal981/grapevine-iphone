//
//  ProfileInterestsTableViewCell.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/24/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileInterestsTableViewCell : UITableViewCell {
  IBOutlet UIButton* button;
}


@property (nonatomic,retain) IBOutlet UILabel* label;

@end
