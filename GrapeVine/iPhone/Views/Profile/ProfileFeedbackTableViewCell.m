//
//  ProfilePastEventFeedbackView.m
//  Grapevine
//
//  Created by Zachary Gavin on 10/5/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "ProfileFeedbackTableViewCell.h"
#import "Event.h"

@interface ProfileFeedbackTableViewCell ()

@end

@implementation ProfileFeedbackTableViewCell


static NSDictionary* feedbackTypeMap;
+ (void) initialize {
  feedbackTypeMap = @{
    EventFeedbackOverall:@"Overall, it was high quality",
    EventFeedbackWelcomed:@"I felt welcomed by the leadership",
    EventFeedbackConnections:@"I made a strong social connection",
    EventFeedbackInvolvement:@"Made me want to get more involved",
    EventFeedbackResonated:@"This resonated with me",
    EventFeedbackRecommend: @"I would recommend this to a friend",
  };
}

- (IBAction) buttonPressed:(id)sender {
  self.value = ((UIView*) sender).tag;
  if([self.delegate respondsToSelector:@selector(feedbackCell:userDidChangeValueTo:)]) [self.delegate feedbackCell:self userDidChangeValueTo:_value];
}

- (void) setFeedbackKey:(NSString *)feedbackKey {
  _feedbackKey = feedbackKey;
  titleLabel.text = feedbackTypeMap[_feedbackKey];
}

- (void) setValue:(NSInteger)value {
  _value = value;
  noneButton.selected = _value == 0;
  for (UIButton* button in starButtonsContainer.subviews) {
    button.selected = button.tag <= _value;
  }
}


@end
