//
//  ProfilePastEventsTableViewCell.m
//  Grapevine
//
//  Created by Zachary Gavin on 10/4/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "ProfilePastEventsTableViewCell.h"

@implementation ProfilePastEventsTableViewCell

- (void) awakeFromNib {
  [super awakeFromNib];
  [feedbackButton addTarget:self action: @selector(feedbackButtonTapped:withEvent:) forControlEvents:UIControlEventTouchUpInside];
}

- (void) setEvent:(Event *)event {
  _event = event;
  titleLabel.text = event.title;
  
  NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
  formatter.locale = [NSLocale currentLocale];
  formatter.dateFormat = @"MMM d, yyyy | h:mm a";
  
  dateLabel.text = [formatter stringFromDate:event.startTime];

  feedbackButton.hidden = event.userFeedback;
}

- (void) feedbackButtonTapped: (UIControl *) button withEvent: (UIEvent *) event {
  UIView* view = self.superview;
  while (![view isKindOfClass:[UITableView class]]) view = view.superview;
  UITableView* table = (UITableView*) view;
  
  NSIndexPath * indexPath = [table indexPathForRowAtPoint: [[[event touchesForView: button] anyObject] locationInView:table]];
  
  if (indexPath == nil) return;
  
  [table.delegate tableView:table accessoryButtonTappedForRowWithIndexPath:indexPath];
}
@end
