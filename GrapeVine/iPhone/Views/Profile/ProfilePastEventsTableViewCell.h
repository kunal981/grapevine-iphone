//
//  ProfilePastEventsTableViewCell.h
//  Grapevine
//
//  Created by Zachary Gavin on 10/4/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"

@interface ProfilePastEventsTableViewCell : UITableViewCell {
  IBOutlet UILabel* titleLabel;
  IBOutlet UILabel* dateLabel;
  IBOutlet UIButton* feedbackButton;
}

@property (nonatomic,strong) Event* event;

@end
