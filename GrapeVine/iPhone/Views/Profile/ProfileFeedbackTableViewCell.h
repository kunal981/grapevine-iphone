//
//  ProfilePastEventFeedbackView.h
//  Grapevine
//
//  Created by Zachary Gavin on 10/5/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ProfileFeedbackTableViewCell;

@protocol ProfileFeedbackTableViewCellDelegate <NSObject>

- (void) feedbackCell:(ProfileFeedbackTableViewCell*)feedbackCell userDidChangeValueTo:(NSInteger)value;

@end

@interface ProfileFeedbackTableViewCell : UITableViewCell {
  IBOutlet UILabel* titleLabel;
  IBOutlet UIButton* noneButton;
  IBOutlet UIView* starButtonsContainer;
}

@property (nonatomic,weak) id<ProfileFeedbackTableViewCellDelegate> delegate;
@property (nonatomic,strong) NSString* feedbackKey;
@property (nonatomic,assign) NSInteger value;


@end


