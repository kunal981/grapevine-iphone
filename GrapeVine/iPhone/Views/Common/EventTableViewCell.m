//
//  EventTableViewCell.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/4/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "EventTableViewCell.h"
#import "User.h"
#import "GANTracker.h"
#import "UIImageView+WebCache.h"

@implementation EventTableViewCell

- (void) setEvent:(Event *)event {

  _event = event;
    NSLog(@"%@",_event.title);
  //titleLabel.text =_event.title;
  titleLabel.text=[event valueForKey:@"title"];
  //self.main_ImageView.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[event valueForKey:@"image_url"]]]];
  NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
  formatter.locale = [NSLocale currentLocale];
  formatter.dateFormat = @"MMM d, yyyy, h:mm a";
  [self.main_ImageView sd_setImageWithURL:[NSURL URLWithString:[event valueForKey:@"image_url"]] placeholderImage:[UIImage imageNamed:@"Default.png"] options:SDWebImageRefreshCached];

     
	/*if (event.isContent) {
		CGRect frame = dateLabel.frame;
		frame.size.height = 42;
		frame.size.width = 229;
		frame.origin.y = 28;
		dateLabel.frame = frame;
		dateLabel.numberOfLines = 3;
		UIFont* font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:10];
		dateLabel.font = font;
		dateLabel.text = event.description;
		[dateLabel sizeToFit];
		neighborhoodLabel.hidden = YES;
	} else {
		CGRect frame = dateLabel.frame;
		frame.size.height = 21;
		frame.size.width = 229;
		frame.origin.y = 28;
		dateLabel.frame = frame;
		dateLabel.numberOfLines = 1;

		dateLabel.text = [formatter stringFromDate:event.startTime];
		UIFont* font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12];
		dateLabel.font = font;
		[dateLabel sizeToFit];
		neighborhoodLabel.hidden = NO;
	}
  
  organizationLabel.text = event.organization.name;
  neighborhoodLabel.text = event.neighborhood.name;
  CGRect frame = neighborhoodLabel.frame;
  frame.size.width = [neighborhoodLabel sizeThatFits:CGSizeMake(150, frame.size.height)].width;
  neighborhoodLabel.frame = frame;
  
  frame = organizationLabel.frame;
  frame.size.width = MIN([organizationLabel sizeThatFits:organizationLabel.bounds.size].width,242-(frame.origin.x+lowerDivider.frame.size.width+neighborhoodLabel.frame.size.width));
  organizationLabel.frame = frame;
  
  neighborhoodLabel.hidden = event.isContent;
  organizationLabel.hidden = event.isContent;
  lowerDivider.hidden = event.isContent;
  
  frame = lowerDivider.frame;
  frame.origin.x = CGRectGetMaxX(organizationLabel.frame);
  lowerDivider.frame = frame;
  
  frame = neighborhoodLabel.frame;
  frame.origin.x =  CGRectGetMaxX(lowerDivider.frame);
  neighborhoodLabel.frame = frame;
  
  likeButton.selected = event.userLike;
  notlikeButton.selected = event.userNotLike;
  
  friendsImage.hidden = !event.hasFacebookFriends;
  bookmarkedImage.hidden = !event.userBookmarked;
  
  frame = bookmarkedImage.frame;
  frame.origin.y = friendsImage.frame.origin.y + (friendsImage.hidden ? 0 : (friendsImage.frame.size.height+8)) ;
  bookmarkedImage.frame = frame;*/
}

- (void) setHighlight:(EventCellHighlight)highlight {
  _highlight = highlight;
  [@{ @(EventCellHighlightDate) : dateLabel, @(EventCellHighlightDistance) : neighborhoodLabel } enumerateKeysAndObjectsUsingBlock:^(NSNumber* key, UILabel* label,BOOL* stop) {
    label.textColor = _highlight == key.unsignedIntegerValue && !self.event.isContent ? [UIColor colorWithRed:114.0/255 green:158.0/255 blue:29.0/255 alpha:1]: [UIColor colorWithRed:75.0/255 green:73.0/255 blue:80.0/255 alpha:1];
  }];
}

- (IBAction) likePressed:(id)sender {
  likeButton.selected = !likeButton.selected;
  notlikeButton.selected = NO;
  [self.event setUserLike:likeButton.selected sync:YES];
  
  if(likeButton.selected) [[GANTracker sharedTracker] trackEvent:@"Interaction" action:@"Like" label:((NSNumber*) self.event.key).stringValue value:0 withError:nil];
}

- (IBAction) notlikePressed:(id)sender {
  notlikeButton.selected = !notlikeButton.selected;
  likeButton.selected = NO;
  [self.event setUserNotLike:notlikeButton.selected sync:YES];
  
  if(notlikeButton.selected) [[GANTracker sharedTracker] trackEvent:@"Interaction" action:@"Dislike" label:((NSNumber*) self.event.key).stringValue value:0 withError:nil];
}

@end
