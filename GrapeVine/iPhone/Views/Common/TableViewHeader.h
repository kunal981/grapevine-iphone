//
//  TableViewHeader.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/17/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewHeader : UIView

@property (readonly) UILabel* titleLabel;
@property (nonatomic,retain) NSString* title;

- (id) initWithTitle:(NSString*)title;

+ (CGFloat) headerHeight;

@end
