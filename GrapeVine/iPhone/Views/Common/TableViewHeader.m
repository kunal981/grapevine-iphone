//
//  TableViewHeader.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/17/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "TableViewHeader.h"

@implementation TableViewHeader

- (id) initWithFrame:(CGRect)frame {
  if(self = [super initWithFrame:frame]) {
    [self setup];
  }
  return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder {
  if(self = [super initWithCoder:aDecoder]) {
    [self setup];
  }
  return self;
}

- (id) initWithTitle:(NSString*)title {
  if(self = [self initWithFrame:CGRectMake(0,0,320,25)]) {
    self.title = title;
  }
  return self;
}

static UIImage* backgroundImage;

- (void) setup {
  if(!backgroundImage) backgroundImage = [UIImage imageNamed:@"tableView.header.background.png"];
  self.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
  _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,7,300,12)];
  _titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:11];
  _titleLabel.textColor = [UIColor whiteColor];
  _titleLabel.backgroundColor = [UIColor clearColor];
  [self addSubview:_titleLabel];
}

- (void) setTitle:(NSString *)title {
  _title = title;
  self.titleLabel.text = _title;
}

+ (CGFloat) headerHeight {
  return 25;
}

@end
