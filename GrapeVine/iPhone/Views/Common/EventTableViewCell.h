//
//  EventTableViewCell.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/4/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"

enum {
  EventCellHighlightNone,
  EventCellHighlightDate,
  EventCellHighlightDistance 
};
typedef NSUInteger EventCellHighlight;

@interface EventTableViewCell : UITableViewCell {
  IBOutlet UILabel* titleLabel;
  IBOutlet UILabel* dateLabel;
  IBOutlet UILabel* organizationLabel;
  IBOutlet UILabel* neighborhoodLabel;
  
  IBOutlet UIView* lowerDivider;
  
  IBOutlet UIButton* likeButton;
  IBOutlet UIButton* notlikeButton;
  
  IBOutlet UIImageView* bookmarkedImage;
  IBOutlet UIImageView* friendsImage;
}

@property (nonatomic,retain) Event* event;
@property (nonatomic,assign) EventCellHighlight highlight;

@property (strong, nonatomic) IBOutlet UIImageView *main_ImageView;

@end
