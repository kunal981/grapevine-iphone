//
//  MyEventsController.h
//  GrapeVine
//
//  Created by Zachary Gavin on 9/19/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "EventsController.h"

@interface MyEventsController : EventsController {
  IBOutlet UISegmentedControl* segmentedControl;
}

@end
