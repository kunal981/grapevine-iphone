//
//  MyEventsController.m
//  GrapeVine
//
//  Created by Zachary Gavin on 9/19/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "MyEventsController.h"
#import "UIViewController+IsLoading.h"
#import "Event.h"
#import "User.h"
#import "GANTracker.h"
#import "NSArray+Enumerable.h"

@interface MyEventsController ()

@end

@implementation MyEventsController

- (void) viewDidLoad {
  [super viewDidLoad];
	
  CGRect frame = segmentedControl.frame;
  frame.size.height = 29;
  segmentedControl.frame = frame;
}

- (void) viewWillAppear:(BOOL)animated {
    
    
    
  [super viewWillAppear:animated];
	
  if(self.events && !User.currentUser.eventsLoaded) [self loadData];
  if(User.currentUser.eventsLoaded) [self filterEvents];
  
	[self track];
}

- (void) track {
  [[GANTracker sharedTracker] trackPageview:[@"myevents/" stringByAppendingString:@[@"registered",@"bookmarked",@"all"][segmentedControl.selectedSegmentIndex]] withError:nil];
}

- (void) loadData {
  self.isLoading = YES;
  [User.currentUser loadRelatedEventsWithCompletionBlock:^(NSError* error){
    self.isLoading = NO;
    [self filterEvents];
  }];
}

- (void) filterEvents {
  User* user = User.currentUser;
  
  if(segmentedControl.selectedSegmentIndex == 2) {
    NSMutableSet* set = [NSMutableSet setWithArray:user.registeredEventsWithoutContent];
    [set addObjectsFromArray:user.bookmarkedEvents];
    self.events = set.allObjects;
  } else {
    self.events = segmentedControl.selectedSegmentIndex == 0 ? user.registeredEventsWithoutContent : user.bookmarkedEvents;
  }
  
  self.events = [self.events sortedArrayUsingComparator:^(Event* a,Event* b){ return [a.startTime compare:b.startTime]; }];
  
  [self reloadTable];
}

- (IBAction) segmentedControlChanged:(id)sender {
  [self filterEvents];
  [self track];
}

@end
