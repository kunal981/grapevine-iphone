//
//  ProfileOverviewController.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/24/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RemoteImageView.h"
#import "PickerButton.h"

@interface ProfileOverviewController : UIViewController <UITextFieldDelegate,PickerButtonDelegate> {
  IBOutlet UITextField* emailTextField;
  IBOutlet UITextField* firstNameTextField;
  IBOutlet UITextField* lastNameTextField;
  IBOutlet UITextField* ageTextField;
  IBOutlet PickerButton* genderPickerButton;

  IBOutlet UILabel* eventTitleLabel;
  IBOutlet UILabel* eventDescriptionLabel;
  
  IBOutlet UILabel* registeredLabel;
  IBOutlet UILabel* likesLabel;
  IBOutlet UILabel* bookmarksLabel;
  
  IBOutlet UIToolbar* toolbar;
  
  IBOutlet UITextView* userKeyTextView;
  
  IBOutlet RemoteImageView* imageView;
}

@end
