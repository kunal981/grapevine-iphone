//
//  ProfilePastEventsController.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/24/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "ProfilePastEventsController.h"
#import "Event.h"
#import "User.h"
#import "UIViewController+IsLoading.h"
#import "ProfilePastEventsTableViewCell.h"
#import "ProfileFeedbackController.h"
#import "GANTracker.h"
#import "NSArray+Enumerable.h"

@interface ProfilePastEventsController ()

@end

@implementation ProfilePastEventsController

- (void) viewDidLoad {
  [super viewDidLoad];
  
  [table registerNib:[UINib nibWithNibName:@"ProfilePastEventsTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"eventCell"];
}

- (void) viewWillAppear:(BOOL)animated {
  self.isLoading = YES;
  [User.currentUser loadRelatedEventsWithCompletionBlock:^(NSError* error){
    self.isLoading = NO;
    self.events = [User.currentUser.pastEvents sortedArrayUsingComparator:^(Event* a,Event*b) { return [b.startTime compare:a.startTime]; }];
    self.events = [self.events select:^(Event* event){ return (BOOL) (!event.isContent); }];
    [self reloadTable];
  }];
  [[GANTracker sharedTracker] trackPageview:@"myprofile/pastevents" withError:nil];
}

- (void) tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
  ProfileFeedbackController* controller = [[ProfileFeedbackController alloc] init];
  controller.event = self.events[indexPath.row];
  [self.navigationController pushViewController:controller animated:YES];
}

- (UIView*) isLoadingTargetView {
  return table;
}

@end
