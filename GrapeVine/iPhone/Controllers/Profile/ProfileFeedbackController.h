//
//  ProfilePastEventController.h
//  Grapevine
//
//  Created by Zachary Gavin on 10/4/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "ProfileFeedbackTableViewCell.h"

@interface ProfileFeedbackController : UIViewController <UITableViewDataSource,UITableViewDelegate,ProfileFeedbackTableViewCellDelegate,UITextViewDelegate> {
  IBOutlet UILabel* titleLabel;
  IBOutlet UITableView* table;
  IBOutlet UITextView* textView;
  IBOutlet UIToolbar* toolbar;
  
}

@property (nonatomic,strong) Event* event;

@end
