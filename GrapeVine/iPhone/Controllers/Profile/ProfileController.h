//
//  ProfileController.h
//  GrapeVine
//
//  Created by Zachary Gavin on 7/23/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileController : UIViewController {
  IBOutlet UISegmentedControl* segmentedControl;
  IBOutlet UIScrollView* scrollView;
	
  IBOutletCollection(UIViewController) NSArray* viewControllers;
}

@end
