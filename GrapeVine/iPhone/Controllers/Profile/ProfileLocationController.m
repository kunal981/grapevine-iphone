//
//  ProfileLocationController.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/24/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "ProfileLocationController.h"
#import "Community.h"
#import "User.h"
#import "NSArray+Enumerable.h"
#import "UIViewController+IsLoading.h"
#import "GANTracker.h"

@interface ProfileLocationController ()

@end

@implementation ProfileLocationController

- (void) viewDidLoad {
  [super viewDidLoad];
  
  CGRect frame = self.view.frame;
  frame.size.height = 310;
  self.view.frame = frame;
}

- (void) viewWillAppear:(BOOL)animated {
  communities = [Community.all select:^(Community* community) { return (BOOL) (community.name != nil); }];
  
  communityPickerButton.options = [communities mapKey:@"name"];
  communityPickerButton.selectedIndex = [communities indexOfObject:User.currentUser.community];
  
  NSArray* neighborhoods = User.currentUser.community.neighborhoods;
  
  neighborhoodPickerButton.options = [neighborhoods mapKey:@"name"];
  
  NSInteger idx = [neighborhoods indexOfObject:User.currentUser.neighborhood];
  neighborhoodPickerButton.selectedIndex = idx == NSNotFound ? 0 : idx;
  
  [[GANTracker sharedTracker] trackPageview:@"myprofile/location" withError:nil];
}

- (void) pickerButton:(PickerButton*)pickerButton didChangeValue:(NSString*)value {
  if(pickerButton == communityPickerButton) {
    NSArray* neighborhoods = ((Community*) communities[pickerButton.selectedIndex]).neighborhoods;
    
    neighborhoodPickerButton.options = [neighborhoods mapKey:@"name"];
    neighborhoodPickerButton.selectedIndex = 0;
  }
}

- (IBAction) updatePressed:(id)sender {
  NSArray* neighborhoods = ((Community*) communities[communityPickerButton.selectedIndex]).neighborhoods;
  
  self.isLoading = YES;
  [User.currentUser updateNeighborhood:(Neighborhood*) (neighborhoods[neighborhoodPickerButton.selectedIndex]) withCompletionBlock:^(NSError* error) {
    self.isLoading = NO;
  }];
}



@end
