//
//  ProfileController.m
//  GrapeVine
//
//  Created by Zachary Gavin on 7/23/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "ProfileController.h"
#import "ProfileOverviewController.h"
#import "NSArray+Subscript.h"
#import "NSArray+Enumerable.h"
#import "User.h"
#import "AppDelegate.h"

@interface ProfileController ()

@end

@implementation ProfileController

- (void) viewDidLoad {
  [super viewDidLoad];
  
  CGRect frame = segmentedControl.frame;
  frame.size.height = 29;
  segmentedControl.frame = frame;

  viewControllers = [@[@"Overview",@"Interests",@"PastEvents",@"Location"] map:^(NSString* identifier)
                     {
    return [[NSClassFromString([NSString stringWithFormat:@"Profile%@Controller",identifier]) alloc] init];
  }];
  
    NSLog(@"viewControllers = %@",viewControllers);
  for(UIViewController* viewController in viewControllers)
  {
        NSLog(@"viewControllers = %@",viewController);
      [self addChildViewController:viewController];
  }
    
    
}

- (void) viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
	
  if( [viewControllers all:^(UIViewController* controller){ return (BOOL) (controller.view.superview == nil); }] ) [self segmentSelected:segmentedControl];
}

- (IBAction) logoutPressed:(id)sender {
  [User  logout];
  [AppDelegate.currentDelegate transitionToLogin:UIViewAnimationOptionTransitionFlipFromRight];
}

- (IBAction) segmentSelected:(id)sender {
  for (UIViewController* viewController in viewControllers) {
    NSInteger idx = [viewControllers indexOfObject:viewController];
    if(viewController.view.superview && segmentedControl.selectedSegmentIndex != idx) [viewController.view removeFromSuperview];
    if(!viewController.view.superview && segmentedControl.selectedSegmentIndex == idx) {
			
			[scrollView addSubview:viewController.view];
			
			CGRect frame = viewController.view.frame;
			frame.origin.y = ((UIView*) scrollView.subviews[0]).frame.size.height;
			frame.size.height = fmax(frame.size.height, scrollView.frame.size.height-frame.origin.y);
			viewController.view.frame = frame;
      
			
      scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, CGRectGetMaxY(frame));
			
			scrollView.scrollEnabled = scrollView.contentSize.height > scrollView.frame.size.height;
    }
  }
}



@end
