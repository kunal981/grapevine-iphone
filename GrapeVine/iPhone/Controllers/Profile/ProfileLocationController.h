//
//  ProfileLocationController.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/24/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickerButton.h"

@interface ProfileLocationController : UIViewController <PickerButtonDelegate> {
  IBOutlet PickerButton* communityPickerButton;
  IBOutlet PickerButton* neighborhoodPickerButton;
  
  NSArray* communities;
}

@end
