//
//  ProfileOverviewController.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/24/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "ProfileOverviewController.h"
#import "User.h"
#import "Event.h"
#import "UIViewController+IsLoading.h"
#import "NSArray+Subscript.h"
#import <TapkuLibrary/TapkuLibrary.h>
#import "GANTracker.h"

@interface ProfileOverviewController ()

@end

@implementation ProfileOverviewController

- (void) viewDidLoad {
  [super viewDidLoad];
  
  ageTextField.inputAccessoryView = toolbar;
}

- (void) viewWillAppear:(BOOL)animated {
  User* u = User.currentUser;
  emailTextField.text = u.email;
  firstNameTextField.text = u.firstName;
  lastNameTextField.text = u.lastName;
  ageTextField.text = u.age.stringValue;
  
  genderPickerButton.options = @[@"N/A",@"Male",@"Female"];
  genderPickerButton.selectedIndex = u.gender ? [u.gender isEqualToString:@"male"] ? 1 : 2 : 0;
  
  userKeyTextView.text = u.key;
    
  self.isLoading = YES;
  [u loadRelatedEventsWithCompletionBlock:^(NSError* error){
    self.isLoading = NO;
    
    registeredLabel.text = @(u.registeredEventsWithoutContent.count).stringValue;
    likesLabel.text = @(u.likedEvents.count).stringValue;
    bookmarksLabel.text = @(u.bookmarkedEvents.count).stringValue;
    
    NSArray* sortedEvents = [u.registeredEventsWithoutContent sortedArrayUsingComparator:^(Event* a,Event* b){ return [a.startTime compare:b.startTime]; }];
    
    if(sortedEvents.count) {
      Event* event = sortedEvents[0];
      eventTitleLabel.text = event.title ?: @"";
      
      NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
      formatter.dateFormat = @"MMM d, yyyy h:MM a";
      formatter.locale = [NSLocale currentLocale];
      
      eventDescriptionLabel.text = [NSString stringWithFormat:@"%@\n%@, %@ %@", event.startTime ? [formatter stringFromDate:event.startTime] : @"" ,event.city ?: @"",event.state ?: @"", event.zipcode ?: @""];
    } else {
      eventTitleLabel.text = @"";
      eventDescriptionLabel.text = @"";
    }
  }];
  
  imageView.hidden = u.facebookImage == nil;
  if(u.facebookImage) imageView.url = u.facebookImage;
 
  [[GANTracker sharedTracker] trackPageview:@"myprofile/overview" withError:nil];  
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
  [textField resignFirstResponder];
  
  return NO;
}


- (IBAction) ageDonePressed:(id)sender {
  [ageTextField resignFirstResponder];
}

- (IBAction) submitPressed:(id)sender {
  NSString* error = [self getUpdateError];
  
  if(error) {
    [[TKAlertCenter defaultCenter] postAlertWithMessage:error];
  } else {
    [self.view endEditing:YES];
    
    User* u = User.currentUser;
    u.firstName = firstNameTextField.text;
    u.lastName = lastNameTextField.text;
    u.email = emailTextField.text;
    u.age = @(ageTextField.text.integerValue);
    u.gender = genderPickerButton.selectedIndex ? genderPickerButton.value.lowercaseString : nil;

    self.isLoading = YES;
    [User.currentUser updateDetailsWithCompletionBlock:^(NSError* error){
      self.isLoading = NO;
      
    }];
  }
}

- (NSString*) getUpdateError {
  NSRegularExpression* regexp = [NSRegularExpression regularExpressionWithPattern:@"\\A.*@([^.]+\\.)+[a-z0-9\\-]+\\Z" options:NSRegularExpressionCaseInsensitive error:nil];
  if([regexp rangeOfFirstMatchInString:emailTextField.text options:0 range:NSMakeRange(0, emailTextField.text.length)].location == NSNotFound)
    return @"Please enter a valid email address";
  
  if(firstNameTextField.text.length < 1)
    return @"Please enter a first name";
  
  if(lastNameTextField.text.length < 1)
    return @"Please enter a last name";
  
  if(ageTextField.text.integerValue < 1)
    return @"Please enter an age";
  
  return nil;
}

@end
