//
//  ProfilePastEventController.m
//  Grapevine
//
//  Created by Zachary Gavin on 10/4/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "ProfileFeedbackController.h"
#import <TapkuLibrary/TapkuLibrary.h>
#import "GANTracker.h"

@interface ProfileFeedbackController ()

@end

@implementation ProfileFeedbackController

- (void) viewDidLoad {
  [table registerNib:[UINib nibWithNibName:@"ProfileFeedbackTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"feedbackCell"];
  
  textView.inputAccessoryView = toolbar;
  
  
}

- (void) viewWillAppear:(BOOL)animated {
  titleLabel.text = self.event.title;
  textView.text = [self.event feedbackForKey:EventFeedbackComments];
  [table reloadData];
  
  [[GANTracker sharedTracker] trackPageview:[@"myprofile/pastevents/feedback/" stringByAppendingFormat:@"%@",self.event.key] withError:nil];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return EventFeedbackRatings.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  ProfileFeedbackTableViewCell* cell = [table dequeueReusableCellWithIdentifier:@"feedbackCell"];
  
  NSString* feedbackKey = EventFeedbackRatings[indexPath.row];
  cell.delegate = self;
  cell.feedbackKey = feedbackKey;
  
  cell.value = ((NSNumber*)[self.event feedbackForKey:feedbackKey]).integerValue;
  
  return cell;
}

- (void) feedbackCell:(ProfileFeedbackTableViewCell *)feedbackCell userDidChangeValueTo:(NSInteger)value {
  [self.event setFeedback:@(value) forKey:feedbackCell.feedbackKey];
}

- (void) textViewDidBeginEditing:(UITextView *)textView {
  UIView* footer = table.tableFooterView;
  CGRect frame = footer.frame;
  frame.size.height = 370;
  footer.frame = frame;
  
  table.tableFooterView = footer;
  [table scrollRectToVisible:footer.frame animated:YES];
}

- (void) textViewDidChange:(UITextView *)_textView {
  [self.event setFeedback:textView.text forKey:EventFeedbackComments];
}

- (IBAction) donePressed:(id)sender {
  [textView resignFirstResponder];
  
  UIView* footer = table.tableFooterView;
  CGRect frame = footer.frame;
  frame.size.height = 220;
  footer.frame = frame;
    
  table.tableFooterView = footer;
}

- (IBAction) submitPressed:(id)sender {  
  [self.event setFeedback:textView.text forKey:EventFeedbackComments];

  [self.event submitFeedback];
  
  [[GANTracker sharedTracker] trackEvent:@"Interaction" action:@"Feedback" label:((NSNumber*) self.event.key).stringValue value:0 withError:nil];

  [self.navigationController popViewControllerAnimated:YES];
  [[TKAlertCenter defaultCenter] postAlertWithMessage:@"Thanks for your feedback!"];
}

@end
