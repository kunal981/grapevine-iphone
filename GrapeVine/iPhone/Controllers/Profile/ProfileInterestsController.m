//
//  ProfileInterestsController.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/24/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "ProfileInterestsController.h"
#import "ProfileInterestsTableViewCell.h"
#import "NSArray+Subscript.h"
#import "GVObject.h"
#import "Interest.h"
#import "SocialCommunity.h"
#import "User.h"
#import "TableViewHeader.h"
#import "AppDelegate.h"
#import "ProfileController.h"
#import "GANTracker.h"

@interface ProfileInterestsController ()

@end

@implementation ProfileInterestsController

- (void) viewDidLoad {
  [super viewDidLoad];
  
  [table registerNib:[UINib nibWithNibName:@"ProfileInterestsTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"InterestCell"];
}

- (void) viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  
  NSComparisonResult(^block)(id,id) = ^(id<NamedGVObject> a,id<NamedGVObject> b) { return [a.name compare:b.name]; };
  interests =  [Interest.all sortedArrayUsingComparator:block];
  socialCommunities = [SocialCommunity.all sortedArrayUsingComparator:block];
  
  [table reloadData];
  
  if([self.parentViewController isKindOfClass:[UINavigationController class]]) {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Continue" style:UIBarButtonItemStyleDone target:self action:@selector(continuePressed:withEvent:)];
    table.scrollEnabled = YES;
    [self.navigationItem setHidesBackButton:YES animated:NO];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]];
  } else {
    table.scrollEnabled = YES;
    CGRect frame = self.view.frame;
    //frame.size.height = table.tableHeaderView.frame.size.height+(TableViewHeader.headerHeight*2)+(interests.count+socialCommunities.count)*table.rowHeight+table.tableFooterView.frame.size.height;
    self.view.frame = frame;
    
    //table.frame = CGRectMake(0, 0, 320, frame.size.height);
  }
  
  [[GANTracker sharedTracker] trackPageview:@"myprofile/interests" withError:nil];
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
  return 2;
}

- (UIView*) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
  return [[TableViewHeader alloc] initWithTitle:@[@"COMMUNITIES",@"INTERESTS"][section]];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return !section ? socialCommunities.count : interests.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  ProfileInterestsTableViewCell* cell = (ProfileInterestsTableViewCell*) [table dequeueReusableCellWithIdentifier:@"InterestCell"];
  id<NamedGVObject> obj = (!indexPath.section ? socialCommunities : interests)[indexPath.row];

  cell.label.text = obj.name;
  if(!indexPath.section ? [User.currentUser hasSocialCommunity:obj] : [User.currentUser hasInterest:obj]) [table selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
  
  return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  if(!indexPath.section) {
    [User.currentUser addSocialCommunity:socialCommunities[indexPath.row]];
    [User.currentUser updateSocialCommunities];
  } else {
    [User.currentUser addInterest:interests[indexPath.row]];
    [User.currentUser updateInterests];
  }
}

- (void) tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
  if(!indexPath.section) {
    [User.currentUser removeSocialCommunity:socialCommunities[indexPath.row]];
    [User.currentUser updateSocialCommunities];
  } else {
    [User.currentUser removeInterest:interests[indexPath.row]];
    [User.currentUser updateInterests];
  }
}

- (void) continuePressed:(UIControl*)controler withEvent:(UIEvent*)event {
  [AppDelegate.currentDelegate transitionToHome:UIViewAnimationOptionTransitionFlipFromRight];
}

@end
