//
//  EventWebviewController.m
//  GrapeVine
//
//  Created by Zachary Gavin on 7/22/13.
//  Copyright (c) 2013 com.grape-vine. All rights reserved.
//

#import "EventWebviewController.h"
#import <TapkuLibrary/TapkuLibrary.h>

@implementation EventWebviewController

- (void) loadView {
	[[UINib nibWithNibName:@"Webview" bundle:NSBundle.mainBundle] instantiateWithOwner:self options:nil];	
}

- (void) setEvent:(Event *)event {
	_event = event;
	
	self.url = event.registrationUrl;
}

- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	if( ![request.URL.scheme isEqualToString:@"grapevine"] ) return [super webView:webView shouldStartLoadWithRequest:request navigationType:navigationType];
	
	if ( [request.URL.host isEqualToString:@"opportunity"]) {
		NSArray* pathComponents = [request.URL.path componentsSeparatedByString:@"/"];
		NSString* action = pathComponents[1];
		NSInteger key = [pathComponents.lastObject integerValue];
		
		if ( [self.event.key integerValue] == key ) {
			if ( [action isEqualToString:@"addtocalendar"]) {
				[self.event addToCalendarWithCompletionBlock:^(BOOL success){
					[[TKAlertCenter defaultCenter] postAlertWithMessage:@"Event added to your calendar!"];
				}];
				
			} else if ( [action isEqualToString:@"register"]) {
				[self.event setUserRegistered:YES sync:NO];
			}
		}
	}
	
	if ( [request.URL.host isEqualToString:@"dismiss"]) {
		[self.navigationController popViewControllerAnimated:YES];
	}
	
	return NO;
}

@end
