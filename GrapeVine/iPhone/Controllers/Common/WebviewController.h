//
//  WebviewController.h
//  Grapevine
//
//  Created by Zachary Gavin on 10/3/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

@interface WebviewController : UIViewController <UIWebViewDelegate> {
  IBOutlet UIWebView* webview;
}

@property (nonatomic,strong) NSString* url;

@end
