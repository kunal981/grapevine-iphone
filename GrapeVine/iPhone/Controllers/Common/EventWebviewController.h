//
//  EventWebviewController.h
//  GrapeVine
//
//  Created by Zachary Gavin on 7/22/13.
//  Copyright (c) 2013 com.grape-vine. All rights reserved.
//

#import "WebviewController.h"
#import "Event.h"

@interface EventWebviewController : WebviewController

@property (strong, nonatomic) Event* event;

@end
