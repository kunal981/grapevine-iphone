//
//  EventsController.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/12/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsController : UIViewController <UITableViewDataSource,UITableViewDelegate> {
  IBOutlet UITableView* table;
  
  UIView* noResultsView;
  UIView* tableFooter;
  
  id observer;
  
  BOOL requireRefresh;
}

- (void) loadData;
- (void) reloadTable;

@property (nonatomic,retain) NSArray* events;


@end
