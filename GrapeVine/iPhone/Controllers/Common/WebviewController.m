//
//  WebviewController.m
//  Grapevine
//
//  Created by Zachary Gavin on 10/3/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "WebviewController.h"
#import "UIViewController+IsLoading.h"

@implementation WebviewController

- (void) viewDidLoad {
  [super viewDidLoad];
  if(self.url) [self load];
}

- (void) setUrl:(NSString *)url {
  _url = url;
  if(self.isViewLoaded) [self load];
}

- (void) load {
  [webview loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
}

- (void) webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
  self.isLoading = NO;
  [self.navigationController popViewControllerAnimated:NO];
  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.url]];
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
  self.isLoading = YES;
}

- (void) webViewDidFinishLoad:(UIWebView *)webView {
  self.isLoading = webview.loading;
}

- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	return YES;
}

@end
