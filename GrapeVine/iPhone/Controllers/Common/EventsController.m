//
//  EventsController.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/12/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "EventsController.h"
#import "EventController.h"
#import "EventTableViewCell.h"
#import "NSArray+Subscript.h"
#import "UIViewController+IsLoading.h"
#import "User.h"

@interface EventsController ()

@end

@implementation EventsController

static NSMutableDictionary* noResultsNibs;

- (void) viewDidLoad {
  [super viewDidLoad];
  if(!noResultsNibs) noResultsNibs = [@{} mutableCopy];
  NSString* className = NSStringFromClass([self class]);
  if(!noResultsNibs[className]) {
    NSString* nibName = [NSString stringWithFormat:@"%@NoResultsView",[className stringByReplacingOccurrencesOfString:@"Controller" withString:@""]];
    noResultsNibs[className] = [UINib nibWithNibName:[[NSBundle mainBundle] pathForResource:nibName ofType:@"nib"] ? nibName : @"NoResultsView" bundle:[NSBundle mainBundle]];
  }
  
  requireRefresh = NO;
  observer = [[NSNotificationCenter defaultCenter] addObserverForName:UserNotificationNeighborhoodUpdate object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification* notification) {
    requireRefresh = YES;
  }];
  
  tableFooter = table.tableFooterView;
  [table registerNib:[UINib nibWithNibName:@"EventTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"eventCell"];
}

- (void) viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
    
    self.navigationItem.title=@"EXPLORE EVENTS";
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor darkGrayColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Bold" size:19],NSFontAttributeName,nil]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_header.png"] forBarMetrics:UIBarMetricsDefault];
    

  if((self.events.count > 0 || self.isLoading) && !requireRefresh) {
    [self reloadTable];
  } else {
    if(!self.events || requireRefresh) [self loadData];
    requireRefresh = NO;
    [self repositionFooter];
  }
}

- (void) loadData {
  
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	CGFloat height = 76.0;
	return height;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return [self arrayForSection:section].count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  EventTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"eventCell"];
  
  cell.event = [self arrayForSection:indexPath.section][indexPath.row];
  
  return cell;
}

- (void) reloadTable {
  [table reloadData];
  if(self.events.count == 0) {
    if(!noResultsView) noResultsView = [noResultsNibs[NSStringFromClass([self class])] instantiateWithOwner:self options:nil][0];
    CGRect frame = noResultsView.frame;
    frame.size.height = table.frame.size.height - table.tableHeaderView.frame.size.height - tableFooter.frame.size.height;
    frame.origin.y = table.frame.origin.y + table.tableHeaderView.frame.size.height;
    noResultsView.frame = frame;
    
		table.separatorColor = [UIColor clearColor];
		
    if(!noResultsView.superview) [self.view addSubview:noResultsView];
  } else if(noResultsView && noResultsView.superview) {
		table.separatorColor = nil;
    [noResultsView removeFromSuperview];
  }
  
  [self repositionFooter];
}

- (void) repositionFooter {
  CGFloat height = table.tableHeaderView.frame.size.height+tableFooter.frame.size.height;
  for(NSInteger i=0; i< table.numberOfSections;i++) {
    if([self respondsToSelector:@selector(tableView:heightForHeaderInSection:)]) height += [self tableView:table heightForHeaderInSection:i];
    height += [self arrayForSection:i].count*table.rowHeight;
  }

  if(height < table.frame.size.height && tableFooter.superview == table) {
    table.tableFooterView = nil;
    [self.view addSubview:tableFooter];
    CGRect frame = tableFooter.frame;
    frame.size.height = 60;
    frame.origin.y = self.view.frame.size.height-frame.size.height;
    tableFooter.frame = frame;
  } else if(height >= table.frame.size.height && tableFooter.superview == self.view) {
    [tableFooter removeFromSuperview];
    table.tableFooterView = tableFooter;
  }
}

- (void) tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  EventController* eventController = [[EventController alloc] init];
  eventController.event = [self arrayForSection:indexPath.section][indexPath.row];
  [self.navigationController pushViewController:eventController animated:YES];
  [table deselectRowAtIndexPath:[table indexPathForSelectedRow] animated:NO];
}

- (NSArray*) arrayForSection:(NSInteger) section {
  return self.events;
}

- (void) dealloc {
  [[NSNotificationCenter defaultCenter] removeObserver:observer];
}
#pragma mark - StatusBarHidden Method
- (BOOL)prefersStatusBarHidden {
    return YES;
}


@end
