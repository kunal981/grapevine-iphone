//
//  EventController.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/4/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "EventController.h"
#import "User.h"
#import "NSString+Utility.h"
#import "NSArray+Enumerable.h"
#import "LocationManager.h"
#import "EventWebviewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "AppDelegate.h"
#import "Sharer.h"
#import "GANTracker.h"
#import "SearchOrganizationController.h"
#import "UIAlertView+Block.h"
#import "UIImage+Color.h"

#import <TapkuLibrary/TapkuLibrary.h>
#import <QuartzCore/QuartzCore.h>

@interface EventController ()

@end

@implementation EventController

- (void) viewDidLoad {
  [super viewDidLoad];
	
	mapContainer.layer.cornerRadius = 8;
	
	[mapToolbar setBackgroundImage:[UIImage imageWithColor:[UIColor clearColor]] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
}

- (void) viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  
	if ( self.event ) [self refresh];
  
	if ( didSendToRegistration ) {
    [self showRegistrationAlert];
    didSendToRegistration = NO;
  }
}

- (void) setEvent:(Event *)event {
  _event = event;
  if ( self.isViewLoaded ) [self refresh];
}

- (void) refresh {
  [User.currentUser trackAction:@"EventView" withData:self.event.key];
  
  [[GANTracker sharedTracker] trackPageview:[@"events/event/" stringByAppendingFormat:@"%@",self.event.key] withError:nil];
  
  imageView.contentMode = UIViewContentModeScaleAspectFill;
  
  titleLabel.text = self.event.title;
  
  
  NSString* orgText = (self.event.sponsoring_organization ?: self.event.organization).name ?: @" ";
	
  if([organizationLabel respondsToSelector:@selector(setAttributedText:)]) {
    organizationLabel.attributedText =[[NSAttributedString alloc] initWithString:orgText attributes:@{NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle)}];
  } else {
    organizationLabel.text = orgText;
  }
  
  NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
  formatter.dateFormat = @"MMM d, yyyy | h:mm a";
  formatter.locale = [NSLocale currentLocale];
  dateLabel.text = [formatter stringFromDate:self.event.startTime];
  
  NSString* addressText = [NSString stringWithFormat:@"%@\n%@\n%@, %@ %@",self.event.location ?: @"",self.event.addressLine1,self.event.city,self.event.state, self.event.zipcode];
  
  BOOL hasMap = !self.event.isContent && self.event.longitude && self.event.latitude;

  self.navigationItem.rightBarButtonItem = hasMap ? [[UIBarButtonItem alloc] initWithTitle:@"Map" style:UIBarButtonItemStylePlain target:self action:@selector(mapButtonPressed:withEvent:)] : nil;
	
	if ([addressLabel respondsToSelector:@selector(setAttributedText:)] && hasMap) {
    addressLabel.attributedText =[[NSAttributedString alloc] initWithString:addressText attributes:@{NSForegroundColorAttributeName:[UIColor blueColor], NSUnderlineStyleAttributeName:@(NSUnderlineStyleSingle)}];
  } else {
		addressLabel.text = addressText;
	}
  
  descriptionTextView.text = self.event.description;
  
  distanceContainer.hidden = self.event.distance == 0;
  
  distanceLabel.text = [NSString stringWithFormat:self.event.distanceInMiles > 100 ? @"%0.f miles\naway" : @"%0.1f miles\naway",self.event.distanceInMiles];
  
  likeButton.selected = self.event.userLike;
  notlikeButton.selected = self.event.userNotLike;
  bookmarkButton.selected = self.event.userBookmarked;
  registerButton.selected = !self.event.isContent && !self.event.isOnSite && self.event.userRegistered;
	registerButton.enabled = !(self.event.isOnSite && self.event.userRegistered);
  
  [registerButton setBackgroundImage:[registerButton backgroundImageForState:UIControlStateSelected] forState:UIControlStateDisabled];
	
  if(self.event.isContent) {
    distanceContainer.hidden = YES;
    dateLabel.hidden = YES;
    
    calendarButton.hidden = YES;
    addressLabel.hidden = YES;
    
    [registerButton setTitle:@"See More" forState:UIControlStateNormal];
  }
	
	imageView.hidden = !self.event.imageUrl || [self.event.imageUrl endsWith:@"NoImage.jpg"];
	
	if ( !imageView.hidden ) imageView.url = self.event.imageUrl;
  
  [self refreshFriends];
}

- (void) refreshFriends {
  NSString* names;
  NSString* rest;
  NSString* link;
  NSArray* friendNames = [self.event.facebookFriends mapKey:@"name"];
  friendsContainer.hidden = (friendNames.count == 0);
  if(friendNames.count > 2 && !friendsButton.selected) {
    names = [NSString stringWithFormat:@"%@, %@, and %d other friend%@",friendNames[0],friendNames[1],friendNames.count-2,(friendNames.count-2 > 1 ? @"s":@"")];
  } else if(friendNames.count > 2) {
    names = [NSString stringWithFormat:@"%@, and %@",[[friendNames subarrayWithRange:NSMakeRange(0, friendNames.count-1)] componentsJoinedByString:@", "],[friendNames lastObject]];
  } else if (friendNames.count == 2) {
    names = [friendNames componentsJoinedByString:@" and "];
  } else if (friendNames.count) {
    names = friendNames[0];
  } else {
    return;
  }

  friendsImage.url = self.event.facebookFriends[0][@"image_url"];
  
  rest = self.event.isContent ? @" read this article" : (friendNames.count > 1 ? @" are going to this event." : @" is going to this event.");
  
  if([friendsTextView respondsToSelector:@selector(setAttributedText:)]) {
    link = friendsButton.selected || friendNames.count <= 2 ? @"" : @" View Full List";
    UIColor* c = [UIColor colorWithRed:56.0/255 green:55.0/255 blue:60.0/255 alpha:1];
    UIFont* f = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12];
  
    NSMutableAttributedString* attributedString = [[NSMutableAttributedString alloc] initWithString:names attributes:@{NSFontAttributeName:f,NSForegroundColorAttributeName:c}];
    
    f = [UIFont fontWithName:@"HelveticaNeue" size:12];
    NSAttributedString* attributedRest = [[NSAttributedString alloc] initWithString:rest attributes:@{NSFontAttributeName:f,NSForegroundColorAttributeName:c}];
    
    c = [UIColor colorWithRed:133.0/255 green:2.0/255 blue:64.0/255 alpha:1];
    NSAttributedString* attributedLink = [[NSAttributedString alloc] initWithString:link attributes:@{NSFontAttributeName:f,NSForegroundColorAttributeName:c}];
    
    [attributedString appendAttributedString:attributedRest];
    [attributedString appendAttributedString:attributedLink];
    
    friendsTextView.attributedText = attributedString;
  } else {
    link = friendsButton.selected  || friendNames.count <= 2 ? @"" : @" Tap to view full list.";
    friendsTextView.text = [NSString stringWithFormat:@"%@%@%@",names,rest,link];
  }
	
	[self.view setNeedsLayout];
}

- (void) viewWillLayoutSubviews {
	[super viewWillLayoutSubviews];

  CGRect frame;
	
	frame = titleLabel.frame;
	frame.origin.x = imageView.hidden ? 9: 104;
	frame.size.width = self.view.bounds.size.width - frame.origin.x - 11;
  frame.size.height = [titleLabel sizeThatFits:frame.size].height;
  titleLabel.frame = frame;
	
	frame = distanceContainer.frame;
  frame.origin.y = imageView.hidden ? CGRectGetMaxY(titleLabel.frame)+13 : 104;
  distanceContainer.frame = frame;
  
	CGFloat maxLeftColumn = distanceContainer.hidden ? imageView.hidden ? 0 : CGRectGetMaxY(imageView.frame) : CGRectGetMaxY(distanceContainer.frame);

	CGFloat maxRightColumn = CGRectGetMaxY(titleLabel.frame)+8;
	
	for ( UILabel* label in @[organizationLabel,dateLabel,addressLabel] ) {
    if ( label.hidden ) continue;

		frame = label.frame;
		frame.origin.y = maxRightColumn;
		frame.origin.x = maxLeftColumn < frame.origin.y ? 9 : 104;
		frame.size.width = self.view.bounds.size.width - frame.origin.x - 11;
		frame.size.height = [label.text sizeWithFont:label.font constrainedToSize:CGSizeMake(frame.size.width, 100000) lineBreakMode:NSLineBreakByWordWrapping].height;
		label.frame = frame;
		
		maxRightColumn = CGRectGetMaxY(frame);
	}
	
  frame = descriptionTextView.frame;
  frame.size = descriptionTextView.contentSize;
  frame.origin.y = fmax(maxLeftColumn ,maxRightColumn)+8;
  descriptionTextView.frame = frame;
  
  frame = friendsTextView.frame;
  frame.size.height = friendsTextView.contentSize.height;
  friendsTextView.frame = frame;
  friendsButton.frame = frame;
  
  frame = friendsContainer.frame;
  frame.origin.y = CGRectGetMaxY(descriptionTextView.frame) - 16;
  frame.size.height = friendsTextView.frame.origin.y + (self.event.facebookFriends.count ? MAX(friendsTextView.frame.size.height,friendsImage.frame.size.height+12) : 0);
  friendsContainer.frame = frame;
  
	frame = buttonContainer.frame;
	frame.origin.y = CGRectGetMaxY((calendarButton.hidden ? registerButton : calendarButton).frame) + 12;
	buttonContainer.frame = frame;
	
  frame = lowerContainer.frame;
	frame.size.height = CGRectGetMaxY(buttonContainer.frame)+60;
  frame.origin.y = fmax(CGRectGetMaxY(friendsContainer.frame),self.view.bounds.size.height - frame.size.height);
  lowerContainer.frame = frame;
  
  scrollView.contentSize = CGSizeMake(320, frame.origin.y+frame.size.height);
}

- (IBAction) addressPressed:(id)sender {
  if(self.event.longitude && self.event.latitude) [self toggleMap];
}

- (void) mapButtonPressed:(UIControl*)control withEvent:(UIEvent*)event {
  [self toggleMap];
}

- (void) toggleMap {
  if ( !mapView ) {
		CGFloat padding = mapContainer.layer.cornerRadius;
		
    mapView = [[MKMapView alloc] initWithFrame:CGRectMake(padding,padding,mapContainer.frame.size.width-padding*2,mapContainer.frame.size.height-padding*2)];
    MKPointAnnotation* point = [[MKPointAnnotation alloc] init];
    point.coordinate = CLLocationCoordinate2DMake(self.event.latitude, self.event.longitude);
    
    [mapView addAnnotation:point];
		
		[mapContainer addSubviewToBack:mapView];
  }
	
	mapContainer.hidden = !mapContainer.hidden;
  
  if ( mapContainer.hidden ) {
    self.navigationItem.rightBarButtonItem.title = @"Map";
  } else {
    self.navigationItem.rightBarButtonItem.title = @"Hide Map";
    MKCoordinateRegion region;
    region.center.latitude = self.event.latitude;
    region.center.longitude = self.event.longitude;
    region.span.latitudeDelta = 0.025;
    region.span.longitudeDelta = 0.025;

    [mapView setRegion:[mapView regionThatFits:region] animated:NO];
  }
}

- (IBAction) directionsPressed:(id)sender {
  CLLocationCoordinate2D location = LocationManager.sharedLocationManager.currentLocation;
  NSString* urlString = [NSString stringWithFormat:@"http://maps.apple.com/maps?daddr=%f,%f&saddr=%f,%f",self.event.latitude,self.event.longitude,location.latitude,location.longitude];
  
  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
}

- (IBAction) friendsPressed:(id)sender {
  friendsButton.selected = !friendsButton.selected;
  [self refreshFriends];
  [self.view setNeedsLayout];
}

- (IBAction) registerPressed:(id)sender {
	if ( self.event.userRegistered ) {
		[self showRegistrationAlert];
	} else {
		if ( !self.event.isContent ) {
			[[GANTracker sharedTracker] trackEvent:@"Interaction" action:@"Register" label:((NSNumber*) self.event.key).stringValue value:0 withError:nil];
			
			if ( !self.event.isOnSite ) {
				didSendToRegistration = YES;
				
				[self.event setUserRegistered:YES sync:YES];
				registerButton.selected = NO;
			}
		}
		
		EventWebviewController* webviewController =  [[EventWebviewController alloc] init];
		webviewController.event = self.event;
		[self.navigationController pushViewController:webviewController animated:YES];
	}
}

- (void) showRegistrationAlert {
  
    [UIAlertView alertViewWithTitle:@"Register?" message:@"Did you complete registration for this event?" cancelButtonTitle:@"No" otherButtonTitles:@[@"Yes"] onDismiss:^(UIAlertView *alertView, int buttonIndex) {
      if(buttonIndex == alertView.cancelButtonIndex)
        
      {
        [self.event setUserRegistered:NO sync:YES];
        registerButton.selected = NO;
      }

    }];
    
    }

- (IBAction) addToCalendarPressed:(id)sender {
	__weak EventController* weakSelf = self;
	
	[self.event addToCalendarWithCompletionBlock:^(BOOL success){
		[User.currentUser trackAction:@"AddToCalendar" withData:weakSelf.event.key];
		[[TKAlertCenter defaultCenter] postAlertWithMessage:@"Event added to your calendar!"];
	}];
  
  [[GANTracker sharedTracker] trackEvent:@"Interaction" action:@"Add To Calendar" label:((NSNumber*) self.event.key).stringValue value:0 withError:nil];
}

- (IBAction) likePressed:(id)sender {
  likeButton.selected = !likeButton.selected;
  notlikeButton.selected = NO;
  if(likeButton.selected) [[GANTracker sharedTracker] trackEvent:@"Interaction" action:@"Like" label:((NSNumber*) self.event.key).stringValue value:0 withError:nil];
  [self.event setUserLike:likeButton.selected sync:YES];
}

- (IBAction) notlikePressed:(id)sender {
  notlikeButton.selected = !notlikeButton.selected;
  likeButton.selected = NO;
  if(notlikeButton.selected) [[GANTracker sharedTracker] trackEvent:@"Interaction" action:@"Dislike" label:((NSNumber*) self.event.key).stringValue value:0 withError:nil];
  [self.event setUserNotLike:notlikeButton.selected sync:YES];
}

- (IBAction) bookmarkPressed:(id)sender {
  bookmarkButton.selected = !bookmarkButton.selected;
  if(bookmarkButton.selected) [[GANTracker sharedTracker] trackEvent:@"Interaction" action:@"Bookmark" label:((NSNumber*) self.event.key).stringValue value:0 withError:nil];
  [self.event setUserBookmarked:bookmarkButton.selected sync:YES];
}

- (IBAction) sharePressed:(id)sender {
  if(notlikeButton.selected) [[GANTracker sharedTracker] trackEvent:@"Interaction" action:@"Share" label:((NSNumber*) self.event.key).stringValue value:0 withError:nil];
  [[Sharer sharerWithObject:self.event] shareFromController:self];
}

- (IBAction) organizationPressed:(id)sender {
  SearchOrganizationController* controller = [[SearchOrganizationController alloc] init];
  controller.organization = (self.event.sponsoring_organization ?: self.event.organization);
  [self.navigationController pushViewController:controller animated:YES];
}

@end
