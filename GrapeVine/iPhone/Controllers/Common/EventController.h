//
//  EventController.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/4/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Event.h"
#import "RemoteImageView.h"

@interface EventController : UIViewController <UIAlertViewDelegate> {
  IBOutlet UIScrollView* scrollView;
  
	IBOutlet RemoteImageView* imageView;
	
	IBOutlet UIView* distanceContainer;
  IBOutlet UILabel* distanceLabel;
	
  IBOutlet UILabel* titleLabel;
	IBOutlet UILabel* organizationLabel;
  IBOutlet UILabel* dateLabel;
	IBOutlet UILabel* addressLabel;

  IBOutlet UITextView* descriptionTextView;

	IBOutlet UITextView* friendsTextView;
  IBOutlet UIButton* friendsButton;
  IBOutlet RemoteImageView* friendsImage;
  IBOutlet UIView* friendsContainer;
	
  IBOutlet UIView* lowerContainer;
  
	IBOutlet UIButton* registerButton;
  IBOutlet UIButton* calendarButton;
	
	IBOutlet UIView* buttonContainer;
  IBOutlet UIButton* likeButton;
  IBOutlet UIButton* notlikeButton;
  IBOutlet UIButton* bookmarkButton;
  
  MKMapView* mapView;
	IBOutlet UIToolbar* mapToolbar;
  IBOutlet UIView* mapContainer;
  
  BOOL didSendToRegistration;
}

@property (nonatomic,retain) Event* event;

@end
