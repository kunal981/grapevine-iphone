//
//  HomeController.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/17/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventsController.h"

@interface HomeController : EventsController {
  NSArray* sponsoredEvents;
  NSArray* interestEvents;
  NSArray* friendEvents;
  NSArray* allArrays;
}

@property (strong,nonatomic) NSDate* nextUpdate;

@end
