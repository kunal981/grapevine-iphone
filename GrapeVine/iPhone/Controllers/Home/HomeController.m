//
//  HomeController.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/17/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "HomeController.h"
#import "TableViewHeader.h"
#import "NSDictionary+Subscript.h"
#import "Event.h"
#import "User.h"
#import "NSArray+Enumerable.h"
#import "NSArray+Subscript.h"
#import "UIViewController+IsLoading.h"
#import "GANTracker.h"

@implementation HomeController

- (void) viewDidLoad {
	[super viewDidLoad];
	
	[NSNotificationCenter.defaultCenter addObserver:self selector:@selector(profileUpdated:) name:UserNotificationProfileUpdate object:nil];
}

- (void) viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  if(self.events && !self.isLoading && [self.nextUpdate earlierDate:[NSDate date]] == self.nextUpdate) {
    [self loadData];
  }
  
  [[GANTracker sharedTracker] trackPageview:@"events/recommended" withError:nil];
}

- (void) loadData {
  self.isLoading = YES;
  [Event recommendedEventsForUser:[User currentUser] withCompletionBlock:^(NSDictionary* events,NSError* error){
      NSLog(@"%@",events);
      self.isLoading = NO;
    self.events = [[NSOrderedSet orderedSetWithArray:[events.allValues flatten]] array];
    
    if([self.nextUpdate earlierDate:[NSDate date]] == self.nextUpdate) self.nextUpdate = [NSDate dateWithTimeIntervalSinceNow:900];
    
    sponsoredEvents = events[EventTypeSponsored] ?: @[];
    
    NSMutableArray* friendTmp =  [(events[EventTypeFriends] ?: @[]) mutableCopy];
		[friendTmp removeObjectsInArray:sponsoredEvents];
		[friendTmp removeObjectsInArray:User.currentUser.registeredEventsWithoutContent];
    friendEvents = friendTmp;
    
    
    NSMutableArray* interestTmp = [(events[EventTypeRecommended] ?: @[]) mutableCopy];
		[interestTmp removeObjectsInArray:sponsoredEvents];
		[interestTmp removeObjectsInArray:friendEvents];
    interestEvents = interestTmp;
    
    allArrays =  [@[sponsoredEvents,friendEvents,interestEvents] select:^(NSArray* tmp){ return (BOOL) (tmp.count > 0); }];

    [self reloadTable];
  }];
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
  return allArrays.count;
}

- (NSArray*) arrayForSection:(NSInteger)section {
  return self.events ? allArrays[section] : nil;
}

- (UIView*) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
  NSArray* array = [self arrayForSection:section];
  NSInteger idx = [@[sponsoredEvents,friendEvents,interestEvents] indexOfObjectIdenticalTo:array];
  
  
  return [[TableViewHeader alloc] initWithTitle:@[@"FEATURED",@"FRIENDS' OPPORTUNITIES",@"MY RECOMMENDATIONS"][idx]];
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
  return [TableViewHeader headerHeight];
}

- (void) profileUpdated:(NSNotification*)notification {
	requireRefresh = YES;
}

- (void) dealloc {
	[NSNotificationCenter.defaultCenter removeObserver:self];
}

@end
