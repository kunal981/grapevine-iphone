//
//  LoginForgotPasswordController.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginForgotPasswordController : UIViewController <UITextFieldDelegate> {
    IBOutlet UILabel* infoLabel;
    IBOutlet UITextField* emailTextField;
    IBOutlet UIButton* submitButton;
}
@property (strong, nonatomic) IBOutlet UIView *forget_Background_View;

@end
