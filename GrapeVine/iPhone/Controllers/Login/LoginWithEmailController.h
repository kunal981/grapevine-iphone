//
//  LoginWithEmailController.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginWithEmailController : UIViewController <UITextFieldDelegate> {

  IBOutlet UIScrollView* scrollView;
  
  IBOutlet UILabel* errorLabel;
  
  IBOutlet UIView* loginContainer;
  IBOutlet UIView* registrationContainer;
  
  IBOutlet UITextField* loginEmailTextField;
  IBOutlet UITextField* loginPasswordTextField;
  
  IBOutlet UITextField* registrationEmailTextField;
  IBOutlet UITextField* registrationPasswordTextField;
  IBOutlet UITextField* registrationPasswordConfirmationTextField;
  IBOutlet UITextField* registrationAgeTextField;
  IBOutlet UITextField* registrationZipcodeTextField;
  
  IBOutlet UIToolbar* registrationToolbar;
  
  IBOutlet UISegmentedControl* segmentedControl;

}

@property (strong, nonatomic) IBOutlet UIView *mainView;

@property (strong, nonatomic) IBOutlet UIButton *btnSubmit;
- (IBAction)btnRegisterPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnRegister;
- (IBAction)btn_Back_Login:(id)sender;


@end
