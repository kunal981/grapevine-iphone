//
//  LoginResetPasswordController.m
//  GrapeVine
//
//  Created by Zachary Gavin on 9/18/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "LoginRecoverPasswordController.h"
#import <TapkuLibrary/TapkuLibrary.h>
#import "AppDelegate.h"
#import "UIViewController+IsLoading.h"

@interface LoginRecoverPasswordController ()

@end

@implementation LoginRecoverPasswordController

- (IBAction) submitPressed:(id)sender {
  if ([passwordTextField.text isEqualToString:passwordConfirmationTextField.text]) {
    self.isLoading = YES;
    [self.user recoverPassword:passwordTextField.text withCompletionBlock:^(NSError* error){
      self.isLoading = NO;
      if(error) {
        [[TKAlertCenter defaultCenter] postAlertWithMessage:@"Password recovery failed"];
      } else {
        [AppDelegate.currentDelegate transitionToHome:UIViewAnimationOptionTransitionFlipFromRight];
      }
    }];
  } else {
    [[TKAlertCenter defaultCenter] postAlertWithMessage:@"Passwords do not match"];
  }
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
  [textField resignFirstResponder];
  return NO;
}

@end
