//
//  LoginSplashController.h
//  GrapeVine
//
//  Created by Zachary Gavin on 9/9/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TapkuLibrary/TapkuLibrary.h>

@interface LoginSplashController : UIViewController {
  TKProgressCircleView* progressCircleView;
  IBOutlet UIImageView* logoImage;
  IBOutlet UIImageView* backgroundImage;
}



@end
