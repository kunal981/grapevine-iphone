//
//  LoginController.m
//  GrapeVine
//
//  Created by Zachary Gavin on 7/23/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <FacebookSDK/FacebookSDK.h>

#import <TapkuLibrary/TapkuLibrary.h>
#import "LoginWithFacebookController.h"
#import "User.h"
#import "UIViewController+IsLoading.h"
#import "LoginWithEmailController.h"
#import "AppDelegate.h"
#import "WebviewController.h"
#import "ProfileInterestsController.h"
#import <Accounts/Accounts.h>
#import "GANTracker.h"

@interface LoginWithFacebookController ()

@end

@implementation LoginWithFacebookController

- (void) viewDidLoad {
  [super viewDidLoad];

  loginButton.titleLabel.textAlignment = UITextAlignmentCenter;
}

- (void) viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
}

- (IBAction) facebookPressed:(id)sender {
  loginRetried = NO;
  [[GANTracker sharedTracker] trackPageview:@"login/facebook" withError:nil];

  [FBSession.activeSession closeAndClearTokenInformation];
  //[self facebookLogin];
}
/*
- (void) facebookLogin {
  [FBSession openActiveSessionWithReadPermissions:@[@"email",@"user_birthday",@"user_location"] allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
    if(session.state == FBSessionStateOpen) {
      self.isLoading = YES;
      [User loginRegisterWithFacebookAccessToken:session.accessToken andCompletionBlock:^(User* user, NSError* error) {
				if(error) NSLog(@"%@",error);
				
        if ( error && (([error.domain isEqualToString:NSURLErrorDomain] && error.code == NSURLErrorTimedOut) || ([error.domain isEqualToString:@"HTTP"] && error.code == 0)) ) {
          self.isLoading = NO;
          [[TKAlertCenter defaultCenter] postAlertWithMessage:@"A timeout occurred during login, please wait a few moments and try again"];
        } else if (error && [self canRetryLogin]) {
          [self renewFBCredentialsAndRetryOnce];
        } else if(error) {
					self.isLoading = NO;
          [[TKAlertCenter defaultCenter] postAlertWithMessage:@"A server error occured, please wait a few moments and try again"];
        } else if(user.isNew) {
          self.isLoading = NO;
					
					AppDelegate.currentDelegate.homeController.nextUpdate = [NSDate dateWithTimeIntervalSinceNow:90];
					
          ProfileInterestsController* controller = [[ProfileInterestsController alloc] init];
          [self.navigationController pushViewController:controller animated:YES];
        } else {
          self.isLoading = NO;
					
					AppDelegate.currentDelegate.homeController.nextUpdate = [NSDate dateWithTimeIntervalSinceNow:90];
          
					[AppDelegate.currentDelegate transitionToHome:UIViewAnimationOptionTransitionFlipFromRight];
        }
      }];
    } else if (error) {
      NSArray* errors = @[FBErrorLoginFailedReasonInlineCancelledValue,FBErrorLoginFailedReasonInlineNotCancelledValue,FBErrorReauthorizeFailedReasonUserCancelled];
      if([errors indexOfObject:error.userInfo[FBErrorLoginFailedReason]] == NSNotFound) {
        // Login failed for some reason other than user interaction
        if([self canRetryLogin]) {
          [self renewFBCredentialsAndRetryOnce];
        } else {
          self.isLoading = NO;
          [[TKAlertCenter defaultCenter] postAlertWithMessage:@"Unable to login through Facebook, please ensure you have granted correct permissions"];
        }
      }
    }
  }];
}*/



- (BOOL) canRetryLogin {
  return NSClassFromString(@"ACAccountStore") && !loginRetried;
}

- (void) renewFBCredentialsAndRetryOnce {
  /*loginRetried = YES;
  ACAccountStore *accountStore;
  ACAccountType *accountTypeFB;
  if ((accountStore = [[ACAccountStore alloc] init]) && (accountTypeFB = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook] ) ){
    NSArray *fbAccounts = [accountStore accountsWithAccountType:accountTypeFB];
    id account;
    if (fbAccounts && [fbAccounts count] > 0 && (account = [fbAccounts objectAtIndex:0])){
      [accountStore renewCredentialsForAccount:account completion:^(ACAccountCredentialRenewResult renewResult, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
          if(!error && renewResult == ACAccountCredentialRenewResultRenewed) {
            self.isLoading = YES;
            [FBSession.activeSession closeAndClearTokenInformation];
            [self performSelector:@selector(facebookLogin) withObject:nil afterDelay:0.5];
          } else {
            // Failed to renew credentials
            self.isLoading = NO;
            [[TKAlertCenter defaultCenter] postAlertWithMessage:@"Unable to verify Facebook credentials, try refreshing your Facebook account in your device's settings"];
          }
        });
      }];
    } else {
      // user is using facebook webview, can't renew credentials.
      self.isLoading = NO;
      [[TKAlertCenter defaultCenter] postAlertWithMessage:@"There was a problem logging in through Facebook, please ensure you have granted correct permissions"];
    }
  } else {
    self.isLoading = NO;
    [[TKAlertCenter defaultCenter] postAlertWithMessage:@"Unable to initialize account store, try refreshing your Facebook account in your device's settings"];
  }*/
}
- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (IBAction) loginPressed:(id)sender {
  [self.navigationController pushViewController: [[LoginWithEmailController alloc] init] animated:YES];
}

- (IBAction) linkPressed:(id)sender {
  WebviewController* controller = [[WebviewController alloc] init];
  controller.url = @{@(0):@"http://www.grape-vine.com/m/PrivacyPolicy.html",@(1):@"http://www.grape-vine.com/m/AboutUs.html",@(2):@"http://www.grape-vine.com/m/TermAndConditions.html"}[@(((UIView*) sender).tag)];
  [self.navigationController pushViewController:controller animated:YES];
}

@end
