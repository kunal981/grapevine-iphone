//
//  LoginForgotPasswordController.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "LoginForgotPasswordController.h"
#import "UIViewController+IsLoading.h"
#import "User.h"
#import <TapkuLibrary/TapkuLibrary.h>

@interface LoginForgotPasswordController ()

@end

@implementation LoginForgotPasswordController


-(void)viewDidLoad
{
    self.forget_Background_View.backgroundColor=[UIColor colorWithRed:0.7020 green:0.7059 blue:0.7176 alpha:0.7];
    submitButton.backgroundColor=[UIColor colorWithRed:0.4706 green:0.1529 blue:0.3961 alpha:1.0];
    
    [self lowerView];
}

- (IBAction) submitPressed:(id)sender {
  self.isLoading = YES;
  [User requestPasswordReset:emailTextField.text andCompletionBlock:^(NSError* error){
    self.isLoading = NO;
    
    if(error) {
      [[TKAlertCenter defaultCenter] postAlertWithMessage:@"Invalid Email, Please Retry"];
    } else {
      submitButton.hidden = YES;
      emailTextField.hidden = YES;
      infoLabel.text = @"We've sent a reset link to your email.  If you open the link from your phone, it will allow you to reset password in this app.";
      [infoLabel sizeToFit];
    }
  }];
}
#pragma mark - Sponser-View Method
-(void)lowerView
{
    
    int height=50;
    UIView * firstSponserView=[[UIView alloc]initWithFrame:CGRectMake(0,[UIScreen mainScreen].bounds.size.height-height, (self.view.frame.size.width/2)-0.5,height)];
    firstSponserView.backgroundColor=[UIColor whiteColor];
    
    [self.view addSubview:firstSponserView];
    UIImageView* spon1=[[UIImageView alloc]initWithFrame:CGRectMake(40, 0, firstSponserView.frame.size.width-80, height)];
    spon1.layer.masksToBounds=YES;
    spon1.image=[UIImage imageNamed:@"spon1.png"];
    [firstSponserView addSubview:spon1];
    
    UIView * secondSponserView=[[UIView alloc]initWithFrame:CGRectMake(firstSponserView.frame.size.width+1,  [UIScreen mainScreen].bounds.size.height-height, self.view.frame.size.width/2,height)];
    secondSponserView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:secondSponserView];
    UIImageView* spon2=[[UIImageView alloc]initWithFrame:CGRectMake(40, 3, secondSponserView.frame.size.width-80, height-3)];
    spon2.layer.masksToBounds=YES;
    spon2.image=[UIImage imageNamed:@"spon2.png"];
    [secondSponserView addSubview:spon2];
    
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
  [textField resignFirstResponder];
  return NO;
}
#pragma mark - StatusBarHidden Method
- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
