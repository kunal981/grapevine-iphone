//
//  LoginSplashController.m
//  GrapeVine
//
//  Created by Zachary Gavin on 9/9/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "LoginSplashController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "User.h"
#import "AppDelegate.h"
#import "LocationManager.h"

@interface LoginSplashController ()

@end

@implementation LoginSplashController

- (void) viewDidLoad {
  [super viewDidLoad];
  progressCircleView = [[TKProgressCircleView alloc] init];
  progressCircleView.center = CGPointMake(160, 220);
  
  progressCircleView.twirlMode = YES;
  
  [self.view addSubview:progressCircleView];
	

  
  logoImage.hidden = YES;
}

- (void) viewWillAppear:(BOOL)animated {
	if( [[UIScreen mainScreen] applicationFrame].size.height > 480 ) {
		backgroundImage.image = [UIImage imageNamed:@"Default-568h.png"];

		CGRect frame = backgroundImage.frame;
		frame.origin.y = -20;
		backgroundImage.frame = frame;		
	}
	
  if(self.view.bounds.size.height < backgroundImage.frame.size.height) {
    CGRect frame = backgroundImage.frame;
    frame.origin.y = self.view.bounds.size.height - frame.size.height;
    backgroundImage.frame = frame;
  }
}

- (void) viewDidAppear:(BOOL)animated {
  [[LocationManager sharedLocationManager] fetchLocation];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveLocationNotification:) name:@"LocationNotification" object:nil];
}

- (void) receiveLocationNotification:(NSNotification *) notification {
  CLLocationDegrees lat = LocationManager.sharedLocationManager.currentLocation.latitude;
  CLLocationDegrees lon = LocationManager.sharedLocationManager.currentLocation.longitude;
  
  logoImage.hidden = !(lon > -74.3 && lon < -73.7 &&  lat > 40.5 && lat < 40.8);
  
  [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LocationNotification" object:nil];
  
  if(FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded) {
    [FBSession.activeSession openWithBehavior:FBSessionLoginBehaviorForcingWebView completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
      if(session.state == FBSessionStateOpen) {
        [User loginWithSavedDetails:^(User* u, NSError* error) {
          [u updateAccessToken:session.accessToken];
       //   AppDelegate.currentDelegate.facebook.accessToken = session.accessToken;
          
          [self transition:error];
        }];
      } else {
        [self transition:error];
      }
    }];
  } else if([User isLoginSaved]) {
    [User loginWithSavedDetails:^(User* u, NSError* error) {
      [self transition:error];
    }];
  } else {
    [progressCircleView removeFromSuperview];
    [self transition:nil];
  }
}

- (void) transition:(NSError*)error {
  if(User.currentUser && !error) {
    [AppDelegate.currentDelegate transitionToHome:UIViewAnimationOptionTransitionCrossDissolve];
  } else {
    [AppDelegate.currentDelegate transitionToLogin:UIViewAnimationOptionTransitionCrossDissolve];
    if(error) [[TKAlertCenter defaultCenter] postAlertWithMessage:@"Automatic Login Failed"];
  }
}

- (void) dealloc {
	// If you don't remove yourself as an observer, the Notification Center
	// will continue to try and send notification objects to the deallocated
	// object.
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
