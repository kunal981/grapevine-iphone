//
//  LoginWithEmailController.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "LoginWithEmailController.h"
#import "User.h"
#import "APIWrapper.h"
#import "UIViewController+IsLoading.h"
#import "NSDictionary+Enumerable.h"
#import "LoginForgotPasswordController.h"
#import "AppDelegate.h"
#import "LoginActivationController.h"
#import "ProfileInterestsController.h"
#import "GANTracker.h"

@interface LoginWithEmailController ()

@end

@implementation LoginWithEmailController
#pragma mark - ViewDidLoad and UITextField Method
- (void) viewDidLoad {
  [super viewDidLoad];
    
      loginContainer.backgroundColor=[UIColor colorWithRed:0.7020 green:0.7059 blue:0.7176 alpha:0.7];
    registrationContainer.backgroundColor=[UIColor colorWithRed:0.7020 green:0.7059 blue:0.7176 alpha:0.7];

    loginEmailTextField.backgroundColor=[UIColor whiteColor];
    loginEmailTextField.layer.borderColor=[UIColor whiteColor].CGColor;
    loginEmailTextField.layer.borderWidth=1.0;
    loginEmailTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@"  Enter your Email"attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:1]}];
    loginEmailTextField.font=[UIFont boldSystemFontOfSize:15];


    loginPasswordTextField.backgroundColor=[UIColor whiteColor];
    loginPasswordTextField.layer.borderColor=[UIColor whiteColor].CGColor;
    loginPasswordTextField.layer.borderWidth=1.0;
    loginPasswordTextField.attributedPlaceholder =
    [[NSAttributedString alloc] initWithString:@"  Enter your password"
                                    attributes:@{
                                                 NSFontAttributeName : [UIFont boldSystemFontOfSize:15]
                                                 }
     ];


    self.btnSubmit.backgroundColor=[UIColor colorWithRed:0.4706 green:0.1529 blue:0.3961 alpha:1.0];
    self.btnRegister.backgroundColor=[UIColor colorWithRed:0.4706 green:0.1529 blue:0.3961 alpha:1.0];

 
    CGRect frame = segmentedControl.frame;
  frame.size.height = 29;
  segmentedControl.frame= frame;
  
  registrationAgeTextField.inputAccessoryView = registrationToolbar;
  registrationZipcodeTextField.inputAccessoryView = registrationToolbar;
    
    [self lowerView];
}
#pragma mark - Sponser-View Method
-(void)lowerView
{
    
    int height=50;
    UIView * firstSponserView=[[UIView alloc]initWithFrame:CGRectMake(0,[UIScreen mainScreen].bounds.size.height-height, (self.view.frame.size.width/2)-0.5,height)];
    firstSponserView.backgroundColor=[UIColor whiteColor];
   
    [self.view addSubview:firstSponserView];
    UIImageView* spon1=[[UIImageView alloc]initWithFrame:CGRectMake(40, 0, firstSponserView.frame.size.width-80, height)];
    spon1.layer.masksToBounds=YES;
    spon1.image=[UIImage imageNamed:@"spon1.png"];
    [firstSponserView addSubview:spon1];

    UIView * secondSponserView=[[UIView alloc]initWithFrame:CGRectMake(firstSponserView.frame.size.width+1,  [UIScreen mainScreen].bounds.size.height-height, self.view.frame.size.width/2,height)];
    secondSponserView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:secondSponserView];
    UIImageView* spon2=[[UIImageView alloc]initWithFrame:CGRectMake(40, 3, secondSponserView.frame.size.width-80, height-3)];
    spon2.layer.masksToBounds=YES;
    spon2.image=[UIImage imageNamed:@"spon2.png"];
     [secondSponserView addSubview:spon2];
    
}
#pragma mark - ViewWillAppear Method

- (void) viewWillAppear:(BOOL)animated {
    
    
//    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Neue" size:17],NSFontAttributeName,nil]];
       self.navigationController.navigationBar.hidden=NO;
    self.navigationItem.hidesBackButton=YES;
    
  [[GANTracker sharedTracker] trackPageview:@"loginfb" withError:nil];
    
 
}
#pragma mark - StatusBarHidden Method
- (BOOL)prefersStatusBarHidden {
    return YES;
}


- (void) track {
  [self track:nil];
}

- (void) track:(NSString*)page {
  page = page ?: segmentedControl.selectedSegmentIndex == 0 ? @"login/email" : @"login/register";
  
  [[GANTracker sharedTracker] trackPageview:page withError:nil];
}

- (void) viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  
  scrollView.contentSize = self.view.frame.size;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
  NSDictionary* dict = @{ @(3):registrationAgeTextField };
  
  return [dict.allValues indexOfObject:textField] == NSNotFound || [dict all:^(NSNumber* length,UITextField* tf) {
    if(tf != textField) return YES;
    
    NSRegularExpression* regexp = [NSRegularExpression regularExpressionWithPattern:[NSString stringWithFormat:@"\\A\\d{0,%d}\\Z",length.intValue] options:0 error:nil];
    NSString* tmp = [tf.text stringByReplacingCharactersInRange:range withString:string];
    
    return (BOOL) ([regexp rangeOfFirstMatchInString:tmp options:0 range:NSMakeRange(0, tmp.length)].location != NSNotFound);
  }];
}

- (void) textFieldDidBeginEditing:(UITextField *)textField {
  [UIView animateWithDuration:0.25 animations:^{
    CGRect frame = scrollView.frame;
    frame.size.height = self.view.frame.size.height - (textField.inputView.frame.size.height ?: 216);
    scrollView.frame = frame;
		
		for(UIView* view in @[loginContainer,registrationContainer]) {
			if( !view.hidden ) {
				scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, CGRectGetMaxY(view.frame));
			}
		}
  } completion:^(BOOL finished){
    [UIView animateWithDuration:0.25 animations:^{
      if( scrollView.contentSize.height > scrollView.frame.size.height ) scrollView.contentOffset = CGPointMake(0,textField.frame.origin.y);
    }];
  }];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
  [textField resignFirstResponder];
  [self resetScrollView];
  return NO;
}

- (IBAction) donePressed:(id)sender {
  for (UITextField* textField in @[registrationZipcodeTextField,registrationAgeTextField]) {
    [textField resignFirstResponder];
  }
  [self resetScrollView];
}

- (void) resetScrollView {
  [UIView animateWithDuration:0.25 animations:^{
    scrollView.frame = self.view.frame;
  }];
}

- (NSString*) getRegistrationError {
  
  //Check email
  NSRegularExpression* regexp = [NSRegularExpression regularExpressionWithPattern:@"\\A.*@([a-z0-9\\-]+\\.)+[a-z0-9\\-]+\\Z" options:NSRegularExpressionCaseInsensitive error:nil];
  if([regexp rangeOfFirstMatchInString:registrationEmailTextField.text options:0 range:NSMakeRange(0, registrationEmailTextField.text.length)].location == NSNotFound)
    return @"Invalid email";
  
  //Check password
  if(registrationPasswordTextField.text.length < 8)
    return @"Password must be at least 8 characters";
  
  //Check confirm password
  if(![registrationPasswordConfirmationTextField.text isEqualToString:registrationPasswordTextField.text])
    return @"Passwords must match";
  
  //Check zip code
  if(registrationZipcodeTextField.text.length < 5)
    return @"Zip Code must be 5 numbers long";
  
  return nil;
}

- (IBAction) registerPressed:(id)sender {
  
  [self.view endEditing:YES];
  [self resetScrollView];
  NSString* err = [self getRegistrationError];
  if(err) {
    [self track:@"login/register/error"];
    errorLabel.hidden = NO;
    errorLabel.text = err;
    return;
  }
  
  self.isLoading = YES;
  NSDictionary* params = @{
    kUserParamsEmail        : registrationEmailTextField.text,
    kUserParamsPassword     : registrationPasswordTextField.text,
    kUserParamsAge          : registrationAgeTextField.text,
    kUserParamsZipcode      : registrationZipcodeTextField.text
  };
  [User registerWithParams:params andCompletionBlock:^(User* user,NSError* error) {
    self.isLoading = NO;
    if (error) {
      [self track:@"login/register/error"];
      errorLabel.text = error.code == -1001 && [error.domain isEqualToString:NSURLErrorDomain] ? @"Request timeout.  Please try again." : error.description;
      errorLabel.hidden = NO;
    } else if(user.keyStatus == UserKeyStatusActivated) {
      ProfileInterestsController* controller = [[ProfileInterestsController alloc] init];
      [self.navigationController pushViewController:controller animated:YES];
    } else {
      [self.navigationController pushViewController:[[LoginActivationController alloc] init] animated:YES];
    }
  }];
}

- (IBAction) loginPressed:(id)sender {
  [self.view endEditing:YES];
  self.isLoading = YES;
  [self resetScrollView];
  [User loginWithEmail:loginEmailTextField.text andPassword:loginPasswordTextField.text andCompletionBlock:^(User* u, NSError* error){
    self.isLoading = NO;
    if(!error) {
      [AppDelegate.currentDelegate transitionToHome:UIViewAnimationOptionTransitionFlipFromRight];
    } else if(error.code == -1001 && [error.domain isEqualToString:NSURLErrorDomain]) {
      errorLabel.text = @"Request timeout.  Please try again.";
      errorLabel.hidden = NO;
    } else {
      errorLabel.text = @" Invalid Email/Password";
      errorLabel.hidden = NO;
    }
  }];
}

- (IBAction) forgotPasswordPressed:(id)sender {
  [self.navigationController pushViewController:[[LoginForgotPasswordController alloc] init] animated:YES];
}


- (IBAction) segmentChanged:(id)sender {
  for(UIView* view in @[loginContainer,registrationContainer]) {
		view.hidden = !view.hidden;
		
		if( !view.hidden ) {
			scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, CGRectGetMaxY(view.frame));
		}
	}
}


- (IBAction)btnRegisterPressed:(id)sender {
    
    for(UIView* view in @[loginContainer,registrationContainer]) {
        view.hidden = !view.hidden;
        
        if( !view.hidden ) {
            scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, CGRectGetMaxY(view.frame));
        }
    }

}
- (IBAction)btn_Back_Login:(id)sender {
    for(UIView* view in @[loginContainer,registrationContainer]) {
        view.hidden = !view.hidden;
        
        if( !view.hidden ) {
            scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, CGRectGetMaxY(view.frame));
        }
    }

}
@end
