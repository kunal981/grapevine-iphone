//
//  LoginController.h
//  GrapeVine
//
//  Created by Zachary Gavin on 7/23/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>

extern const NSString* KEYCHAIN_IDENTIFIER;
@interface LoginWithFacebookController : UIViewController {
	IBOutlet UIButton* backdoorButton;
  IBOutlet UIButton* loginButton;
  
  BOOL loginRetried;
}



@end
