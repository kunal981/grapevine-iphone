//
//  LoginResetPasswordController.h
//  GrapeVine
//
//  Created by Zachary Gavin on 9/18/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface LoginRecoverPasswordController : UIViewController <UITextFieldDelegate> {
  IBOutlet UITextField* passwordTextField;
  IBOutlet UITextField* passwordConfirmationTextField;
}

@property (strong,nonatomic) User* user;

@end
