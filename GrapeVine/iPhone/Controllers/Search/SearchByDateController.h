//
//  SearchByDateController.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchByController.h"
#import <TapkuLibrary/TapkuLibrary.h>

@interface SearchByDateController : SearchByController <UITextFieldDelegate,TKCalendarMonthViewDataSource> {
  IBOutlet UIView* calendarContainer;
  TKCalendarMonthView* calendar;
  
  IBOutlet UIButton* startDateButton;
  IBOutlet UIButton* endDateButton;  
  
  NSDateFormatter* formatter;
  
  NSDictionary* dates;
}

@end
