//
//  SearchOrganizationsController.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickerButton.h"

@interface SearchOrganizationsController : UIViewController <UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,PickerButtonDelegate> {
  IBOutlet UITableView* table;
  IBOutlet UITextField* textField;
  IBOutlet PickerButton* pickerButton;
  
  UIView* tableFooter;
  
  NSArray* organizations;
  NSArray* sections;
  
  NSMutableDictionary* indexedOrganizations;
}

@end
