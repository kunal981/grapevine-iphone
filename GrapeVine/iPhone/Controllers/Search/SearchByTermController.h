//
//  SearchByTermController.h
//  Grapevine
//
//  Created by Zachary Gavin on 10/3/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "EventsController.h"

@interface SearchByTermController : EventsController <UITextFieldDelegate> {
  IBOutlet UITextField* searchTextField;
}

@property (nonatomic,strong) NSString* searchTerm;

@end
