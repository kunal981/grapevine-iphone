//
//  SearchByNeighborhoodController.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "SearchByNeighborhoodController.h"
#import "Neighborhood.h"
#import "Event.h"
#import "EventTableViewCell.h"
#import "EventController.h"
#import "UIViewController+IsLoading.h"
#import "NSDictionary+Subscript.h"
#import "NSArray+Subscript.h"
#import "User.h"
#import "NSArray+Enumerable.h"
#import "GANTracker.h"

@interface SearchByNeighborhoodController ()

@end

static NSString* CurrentLocationString = @"Current Location";
static NSString* AllNeighborhoodsString = @"All Neighborhoods";

@implementation SearchByNeighborhoodController

- (void) viewDidLoad {
  [super viewDidLoad];
  
  CGRect frame = segmentedControl.frame;
  frame.size.height = 29;
  segmentedControl.frame= frame;
}

- (void) viewWillAppear:(BOOL)animated {
  neighborhoods = User.currentUser.community.neighborhoods;
  NSInteger idx = pickerButton.selectedIndex;
  pickerButton.options =  [@[CurrentLocationString,AllNeighborhoodsString] arrayByAddingObjectsFromArray:[neighborhoods mapKey:@"name"]];
  pickerButton.selectedIndex = idx >= neighborhoods.count ? 0 : idx;
  [super viewWillAppear:animated];
  [self track];
}

- (void) track {
  [[GANTracker sharedTracker] trackPageview:segmentedControl.selectedSegmentIndex ? @"events/neighborhood/closest" : @"events/neighborhood/soonest" withError:nil];
}

- (void) loadData {
  self.isLoading = YES;
  EventCallback callback = ^(NSDictionary* eventsDict,NSError* error){
    self.isLoading = NO;
    self.events = eventsDict[pickerButton.selectedIndex ? EventTypeAll : EventTypeNearby];
    self.events = [self.events select:^(Event* event){ return (BOOL) (!event.isContent); }];
    [self sortEvents];
    [self reloadTable];
  };
  

  if (pickerButton.selectedIndex == 0) {
    [Event nearbyEventsForUser:User.currentUser withCompletionBlock:callback];
  } else if (pickerButton.selectedIndex == 1) {
    [Event eventsForUser:User.currentUser byCommunity:User.currentUser.community withCompletionBlock:callback];
  } else {
    [Event eventsForUser:User.currentUser byNeighborhood:neighborhoods[pickerButton.selectedIndex-2] withCompletionBlock:callback];
  }
}

- (void) pickerButton:(PickerButton *)pickerButton didChangeValue:(NSString *)value {
  [self loadData];
}


@end
