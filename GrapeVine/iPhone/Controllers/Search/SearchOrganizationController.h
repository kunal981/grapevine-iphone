//
//  SearchOrganizationController.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/12/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "EventsController.h"
#import "Organization.h"
#import "RemoteImageView.h"

@interface SearchOrganizationController : EventsController {
  IBOutlet UISegmentedControl* sortSegmentedControl;
  IBOutlet UISegmentedControl* eventsSegmentedControl;
  IBOutlet UISegmentedControl* profileSegmentedControl;
  
  IBOutlet UIScrollView* scrollView;
  
  IBOutlet UILabel* addressLabel;
  IBOutlet UIButton* urlButton;
  IBOutlet UIButton* facebookUrlButton;
  IBOutlet UILabel* descriptionLabel;
  IBOutlet RemoteImageView* profileImageView;
  IBOutlet RemoteImageView* eventsImageView;
  IBOutlet UIButton* callButton;
  
  IBOutlet UILabel* nameLabel;
  IBOutlet UIView* infoContainer;
  
  IBOutlet UIView* moreInfoContainer;
  IBOutlet UILabel* moreInfoNameLabel;
  IBOutlet UILabel* moreInfoEmailLabel;
  IBOutlet UILabel* moreInfoPhoneLabel;
  
  IBOutlet UIView* footerContainer;
}

@property (nonatomic,retain) Organization* organization;

@end
