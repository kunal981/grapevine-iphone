//
//  SearchByNeighborhoodnController.h
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchByController.h"
#import "PickerButton.h"

@interface SearchByNeighborhoodController : SearchByController <PickerButtonDelegate> {
  IBOutlet PickerButton* pickerButton;
  
  NSArray* neighborhoods;
}

@end
