//
//  SearchByController.h
//  GrapeVine
//
//  Created by Zachary Gavin on 3/13/13.
//  Copyright (c) 2013 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventsController.h"

@interface SearchByController : EventsController {
  IBOutlet UISegmentedControl* segmentedControl;
}

- (void) sortEvents;

@end
