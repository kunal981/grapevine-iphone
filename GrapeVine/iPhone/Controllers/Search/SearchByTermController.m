//
//  SearchByTermController.m
//  Grapevine
//
//  Created by Zachary Gavin on 10/3/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "SearchByTermController.h"
#import "Event.h"
#import "User.h"
#import "UIViewController+IsLoading.h"
#import "GANTracker.h"

@interface SearchByTermController ()

@end

@implementation SearchByTermController

- (void) viewDidLoad {
  [super viewDidLoad];
  searchTextField.text = self.searchTerm;
}

- (void) viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [[GANTracker sharedTracker] trackPageview:[@"events/search/" stringByAppendingString:[self.searchTerm stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]] withError:nil];
}

- (void) loadData {
  self.isLoading = YES;
  [Event eventsForUser:User.currentUser bySearchTerm:self.searchTerm withCompletionBlock:^(NSDictionary* events,NSError* error) {
    self.isLoading = NO;
    self.events = events[EventTypeAll];
    [self reloadTable];
  }];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
  [textField resignFirstResponder];
  self.searchTerm = textField.text;
  [[GANTracker sharedTracker] trackEvent:@"Search" action:@"Event" label:textField.text value:0 withError:nil];
  [self loadData];
  return NO;
}

@end
