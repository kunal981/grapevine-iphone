//
//  SearchOrganizationsController.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "SearchOrganizationsController.h"
#import "Organization.h"
#import "SearchOrganizationController.h"
#import "NSArray+Subscript.h"
#import "NSDictionary+Subscript.h"
#import "NSMutableDictionary+Subscript.h"
#import "NSArray+Enumerable.h"
#import "TableViewHeader.h"
#import "Interest.h"
#import "User.h"
#import "GANTracker.h"

@interface SearchOrganizationsController ()

@end

static NSString* AllInterestsOption = @"All Interests";

@implementation SearchOrganizationsController


- (void) viewDidLoad {
  [super viewDidLoad];
  
  organizations = [Organization all];
  organizations = [organizations sortedArrayUsingComparator:^(Organization* a, Organization* b) {
    return [a.name localizedCaseInsensitiveCompare:b.name];
  }];

  pickerButton.options = [@[AllInterestsOption] arrayByAddingObjectsFromArray:[[Interest.all mapKey:@"name"] compact]];
  
  textField.returnKeyType = UIReturnKeyDone;
  textField.enablesReturnKeyAutomatically = NO;
  
  tableFooter = table.tableFooterView;
}

- (void) viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [[GANTracker sharedTracker] trackPageview:@"events/organizations" withError:nil];
  [self filterOrganizations];
}

- (void) filterOrganizations {
  [self filterOrganizations:nil];
}

- (void) filterOrganizations:(NSString*) string {
  string = string ?: textField.text;
  
  NSArray* filteredOrganizations = [organizations select:^(Organization* org) {
    BOOL matchesCommunity = [org.communities indexOfObject:User.currentUser.community] != NSNotFound;
    BOOL matchesString = !string || string.length == 0 || [org.name.uppercaseString rangeOfString:string.uppercaseString].location != NSNotFound;
    BOOL matchesInterest = pickerButton.selectedIndex == 0 || [org.interests any:^(Interest* interest) { return [interest.name isEqualToString:pickerButton.value]; }];
    return (BOOL) (matchesCommunity && matchesString && matchesInterest);
  }];
  
  indexedOrganizations = [NSMutableDictionary dictionaryWithCapacity:filteredOrganizations.count];
  
  for(Organization* org in filteredOrganizations) {
    if(org.name.length < 1) continue;
    NSString* letter = [org.name substringToIndex:1].uppercaseString;
    letter = letter.UTF8String[0] >= 65 && letter.UTF8String[0] <= 90 ? letter : @"#";
    if(!indexedOrganizations[letter]) indexedOrganizations[letter] = [@[] mutableCopy];
    [indexedOrganizations[letter] addObject:org];
  }
  
  sections = [indexedOrganizations.allKeys sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
  
  [table reloadData];
  
  if(sections.count == 0) {
    table.tableFooterView = nil;
    
    CGRect frame = tableFooter.frame;
    frame.origin.y = self.view.frame.size.height - frame.size.height;
    tableFooter.frame = frame;
    
    if(tableFooter.superview == nil) [self.view addSubview:tableFooter];
  } else if(table.tableFooterView != tableFooter) {
    table.tableFooterView = tableFooter;
  }
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
  [self filterOrganizations:@""];
  return YES;
}

- (BOOL) textField:(UITextField *)_textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
  [self filterOrganizations:[textField.text stringByReplacingCharactersInRange:range withString:string]];
  return YES;
}

- (BOOL) textFieldShouldReturn:(UITextField *)_textField {
  [textField resignFirstResponder];
  [[GANTracker sharedTracker] trackEvent:@"Search" action:@"Organization" label:textField.text value:0 withError:nil];
  return NO;
}

- (void) pickerButton:(PickerButton *)pickerButton didChangeValue:(NSString *)value {
  [self filterOrganizations];
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
  return sections.count;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return ((NSArray*) indexedOrganizations[sections[section]]).count;
}

- (UIView*) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
  return [[TableViewHeader alloc] initWithTitle:sections[section]];
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
  return [TableViewHeader headerHeight];
}

- (NSArray*) sectionIndexTitlesForTableView:(UITableView *)tableView {
  NSMutableArray* tmp = [@[UITableViewIndexSearch] mutableCopy];
  for(int i=65;i<=90;i++) { [tmp addObject:[[NSString alloc] initWithBytes:&i length:1 encoding:NSUTF8StringEncoding]]; }
  [tmp addObject:@"#"];
  
  return tmp;
}

- (NSInteger) tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
  if(index == 0) {
    table.contentOffset = CGPointMake(0, 0);
    return -1;
  }
  
  if([title isEqualToString:@"#"]) return sections.count-1;
  
  for(int i=title.UTF8String[0];i>=65;i--) {
    NSInteger section = [sections indexOfObject:[[NSString alloc] initWithBytes:&i length:1 encoding:NSUTF8StringEncoding]];
    if(section != NSNotFound) return section;
  }
  
  return 0;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"organizationCell"] ?: [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"organizationCell"];
  cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
  cell.textLabel.text = ((Organization*) indexedOrganizations[sections[indexPath.section]][indexPath.row]).name;
  return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  Organization* org = [organizations find:^(Organization* o){
    return [o.name isEqualToString: [table cellForRowAtIndexPath:indexPath].textLabel.text];
  }];
  
  SearchOrganizationController* organizationController = [[SearchOrganizationController alloc] init];
  organizationController.organization = org;
  [self.navigationController pushViewController:organizationController animated:YES];
  [table deselectRowAtIndexPath:[table indexPathForSelectedRow] animated:NO];
}

@end
