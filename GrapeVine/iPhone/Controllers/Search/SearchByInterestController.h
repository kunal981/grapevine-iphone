//
//  SearchByInterestController.h
//  GrapeVine
//
//  Created by Zachary Gavin on 3/13/13.
//  Copyright (c) 2013 com.grape-vine. All rights reserved.
//

#import "SearchByController.h"
#import "PickerButton.h"

@interface SearchByInterestController : SearchByController <PickerButtonDelegate> {
  IBOutlet PickerButton* pickerButton;
  
  NSArray* interests;
}

@end
