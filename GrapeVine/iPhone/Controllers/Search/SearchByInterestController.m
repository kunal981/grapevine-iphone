//
//  SearchByInterestController.m
//  GrapeVine
//
//  Created by Zachary Gavin on 3/13/13.
//  Copyright (c) 2013 com.grape-vine. All rights reserved.
//

#import "SearchByInterestController.h"
#import "Interest.h"
#import "NSArray+Enumerable.h"
#import "GANTracker.h"
#import "UIViewController+IsLoading.h"
#import "Event.h"
#import "User.h"

@interface SearchByInterestController ()

@end

@implementation SearchByInterestController



- (void) viewDidLoad {
  [super viewDidLoad];
  
  CGRect frame = segmentedControl.frame;
  frame.size.height = 29;
  segmentedControl.frame= frame;
}

- (void) viewWillAppear:(BOOL)animated {
  interests = [Interest all];
  NSInteger idx = pickerButton.selectedIndex;
  pickerButton.options =  [interests mapKey:@"name"];
  pickerButton.selectedIndex = idx >= interests.count ? 0 : idx;
  [super viewWillAppear:animated];
  [self track];
}

- (void) track {
  [[GANTracker sharedTracker] trackPageview:segmentedControl.selectedSegmentIndex ? @"events/interest/closest" : @"events/interest/soonest" withError:nil];
}

- (void) loadData {
  self.isLoading = YES;

  [Event eventsForUser:User.currentUser byInterest:interests[pickerButton.selectedIndex] withCompletionBlock:^(NSDictionary* eventsDict,NSError* error){
    self.isLoading = NO;
    self.events = eventsDict[pickerButton.selectedIndex ? EventTypeAll : EventTypeNearby];
    self.events = [self.events select:^(Event* event){ return (BOOL) (!event.isContent); }];
    [self sortEvents];
    [self reloadTable];
  }];
}

- (void) pickerButton:(PickerButton *)pickerButton didChangeValue:(NSString *)value {
  [self loadData];
}
@end
