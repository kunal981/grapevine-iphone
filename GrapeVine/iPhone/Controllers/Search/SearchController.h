//
//  SearchController.h
//  GrapeVine
//
//  Created by Zachary Gavin on 7/23/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchController : UIViewController <UITextFieldDelegate> {
  IBOutlet UITextField* searchTextField;
  IBOutlet UIToolbar* toolbar;
}

@end
