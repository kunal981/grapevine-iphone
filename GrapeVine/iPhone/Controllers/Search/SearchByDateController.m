//
//  SearchByDateController.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/2/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "SearchByDateController.h"
#import "Event.h"
#import "EventTableViewCell.h"
#import "EventController.h"
#import "UIViewController+IsLoading.h"
#import "UIView+Sliding.h"
#import "TKCalendarMonthView+date.h"
#import "User.h"
#import "GANTracker.h"
#import "NSArray+Enumerable.h"

@interface SearchByDateController ()

@end

@implementation SearchByDateController

static NSDate* startDate;
static NSDate* endDate;

- (void) viewDidLoad {
  [super viewDidLoad];

  CGRect frame = segmentedControl.frame;
  frame.size.height = 29;
  segmentedControl.frame= frame;
  
  if(!startDate) startDate = [NSDate date];
  if(!endDate) endDate = [startDate dateByAddingTimeInterval:7*24*60*60];
  
  formatter = [[NSDateFormatter alloc] init];
  formatter.locale = [NSLocale currentLocale];
  formatter.dateFormat = @"M/d/yy";
  
  [startDateButton setTitle:[formatter stringFromDate:startDate] forState:UIControlStateNormal];
  [endDateButton setTitle:[formatter stringFromDate:endDate] forState:UIControlStateNormal];
  
  calendar = [[TKCalendarMonthView alloc] initWithSundayAsFirst:YES];
  [calendarContainer addSubview:calendar];
  frame = calendar.frame;
  frame.origin.y = ((UIView*) calendarContainer.subviews[0]).frame.size.height;
  calendar.frame = frame;
  
  [startDateButton setBackgroundImage:[[startDateButton backgroundImageForState:UIControlStateNormal] resizableImageWithCapInsets:UIEdgeInsetsMake(1, 5, 1, 23)] forState:UIControlStateNormal];
  [startDateButton setBackgroundImage:[[startDateButton backgroundImageForState:UIControlStateSelected] resizableImageWithCapInsets:UIEdgeInsetsMake(1, 5, 1, 23)] forState:UIControlStateSelected];
  
  [endDateButton setBackgroundImage:[[endDateButton backgroundImageForState:UIControlStateNormal] resizableImageWithCapInsets:UIEdgeInsetsMake(1, 5, 1, 23)] forState:UIControlStateNormal];
  [endDateButton setBackgroundImage:[[endDateButton backgroundImageForState:UIControlStateSelected] resizableImageWithCapInsets:UIEdgeInsetsMake(1, 5, 1, 23)] forState:UIControlStateSelected];
  
  [Event groupedEventsForUser:User.currentUser withCompletionBlock:^(NSDictionary* _dates,NSError* error){
    dates = _dates;
    calendar.dataSource = self;
    [calendar reload];
  }];
}

- (void) viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [self track];
}

- (void) track {
  [[GANTracker sharedTracker] trackPageview:segmentedControl.selectedSegmentIndex ? @"events/date/closest" : @"events/date/soonest" withError:nil];
}

- (NSArray*) calendarMonthView:(TKCalendarMonthView *)monthView marksFromDate:(NSDate *)startDate toDate:(NSDate *)lastDate {
  NSMutableArray* array = [@[] mutableCopy];
  NSDate* date = startDate;
  
  while([date earlierDate:lastDate] == date || [date isSameDay:lastDate]) {
    NSString* dateString = [formatter stringFromDate:[date dateByAddingDays:1]];
    
    [array addObject:@(dates[dateString] != nil)];
    date = [date dateByAddingDays:1];
  }

  return array;
}

- (void) loadData {
  self.isLoading = YES;
  NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
  NSUInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
  
  NSDateComponents *nextDayComponents = [[NSDateComponents alloc] init];
  nextDayComponents.day = 1;
  NSDate *nextDay = [gregorian dateByAddingComponents:nextDayComponents toDate:endDate options:0];
  
  NSDateComponents* endComponents = [gregorian components:unitFlags fromDate:nextDay];
  NSDateComponents *startComponents = [gregorian components:unitFlags fromDate:startDate];
  
  for(NSDateComponents* components in @[startComponents,endComponents]) {
    components.hour = 0;
    components.minute = 0;
    components.second = 0;
  }
  
  [Event eventsForUser:[User currentUser] fromDate:[gregorian dateFromComponents:startComponents] toDate:[gregorian dateFromComponents:endComponents] withCompletionBlock:^(NSDictionary* eventsDict,NSError* error) {
    self.isLoading = NO;
    self.events = eventsDict[EventTypeAll];
    self.events = [self.events select:^(Event* event){ return (BOOL) (!event.isContent); }];
    [self sortEvents];
    [self reloadTable];
  }];
}

- (IBAction) dateButtonPressed:(id) sender {
  [calendarContainer slideIn];
  
  for(UIButton* button in @[startDateButton,endDateButton]) {
    button.selected = (button == sender);
  }
  
  [[GANTracker sharedTracker] trackPageview:@"events/date/calendar" withError:nil];
  
  calendar.date = sender == startDateButton ? startDate : endDate;
}

- (IBAction) inputDonePressed:(id)sender {
  [calendarContainer slideOut];

  UIButton* button = startDateButton.selected ? startDateButton : endDateButton;
  button.selected = NO;
  
  NSDate* date = calendar.date;

  if(button == startDateButton) startDate = date ?: startDate; else endDate = date ?: endDate;
  
  if ([endDate earlierDate:startDate] == endDate) {
    endDate = startDate;
    [endDateButton setTitle:[formatter stringFromDate:endDate] forState:UIControlStateNormal];
  }
  
  [startDateButton setTitle:[formatter stringFromDate:startDate] forState:UIControlStateNormal];
  [endDateButton setTitle:[formatter stringFromDate:endDate] forState:UIControlStateNormal];

  [self loadData];
}

@end
