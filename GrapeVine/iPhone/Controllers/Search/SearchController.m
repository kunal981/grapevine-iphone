//
//  SearchController.m
//  GrapeVine
//
//  Created by Zachary Gavin on 7/23/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "SearchController.h"
#import "NSArray+Enumerable.h"
#import "SearchOrganizationsController.h"
#import "SearchByDateController.h"
#import "SearchByNeighborhoodController.h"
#import "SearchByTermController.h"
#import "SearchByInterestController.h"
#import "GANTracker.h"

@interface SearchController ()

@end

@implementation SearchController

- (void) viewDidLoad {
	[super viewDidLoad];
	
    self.navigationItem.title=@"SEARCH EVENTS";
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor darkGrayColor],NSForegroundColorAttributeName, [UIFont fontWithName:@"Helvetica Bold" size:19],NSFontAttributeName,nil]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"bg_header.png"] forBarMetrics:UIBarMetricsDefault];
    

    self.view.backgroundColor=[UIColor colorWithRed:0.8588 green:0.8706 blue:0.8902 alpha:1.0];
    
  searchTextField.leftView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home.search.icon.png"]];
  searchTextField.leftViewMode = UITextFieldViewModeAlways;
  searchTextField.inputAccessoryView = toolbar;
searchTextField.attributedPlaceholder =[[NSAttributedString alloc] initWithString:@"  Search All Opportunities"
                                    attributes:@{
                                                 NSFontAttributeName : [UIFont boldSystemFontOfSize:15]
                                                 }
     ];

  
  for(UIButton* button in [self.view.subviews selectClass:[UIButton class]]) {
    button.titleLabel.textAlignment = UITextAlignmentCenter;
  }
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
  [textField resignFirstResponder];
  SearchByTermController* controller = [[SearchByTermController alloc] init];
  controller.searchTerm = textField.text;
  [[GANTracker sharedTracker] trackEvent:@"Search" action:@"Event" label:textField.text value:0 withError:nil];
  
  [self.navigationController pushViewController:controller animated:YES];
  
  return NO;
}

- (IBAction) cancelSearchPressed:(id)sender {
  [searchTextField resignFirstResponder];
}

- (IBAction) interestPressed:(id)sender {
  [self.navigationController pushViewController:[[SearchByInterestController alloc] init] animated:YES];
}

- (IBAction) organizationsPressed:(id)sender {
  [self.navigationController pushViewController:[[SearchOrganizationsController alloc] init] animated:YES];
}

- (IBAction) byDatePressed:(id)sender {
  [self.navigationController pushViewController:[[SearchByDateController alloc] init] animated:YES];
}

- (IBAction) byNeighborhoodPressed:(id)sender {
  [self.navigationController pushViewController:[[SearchByNeighborhoodController alloc] init] animated:YES];
}
#pragma mark - StatusBarHidden Method
- (BOOL)prefersStatusBarHidden {
    return YES;
}


@end
