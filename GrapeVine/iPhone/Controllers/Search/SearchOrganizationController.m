//
//  SearchOrganizationController.m
//  GrapeVine
//
//  Created by Zachary Gavin on 8/12/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "SearchOrganizationController.h"
#import "UIViewController+IsLoading.h"
#import "Event.h"
#import "EventTableViewCell.h"
#import "NSDictionary+Subscript.h"
#import "User.h"
#import "Sharer.h"
#import "GANTracker.h"

@interface SearchOrganizationController ()

@end

@implementation SearchOrganizationController

- (void) viewDidLoad {
  [super viewDidLoad];
  
  for(UISegmentedControl* segmentedControl in @[profileSegmentedControl,eventsSegmentedControl,sortSegmentedControl]) {
    CGRect frame = segmentedControl.frame;
    frame.size.height = 29;
    segmentedControl.frame = frame;
  }
  
  profileImageView.contentMode = UIViewContentModeScaleAspectFit;
  eventsImageView.contentMode = UIViewContentModeScaleAspectFit;
}

- (void) viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  self.title = self.organization.name;
  [self track];
  [User.currentUser trackAction:@"GetOrganization" withData:self.organization.key];
}

- (void) track {
  [[GANTracker sharedTracker] trackPageview:[NSString stringWithFormat:@"events/organizations/%@/%@",scrollView.hidden ? @"events" : @"profile",self.organization.key] withError:nil];
}

- (void) loadData {
  if(self.organization) { [self refresh]; }
}

- (void) setOrganization:(Organization *)organization {
  _organization = organization;
  if(self.isViewLoaded) { [self refresh]; }
}

- (void) refresh {
  self.isLoading = YES;
  [self.organization updateWithCompletionBlock:^(NSError* error) {
    self.events = self.organization.events;
    [self sortEvents];
    [self reloadTable];
    
		NSString* addressText = self.organization.addressLine1.length > 0 ? [self.organization.addressLine1 stringByAppendingString:@"\n"] : @"";
		
    addressLabel.text = [addressText stringByAppendingFormat:@"%@, %@ %@",self.organization.city,self.organization.state,self.organization.postalcode];
    [urlButton setTitle:self.organization.websiteUrl?:@"" forState:UIControlStateNormal];
    [facebookUrlButton setTitle:[self.organization.facebookUrl stringByReplacingOccurrencesOfString:@"http://www.facebook" withString:@"facebook" ]?:@"" forState:UIControlStateNormal];
    descriptionLabel.text = self.organization.summary;
    
    moreInfoEmailLabel.text = self.organization.email;
    moreInfoNameLabel.text = self.organization.primaryContact;
    moreInfoPhoneLabel.text = self.organization.phone;
    
    nameLabel.text = self.organization.name;
    
    if(![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"tel:1234567"]]) callButton.enabled = NO;
    
		profileImageView.url = self.organization.logoUrl;
		eventsImageView.url = self.organization.logoUrl;
		
		tableFooter.hidden = table.hidden;
		noResultsView.hidden = table.hidden;
		
		[self.view setNeedsLayout];
		
    self.isLoading = NO;
  }];
}

- (void) viewWillLayoutSubviews {
	[super viewWillLayoutSubviews];

	CGRect frame;
	frame = nameLabel.frame;
	frame.size.height = [nameLabel sizeThatFits:nameLabel.frame.size].height;
	nameLabel.frame = frame;
	
	frame = infoContainer.frame;
	frame.origin.y = CGRectGetMaxY(nameLabel.frame);
	infoContainer.frame = frame;
	
	frame = descriptionLabel.frame;
	frame.origin.y = CGRectGetMaxY(infoContainer.frame);
	frame.size.height = [descriptionLabel sizeThatFits:descriptionLabel.frame.size].height;
	descriptionLabel.frame = frame;
	
	frame = moreInfoContainer.frame;
	frame.origin.y = descriptionLabel.frame.origin.y+MAX(descriptionLabel.frame.size.height+5, 20);
	moreInfoContainer.frame = frame;
	
	frame = footerContainer.frame;
	frame.origin.y = fmaxf(CGRectGetMaxY(moreInfoContainer.frame),self.view.bounds.size.height-footerContainer.frame.size.height) ;
	footerContainer.frame = frame;
	
	scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, CGRectGetMaxY(frame));
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  EventTableViewCell* cell = (EventTableViewCell*) [super tableView:tableView cellForRowAtIndexPath:indexPath];
  cell.highlight = sortSegmentedControl.selectedSegmentIndex ? EventCellHighlightDistance : EventCellHighlightDate;
  return cell;
}

- (IBAction) sectionSegmentChanged:(id)sender {
  for(UIScrollView* view in @[table,scrollView]) {
    view.hidden = !view.hidden;
    view.scrollEnabled = !view.hidden;
  }
  tableFooter.hidden = table.hidden;
  noResultsView.hidden = table.hidden;
  
  ((UISegmentedControl*) sender).selectedSegmentIndex = !((UISegmentedControl*) sender).selectedSegmentIndex ;
  
  [self track];
}

- (void) sortEvents {
  self.events = [self.events sortedArrayUsingComparator:^(Event* a, Event* b) {
    if (sortSegmentedControl.selectedSegmentIndex) {
      if(b.distance == 0 && a.distance == 0) return (NSComparisonResult) NSOrderedSame;
      if(b.distance == 0) return (NSComparisonResult) NSOrderedAscending;
      if(a.distance == 0) return (NSComparisonResult) NSOrderedDescending;
      return [@(a.distance) compare:@(b.distance)];
    }
    return  [a.startTime compare:b.startTime];
  }];
}

- (IBAction) sortSegmentedChanged:(id)sender {
  [self sortEvents];
  [table reloadData];
}

- (IBAction) urlPressed:(id)sender {
  NSURL *url = [NSURL URLWithString:(sender == facebookUrlButton ? self.organization.facebookUrl : self.organization.websiteUrl)];
  [[UIApplication sharedApplication] openURL:url];
}

- (IBAction) callPressed:(id)sender { 
  NSURL *url = [NSURL URLWithString:[@"tel://" stringByAppendingString:self.organization.phone]];
  [[UIApplication sharedApplication] openURL:url];
}

- (IBAction) emailPressed:(id)sender {
  NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"mailto:%@",self.organization.email]];
  [[UIApplication sharedApplication] openURL:url];
}

- (IBAction) sharePressed:(id)sender {
  [[Sharer sharerWithObject:self.organization] shareFromController:self];
}

@end
