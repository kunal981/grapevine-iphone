//
//  SearchByController.m
//  GrapeVine
//
//  Created by Zachary Gavin on 3/13/13.
//  Copyright (c) 2013 com.grape-vine. All rights reserved.
//

#import "SearchByController.h"
#import "Event.h"
#import "EventTableViewCell.h"

@implementation SearchByController

- (void) viewDidLoad {
  [super viewDidLoad];
  [segmentedControl addTarget:self action:@selector(segmentChanged:) forControlEvents:UIControlEventValueChanged];
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  EventTableViewCell* cell = (EventTableViewCell*) [super tableView:tableView cellForRowAtIndexPath:indexPath];
  cell.highlight = segmentedControl.selectedSegmentIndex ? EventCellHighlightDistance : EventCellHighlightDate;
  return cell;
}

- (void) sortEvents {
  self.events = [self.events sortedArrayUsingComparator:^(Event* a, Event* b) {
    if (segmentedControl.selectedSegmentIndex) {
      if(b.distance == 0 && a.distance == 0) return (NSComparisonResult) NSOrderedSame;
      if(b.distance == 0) return (NSComparisonResult) NSOrderedAscending;
      if(a.distance == 0) return (NSComparisonResult) NSOrderedDescending;
      return [@(a.distance) compare:@(b.distance)];
    }
    return  [a.startTime compare:b.startTime];
  }];
}

- (IBAction) segmentChanged:(id)sender {
  [self sortEvents];
  [self track];
  
  [table reloadData];
}

- (void) track {
  
}

@end
