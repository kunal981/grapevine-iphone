//
//  CLLocationManager+Simulator.h
//  GrapeVine
//
//  Created by Zachary Gavin on 9/24/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

@interface CLLocationManager (Simulator)

@end
