//
//  APIWrapperTests
//  GrapeVine
//
//  Created by Jack Kustanowitz on 8/5/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "APIWrapperTests.h"
#import "ApiWrapper.h"
#import "Organization.h"
#import "User.h"
#import "Event.h"
#import "Community.h"
#import "Interest.h"
#import "NSDictionary+Subscript.h"
#import "NSArray+Subscript.h"
#import "LocationManager.h"
#import "CLLocationManager+Simulator.h"

@implementation APIWrapperTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testGetOrganizations
{
  [self getInterestingUser];
  
	NSArray* organizations = [Organization all];
	
	STAssertTrue([organizations count] > 0, @"Should get more than one organization back from the API");
}

- (void)testUserLoginWithEmail
{
	User* user = [self getInterestingUser];
	
	STAssertNotNil(user.key, @"Should get a user key");
	
	// make sure we got profile data
	STAssertTrue([user.bookmarkedEvents count] > 0, @"Should get bookmarks");
	STAssertTrue([user.likedEvents count] > 0, @"Should get like events");
//	STAssertTrue([user.notLikeEventIds count] > 0, @"Should get notlike events");
	STAssertTrue([user.registeredEvents count] > 0, @"Should get register events");

	// make sure we got general settings info
	STAssertTrue([[Community all] count] > 0, @"Should get locations");
	STAssertTrue([[Neighborhood all] count] > 0, @"Should get neighborhoods");
	STAssertTrue([[Interest all] count] > 0, @"Should get interests");
}

- (void)testUserLoginError
{
	APIWrapper* api = [APIWrapper api];
	NSError* error;
	NSDictionary* userDict = [api userLoginWithEmail:@"hacker@whatever.com" andPassword:@"hack34" error:&error];
	
	STAssertNil(userDict, @"Should not get a user key");
	STAssertNotNil(error, @"Should get an error object");
	STAssertEquals(error.code, 401, @"Should get a permissions error");
}

-(void) testUserGetProfile
{
	User* user = [self getInterestingUser];
  
	APIWrapper* api = [APIWrapper api]; NSError* error;
	NSDictionary* dict = [api userGetProfile:user error:&error];
	
	STAssertNotNil(dict, @"Should get a user");
}

-(void) testUserGetRecommendedEvents
{
	User* user = [self getInterestingUser];
	APIWrapper* api = [APIWrapper api]; NSError* error;

	NSDictionary* eventDict = [api userGetRecommendedEvents:user error:&error];
  
	STAssertTrue([eventDict[EventTypeSponsored] count] > 0, @"Should get sponsored events back from this call");
	STAssertTrue([eventDict[EventTypeFriends] count] > 0, @"Should get friends events back from this call");
	STAssertTrue([eventDict[EventTypeRecommended] count] > 0, @"Should get recommended events back from this call");
	
	NSArray* sponsoredEvents = eventDict[@"sponsored"];
	
	NSDictionary* e = [sponsoredEvents objectAtIndex:0];
	STAssertTrue([e[@"title"] length] > 0, @"event should have a title");
	STAssertTrue([e[@"description"] length] > 0, @"event should have a description");
	
}

-(void) testUserGetNearbyEvents
{
	User* user = [self getInterestingUser];
  
  [[LocationManager sharedLocationManager] fetchLocation];
  
  
	APIWrapper* api = [APIWrapper api]; NSError* error;
	NSArray* events = [api userGetNearbyEvents:user error:&error][EventTypeNearby];
	
	STAssertTrue([events count] > 0, @"Should get some nearby events back for the user");
	
	NSDictionary* e = [events objectAtIndex:0];
	STAssertTrue([e[@"title"] length] > 0, @"event should have a title");
	STAssertTrue([e[@"description"] length] > 0, @"event should have a description");
	
}

-(void) testSearch
{
	User* user = [self getInterestingUser];
	APIWrapper* api = [APIWrapper api]; NSError* error;
	
	// let's assume there will almost always be an upcoming event in the system that is a class of some kind or other
	NSDictionary* events = [api userGetEvents:user bySearchTerm:@"class" error:&error];
	
	STAssertTrue([events count] > 0, @"Should get events back from search");
}

-(void) testUserEventActions
{
	// blah blah just setup at this point
	User* user = [self getInterestingUser];

	APIWrapper* api = [APIWrapper api]; NSError* error;
	NSArray* events = [api userGetRecommendedEvents:user error:&error][@"sponsored"];
	STAssertTrue([events count] > 0, @"Should get some recommended events back for the user");
	
	NSDictionary* e = events[0];
	
	// now the real stuff
	// Location
//	STAssertTrue([Community currentLocation] != nil, @"Should get the current location");
	
//	BOOL ret = [api userAction:@"like" forUserKey:user.key withEventId:[e[@"id"] intValue] error:&error];
//	STAssertTrue(ret, @"User should be able to like an event");
//	
//	ret = [api userAction:@"unlike" forUserKey:user.key withEventId:[e[@"id"] intValue] error:&error];
//	STAssertTrue(ret, @"User should be able to unlike an event");
//	
//	ret = [api userAction:@"notlike" forUserKey:user.key withEventId:[e[@"id"] intValue] error:&error];
//	STAssertTrue(ret, @"User should be able to notlike an event");
//	
//	ret = [api userAction:@"unnotlike" forUserKey:user.key withEventId:[e[@"id"] intValue] error:&error];
//	STAssertTrue(ret, @"User should be able to unnotlike an event");
//	
////	ret = [api userAction:@"view" forUserKey:user.key withEventId:[e[@"id"] intValue] error:&error];
////	STAssertTrue(ret, @"User should be able to like an event");
//	
//	ret = [api userAction:@"bookmark" forUserKey:user.key withEventId:[e[@"id"] intValue] error:&error];
//	STAssertTrue(ret, @"User should be able to bookmark an event");
//	
//	ret = [api userAction:@"unbookmark" forUserKey:user.key withEventId:[e[@"id"] intValue] error:&error];
//	STAssertTrue(ret, @"User should be able to unbookmark an event");
}

//-(void) testUpdateUserInterests
//{
//	User* user = [self getInterestingUser];	
//	
//	int interestCount = [user.interests count];
//	Interest* interest = user.interests[0];
//	
//	// remove interest
//	[user setValue:NO forInterest:interest.interest_id];
//	STAssertTrue(user.interests.count == interestCount - 1, @"Should remove an interest from the user");
//	
//	// re-get the user
//	user = [self getInterestingUser];
//	STAssertTrue(user.interests.count == interestCount - 1, @"Removed interest should persist to server");
//	
//	// add interest back
//	[user setValue:YES forInterest:interest.interest_id];
//
//	STAssertTrue(user.interests.count == interestCount, @"Should add an interest to the user");
//	
//	// re-get the user
//	user = [self getInterestingUser];
//	STAssertTrue(user.interests.count == interestCount, @"Added interest should persist to server");
//}

-(void) testOrganizationGetDetails
{
	APIWrapper* api = [APIWrapper api];
	NSError* error;
	
	// get user key
	NSDictionary* details = [api organizationGetDetails:(Organization*) [Organization objectWithKey:@"70"] error:&error];
	
	STAssertTrue([details[@"name"] length] > 0, @"Should get a non-blank name for the organization");
}


- (void)testUserRegistrationWithEmail
{
	APIWrapper* api = [APIWrapper api];
	NSError* error1;
	NSString* email = @"tester@mountainpasstech.com";
	
	NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:
							email, kUserParamsEmail,
							@"testing", kUserParamsPassword,
							@"20902", kUserParamsZipcode,
							@"49", kUserParamsAge,
							nil];
	
	NSDictionary* ret = [api userRegisterWithParams:params debug:YES error:&error1];
	NSString* code = ret[kUserActivationCode];
	NSString* userKey = ret[kUserKey];
	
	STAssertNotNil(userKey, @"Should get a user key");
	
  User* user = (User*) [User objectWithKey:userKey];
  user.email = email;
  
	// Copmlete registration with activation
	NSError* error2;
	BOOL bRet = [api userActivateRegistration:user withCode:code error:&error2];
	STAssertTrue(bRet, @"Validation should pass");
	
	// And then delete the user
	NSError* error3;
	BOOL bRet2 = [api userDelete:user error:&error3];
	STAssertTrue(bRet2, @"User should get deleted");
	
}

//-(void) testUserActivate
//{
//	APIWrapper* api = [APIWrapper api];
//	NSError* error;
//	NSString* userKey = @"78e024d677664e08abdeaac2ce7e2c6";	// got this in an email after registration, might not always work
//	
//	BOOL ret = [api userActivateRegistrationForUserKey:userKey withCode:@"3f8c40" error:&error];
//	
//	STAssertTrue(ret, @"Validation should pass");
//}

-(User*) getInterestingUser
{
	APIWrapper* api = [APIWrapper api];
	NSError* error;
	NSDictionary* userDict = [api userLoginWithEmail:@"jack@mountainpasstech.com" andPassword:@"testing" error:&error];
	User* user = (User*) [User objectWithDictionary:userDict];

	return user;
}


@end
