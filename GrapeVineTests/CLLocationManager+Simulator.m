//
//  CLLocationManager+Simulator.m
//  GrapeVine
//
//  Created by Zachary Gavin on 9/24/12.
//  Copyright (c) 2012 com.grape-vine. All rights reserved.
//

#import "CLLocationManager+Simulator.h"
#import <objc/runtime.h>
#import <objc/message.h>


@implementation CLLocationManager (Simulator)

+ (void) load {
  Method origMethod = class_getInstanceMethod([CLLocationManager class], @selector(startMonitoringSignificantLocationChanges));
  Method newMethod = class_getInstanceMethod([CLLocationManager class], @selector(startMonitoringFakeSignificantLocationChanges));
  method_exchangeImplementations(origMethod, newMethod);
}

-(void) startMonitoringFakeSignificantLocationChanges {
  CLLocation *fake = [[CLLocation alloc] initWithLatitude:40.3871286 longitude:-111.8080274];
  [self.delegate locationManager:self
             didUpdateToLocation:fake
                    fromLocation:fake];
}

@end
